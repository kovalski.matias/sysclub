<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );

Route::middleware(['auth'])->group(function() {
    include 'rutas/Welcome.php';
    include 'rutas/Sexo.php';
    include 'rutas/User.php';
    include 'rutas/Roles.php';
    include 'rutas/Audit.php';
    include 'rutas/Configuracion.php';
    include 'rutas/TipoDocumento.php';
    include 'rutas/TipoMovimiento.php';
    include 'rutas/Personal.php';
    include 'rutas/Socio.php';
    include 'rutas/Actividad.php';
    include 'rutas/Agenda.php';
    include 'rutas/Pais.php';
    include 'rutas/Provincia.php';
    include 'rutas/Ciudad.php';
    include 'rutas/Perfil.php';
    include 'rutas/Inscripcion.php';
    include 'rutas/Membresia.php';
    include 'rutas/Cuota.php';
    include 'rutas/Mercadopago.php';
    include 'rutas/Email.php';
    include 'rutas/Asistencia.php';
    include 'rutas/Empresa.php';
    include 'rutas/Encuesta.php';
    include 'rutas/Estadistica.php';
    include 'rutas/Espacio.php';
    include 'rutas/Disciplina.php';
    include 'rutas/Recargo.php';
    include 'rutas/Pregunta.php';
    include 'rutas/Archivo.php';
    include 'rutas/Movimiento.php';    
    include 'rutas/Documentacion.php';

});

