 <?php

    Route::get('perfil', 'PerfilController@index')->name('perfil.index');
    Route::get('perfil/actualizar',['as'=> 'perfil.edit', 'uses' => 'PerfilController@edit']);
    Route::get('perfil/show',['as'=> 'perfil.show', 'uses' => 'PerfilController@show']);
    Route::patch('perfil/actualizar',['as'=> 'perfil.update', 'uses' => 'PerfilController@update']);

    Route::get('perfil/membresias',['as'=> 'perfil.membresias', 'uses' => 'PerfilController@membresias']);
    Route::get('perfil/{membresia}/cuotas',['as'=> 'perfil.cuotas', 'uses' => 'PerfilController@cuotas']);

    Route::get('perfil/{membresia}/encuestas',['as'=> 'perfil.encuestas', 'uses' => 'PerfilController@encuestas']);
    

    //confirmar asistencia
    Route::get('confirmar', 'ConfirmarAsistenciaController@index')->name('confirmar.index');
    Route::post('/confirmar/confirmar', 'ConfirmarAsistenciaController@confirmar');
    //Route::get('confirmar/alternativas', 'ConfirmarAsistenciaController@alternativas')->name('confirmar.alternativas');
    Route::post('/confirmar/alternativaConfirmada', 'ConfirmarAsistenciaController@alternativas')->name('confirmar.alternativaConfirmada');

?>