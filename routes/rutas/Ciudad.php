<?php

    Route::get('ciudad', 'CiudadController@index')->name('ciudad.index');

    Route::get('ciudad/create/encontrarPais', 'CiudadController@encontrarPais');
    Route::get('ciudad/create/encontrarciudad', 'CiudadController@encontrarPais');

    Route::post('/ciudad/guardar', 'CiudadController@guardar');

?>