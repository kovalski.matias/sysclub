<?php

    Route::get('tipodocumento', 'TipoDocumentoController@index')->name('tipodocumento.index');

    Route::delete('tipodocumento/{tipodocumento}', 'TipoDocumentoController@destroy')->name('tipodocumento.destroy');


    Route::get('tipodocumento/create', 'TipoDocumentoController@create')->name('tipodocumento.create');


    Route::post('tipodocumento', 'TipoDocumentoController@store')->name('tipodocumento.store');

    Route::get('tipodocumento/{tipodocumento}/edit', 'TipoDocumentoController@edit')->name('tipodocumento.edit');


    Route::patch('tipodocumento/{tipodocumento}', 'TipoDocumentoController@update')->name('tipodocumento.update');


    Route::post('/tipodocumento/guardar', 'TipoDocumentoController@guardar');

    Route::patch('tipodocumento/{tipodocumento}', 'TipoDocumentoController@update')->name('tipodocumento.update');



?>
