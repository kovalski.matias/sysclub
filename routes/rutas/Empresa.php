<?php

    Route::get('empresa', 'EmpresaController@index')->name('empresa.index');

    /*Route::get('empresa/create', 'EmpresaController@create')->name('empresa.create');

    Route::post('empresa', 'EmpresaController@store')->name('empresa.store');

    Route::get('empresa/{empresa}/edit', 'EmpresaController@edit')->name('empresa.edit');*/

    Route::patch('empresa/{empresa}', 'EmpresaController@update')->name('empresa.update');

    /*Route::get('empresa/{empresa}/show', 'EmpresaController@show')->name('empresa.show');*/

    Route::get('empresa/create/encontrarProvincia', 'EmpresaController@encontrarProvincia');
    Route::get('empresa/create/encontrarCiudad', 'EmpresaController@encontrarCiudad');
    /*Route::get('empresa/create/encontrarBarrio', 'EmpresaController@encontrarBarrio');
    Route::get('empresa/create/encontrarCalle', 'EmpresaController@encontrarCalle');*/

    Route::post('empresa/create/paisCreate', 'EmpresaController@paisCreate');
    Route::post('empresa/create/provinciaCreate', 'EmpresaController@provinciaCreate');
    Route::post('empresa/create/ciudadCreate', 'EmpresaController@ciudadCreate');


?>
