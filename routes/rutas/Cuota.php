<?php

    Route::get('cuota', 'CuotaController@index')->name('cuota.index');

    Route::delete('cuota/{cuota}', 'CuotaController@delete')->name('cuota.delete');


    Route::get('cuota/create', 'CuotaController@create')->name('cuota.create');


    Route::post('cuota', 'CuotaController@store')->name('cuota.store');

    Route::get('cuota/{cuota}/edit', 'CuotaController@edit')->name('cuota.edit');


    Route::patch('cuota/{cuota}', 'CuotaController@update')->name('cuota.update');

    Route::get('cuota/{cuota}/show', 'CuotaController@show')->name('cuota.show');

    Route::post('/cuota/guardar', 'CuotaController@guardar');

    Route::post('/cuota/{cuota}/generarPreference', 'MercadoPagoController@generarPreference')->name('cuota.generarPreference');

    Route::post('mercadopago/notificacion', 'MercadoPagoController@notificacion')->name('notificacion');

    Route::post('/cuota/cobrar', 'CuotaController@cobrar');

    Route::get('/cuota/{cuota}/cancelar', 'CuotaController@cancelar');

    //Cuotas de un determinado alumno
    Route::get('cuota/{alumno}', 'CuotaController@cuotaSocio')->name('cuota.cuotaSocio');

    //PDF recibo
    Route::get('recibo/{recibo}/pdf','CuotaController@crearReciboPDF')->name('recibo.pdf');

?>
