<?php

    Route::get('actividad', 'ActividadController@index')->name('actividad.index');

    Route::delete('actividad/{actividad}', 'ActividadController@delete')->name('actividad.delete');


    Route::get('actividad/create', 'ActividadController@create')->name('actividad.create');


    Route::post('actividad', 'ActividadController@store')->name('actividad.store');

    Route::get('actividad/{actividad}/edit', 'ActividadController@edit')->name('actividad.edit');


    Route::patch('actividad/{actividad}', 'ActividadController@update')->name('actividad.update');

    Route::get('actividad/{actividad}/show', 'ActividadController@show')->name('actividad.show');


?>
