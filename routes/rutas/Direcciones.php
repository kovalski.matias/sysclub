<?php

Route::get('direcciones/create/encontrarProvincia', 'DireccionController@encontrarProvincia');
Route::get('direcciones/create/encontrarCiudad', 'DireccionController@encontrarCiudad');
Route::get('direcciones/create/encontrarBarrio', 'DireccionController@encontrarBarrio');
Route::get('direcciones/create/encontrarCalle', 'DireccionController@encontrarCalle');

Route::post('direcciones/create/paisCreate', 'DireccionController@paisCreate');
Route::post('direcciones/provinciaCreate', 'DireccionController@provinciaCreate')->name('direcciones.provinciaCreate');
Route::post('direcciones/create/ciudadCreate', 'DireccionController@ciudadCreate');

?>
