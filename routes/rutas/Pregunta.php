<?php

    Route::get('pregunta', 'PreguntaEncuestaController@index')->name('pregunta.index');
    Route::post('/pregunta/guardar', 'PreguntaEncuestaController@guardar');
    Route::patch('pregunta/{pregunta}', 'PreguntaEncuestaController@update')->name('pregunta.update');

?>