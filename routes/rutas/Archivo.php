<?php

    Route::get('archivo/{alumno}', 'ArchivoController@index')->name('archivo.index');

    Route::post('/archivo/guardar', 'ArchivoController@guardar')->name('archivo.guardar');

    Route::get('archivo/create', 'ArchivoController@create')->name('archivo.create');


    Route::post('archivo', 'ArchivoController@store')->name('archivo.store');

    //Route::get('alumno/{alumno}/show', 'AlumnoController@show')->name('alumno.show');

    //PDF ficha médica
    Route::get('ficha_medica/pdf','ArchivoController@crearFichaPDF')->name('ficha_medica.pdf');

    Route::get('/download/{archivo}' , 'ArchivoController@downloadFile')->name('archivo.descargar');


?>
