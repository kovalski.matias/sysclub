<?php

    Route::get('disciplina', 'DisciplinaController@index')->name('disciplina.index');

    Route::post('/disciplina/guardar', 'DisciplinaController@guardar');

    Route::patch('disciplina/{disciplina}', 'DisciplinaController@update')->name('disciplina.update');



?>