<?php

    Route::get('membresia', 'MembresiaController@index')->name('membresia.index');

    Route::delete('membresia/{membresia}', 'MembresiaController@delete')->name('membresia.delete');


    Route::get('membresia/create', 'MembresiaController@create')->name('membresia.create');


    Route::post('membresia', 'MembresiaController@store')->name('membresia.store');

    Route::get('membresia/{membresia}/edit', 'MembresiaController@edit')->name('membresia.edit');


    Route::patch('membresia/{membresia}', 'MembresiaController@update')->name('membresia.update');

    Route::get('membresia/{membresia}/show', 'MembresiaController@show')->name('membresia.show');

    Route::post('/membresia/guardar', 'MembresiaController@guardar');

    Route::post('/membresia/{membresia}/cancelar', 'MembresiaController@cancelar');


?>
