<?php

    Route::get('inscripciones', 'InscripcionController@index')->name('inscripciones.index');

    Route::delete('inscripciones/{inscripciones}', 'InscripcionController@delete')->name('inscripciones.delete');


    Route::get('inscripciones/create', 'InscripcionController@create')->name('inscripciones.create');


    Route::post('inscripciones', 'InscripcionController@store')->name('inscripciones.store');

    Route::get('inscripciones/{inscripciones}/edit', 'InscripcionController@edit')->name('inscripciones.edit');


    Route::patch('inscripciones/{inscripciones}', 'InscripcionController@update')->name('inscripciones.update');

    Route::get('inscripciones/{inscripciones}/show', 'InscripcionController@show')->name('inscripciones.show');

    Route::post('/inscripciones/guardar', 'InscripcionController@guardar');


?>
