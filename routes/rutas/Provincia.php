<?php

    Route::get('provincia', 'ProvinciaController@index')->name('provincia.index');

    Route::get('provincia/create/encontrarPais', 'ProvinciaController@encontrarPais');

    /*Route::get('provincia/create', 'ProvinciaController@create')->name('provincia.create');
    Route::post('provincia', 'ProvinciaController@store')->name('provincia.store');*/

    Route::post('/provincia/guardar', 'ProvinciaController@guardar');

?>