<?php

    Route::get('socio', 'SocioController@index')->name('socio.index');

    //Route::delete('Socio/{Socio}', 'SocioController@delete')->name('Socio.delete');


    Route::get('socio/create', 'SocioController@create')->name('socio.create');


    Route::post('socio', 'SocioController@store')->name('socio.store');

    Route::get('socio/{socio}/edit', 'SocioController@edit')->name('socio.edit');


    Route::patch('socio/{socio}', 'SocioController@update')->name('socio.update');

    Route::get('socio/{socio}/show', 'SocioController@show')->name('socio.show');

    Route::get('socio/{socio}/eliminar', 'SocioController@delete')->name('socio.eliminar');

    Route::get('socio/eliminado', 'SocioController@eliminados')->name('socio.eliminado');

    Route::get('socio/{socio}', 'SocioController@restaurar')->name('socio.restaurar');

    Route::get('socio/create/encontrarProvincia', 'SocioController@encontrarProvincia');
    Route::get('socio/create/encontrarCiudad', 'SocioController@encontrarCiudad');
    Route::get('socio/create/encontrarBarrio', 'SocioController@encontrarBarrio');
    Route::get('socio/create/encontrarCalle', 'SocioController@encontrarCalle');

    Route::post('socio/create/paisCreate', 'SocioController@paisCreate');
    Route::post('socio/create/provinciaCreate', 'SocioController@provinciaCreate');
    Route::post('socio/create/ciudadCreate', 'SocioController@ciudadCreate');


?>
