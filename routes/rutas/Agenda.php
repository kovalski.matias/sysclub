<?php

    Route::get('/agenda', 'AgendaController@index')->name('agenda.index');
    Route::get('/agenda/listar', 'AgendaController@listar');
    Route::post('/agenda/guardar', 'AgendaController@guardar');

    Route::patch('/agenda/{id}', 'AgendaController@update')
        ->name('agenda.update');


    Route::get('/agenda/informe', 'AgendaController@informe');
    Route::post('/agenda/generar/informe', 'AgendaController@generar_informe');

    Route::get('agenda/create/encontrarEspacio', 'AgendaController@encontrarEspacio');

    //ruta para cargar asistencias del dia
    Route::get('agenda/{agenda}/asistir', 'AsistenciaController@asistir')->name('agenda.asistir');
    Route::post('asistencia', 'AsistenciaController@guardar')->name('asistencia.guardar');
    //actualizar asistencia
    Route::get('agenda/{agenda}/editarsistencia', 'AsistenciaController@edit')->name('agenda.editasistencia');
    Route::patch('asistencia/{asistencia}', 'AsistenciaController@update')->name('asistencia.update');

?>
