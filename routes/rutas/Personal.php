<?php

    Route::get('personal', 'PersonalController@index')->name('personal.index');

    //Route::delete('personal/{personal}', 'PersonalController@delete')->name('personal.delete');


    Route::get('personal/create', 'PersonalController@create')->name('personal.create');


    Route::post('personal', 'PersonalController@store')->name('personal.store');

    Route::get('personal/{personal}/edit', 'PersonalController@edit')->name('personal.edit');


    Route::patch('personal/{personal}', 'PersonalController@update')->name('personal.update');

    Route::get('personal/{personal}/show', 'PersonalController@show')->name('personal.show');

    Route::get('personal/{personal}/eliminar', 'PersonalController@delete')->name('personal.eliminar');

    Route::get('personal/eliminado', 'PersonalController@eliminados')->name('personal.eliminado');

    Route::get('personal/{personal}', 'PersonalController@restaurar')->name('personal.restaurar');



    Route::get('personal/create/encontrarProvincia', 'PersonalController@encontrarProvincia');
    Route::get('personal/create/encontrarCiudad', 'PersonalController@encontrarCiudad');
    Route::get('personal/create/encontrarBarrio', 'PersonalController@encontrarBarrio');
    Route::get('personal/create/encontrarCalle', 'PersonalController@encontrarCalle');

            
    Route::get('personal-list-pdf','PersonalController@exportPdf')
        ->name('personal.pdf');

?>
