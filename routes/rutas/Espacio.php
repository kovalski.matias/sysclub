<?php

    Route::get('espacio', 'EspacioController@index')->name('espacio.index');

    Route::post('/espacio/guardar', 'EspacioController@guardar');

    Route::patch('espacio/{espacio}', 'EspacioController@update')->name('espacio.update');


?>