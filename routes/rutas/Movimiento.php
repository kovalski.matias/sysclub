<?php

    Route::get('movimiento', 'MovimientoController@index')->name('movimiento.index');

    Route::delete('movimiento/{movimiento}', 'MovimientoController@delete')->name('movimiento.delete');


    Route::get('movimiento/create', 'MovimientoController@create')->name('movimiento.create');


    Route::post('movimiento', 'MovimientoController@store')->name('movimiento.store');

    Route::get('movimiento/{movimiento}/edit', 'MovimientoController@edit')->name('movimiento.edit');


    Route::patch('movimiento/{movimiento}', 'MovimientoController@update')->name('movimiento.update');

    Route::get('movimiento/{movimiento}/show', 'MovimientoController@show')->name('movimiento.show');

    Route::get('movimientos-list-pdf','MovimientoController@exportPdf')
        ->name('movimiento.pdf');



?>