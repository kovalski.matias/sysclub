<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class FormaPago extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps=true;

    protected $fillable = ['nombre', 'descripcion', 'logo'];

    public function cobro()
    {
        return $this->hasMany(Cobro::class);
    }
}
