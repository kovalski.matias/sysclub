<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class SubTipoMovimiento extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'nombre', 'tipo_movimiento_id', 'descripcion'
    ];

    protected $table = 'sub_tipo_movimientos';

    public function movimientos()
    {
        return $this->hasMany(Movimiento::class);
    }

    public function tipo()
    {
        return $this->belongsTo(TipoMovimiento::class);
    }
}
