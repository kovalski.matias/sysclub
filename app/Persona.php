<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Persona extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    //use OwenIt\Auditing\Contracts\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'apellidos', 'nombres', 'fecha_nacimiento', 'email', 'sexo_id', 'tipoDocumento_id', 'documento', 'domicilio_id', 'cuenta', 'telefono'
    ];

    public function nombreCompleto()
    {
        return $this->nombres . " " . $this->apellidos;
    }

    public function sexo(){
        return $this->belongsTo(Sexo::class);
    }

    public function tipoDocumento(){
        return $this->belongsTo(TipoDocumento::class);
    }

    public function user(){
        return $this->hasOne(User::class);
    }

    public function socio(){
        return $this->hasOne(Socio::class);
    }

    public function personal(){
        return $this->hasOne(Personal::class);
    }

    /*public function domicilio()
    {
        return $this->hasOne(Domicilio::class);
    }*/

    public function domicilio()
    {
        return $this->belongsTo(Domicilio::class);
    }

    public function direccion()
    {
        return $this->domicilio->direccion . " " . $this->domicilio->piso . " " . $this->domicilio->departamento;
    }

    /*public function direccionHastaBarrio()
    {
        return $this->domicilio->nro_casa . " " . $this->domicilio->piso . ", " .  $this->domicilio->calle->nombre . ", " . $this->domicilio->calle->barrio->nombre;
    }*/

}
