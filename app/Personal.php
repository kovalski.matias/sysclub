<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Personal extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps=true;

    protected $fillable = [
        'estado_id'
    ];

    protected $table = 'personal';

    /*HAS es si tiene el id el otro
      BELONG es si el id lo tengo yo*/

    public function persona()
    {
        return $this->belongsTo('App\Persona');
    }

    public function disciplinas()
    {
        return $this->belongsToMany(Disciplina::class);
    }

    public function estado(){
        return $this->belongsTo(Estado::class);
    }

    
    public function getDisciplinaNames()
    {
        return $this->disciplinas->pluck('nombre');
    }

    public function actividades()
    {
        return $this->hasMany(Actividad::class);
    }


    public function existeReferencia()
    {
        //referencia para saber si el paciente tiene Declaracion Jurada asociada
        $refActividad = Actividad::wherePersonal_id($this->id)->count();

        if($refActividad > 0) //existe referencia
        {
            return "Advertencia el personal tiene $refActividad actividades"; //existe referencia
        }
        else
        {
            return "El personal no tiene actividades asosciadas en este momento";//no existe referencia
        }

    }

    

}
