<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DetalleAsistencia extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'asistencia_id', 'membresia_id', 'observacion', 'presente', 'observacion_asistencia_id'
    ];

    public function asistencia(){
        return $this->belongsTo(Asistencia::class);
    }
    public function membresia(){
        return $this->belongsTo(Membresia::class);
    }

    /**
     * Get the observacionAsistencia that owns the DetalleAsistencia
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function observacionAsistencia(): BelongsTo
    {
        return $this->belongsTo(ObservacionAsistencia::class);
    }
}
