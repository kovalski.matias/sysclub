<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Socio extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'estado_id'
    ];


    protected $table = 'socios';


    public function persona(){
        return $this->belongsTo(Persona::class);
    }

    public function estado(){
        return $this->belongsTo(Estado::class);
    }

    public function archivos()
    {
        return $this->hasMany(Archivo::class);
    }

    public function membresias()
    {
        return $this->hasMany(Membresia::class);
    }

    public function numeroMembresias(){
        $cantidad=0;
        $membresias=Membresia::whereAlumno_id($this->id)->whereEstado_membresia_id(1)->get();
        if(is_null($membresias)){
            return "Ninguna";
        }else{
            foreach ($membresias as $m) {
                $cantidad+=1;
            }
            return "Activas ". $cantidad;
        }
    }

}
