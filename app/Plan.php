<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'descripcion'
    ];

    protected $table = 'planes';

    public function actividades()
    {
        return $this->hasMany(Actividad::class);
    }
}
