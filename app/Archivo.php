<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archivo extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'codigo',
        'documento',
        'descripcion',
        'socio_id'
    ];


    /*HAS es si tiene el id el otro
      BELONG es si el id lo tengo yo*/

    public function socio()
    {
        return $this->belongsTo(Socio::class);
    }
}
