<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Carbon\Carbon;

class Cuota extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /*'codigo',*/ 'monto', 'fecha', 'membresia_id', 'socio', 'actividad', 'estado_cuota_id'/*, 'preference_id'*/
    ];

    public function membresia()
    {
        return $this->belongsTo(Membresia::class);
    }

    public function estadoCuota()
    {
        return $this->belongsTo(EstadoCuota::class);
    }

    public function cobro()
    {
        return $this->hasOne(Cobro::class);
    }

    public function fecha_casteada()
    {
        $fecha_vencimiento=$this->fecha;

        return Carbon::parse($fecha_vencimiento)->format('d/m/Y');
    }

    /**
     * Get all of the movimientos for the Cuota
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function movimientos(): HasMany
    {
        return $this->hasMany(Movimientos::class);
    }
}
