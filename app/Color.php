<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'valor', 'texto',
    ];

    protected $table = 'colores';

    public function agendas()
    {
        return $this->hasMany(Agenda::class);
    }
}
