<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Cobro extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        /*'codigo',*/ 'monto', 'fecha', 'cuota_id', 'estado_cobro_id', 'preference_id', 'forma_pago_id'
    ];

    public function cuota()
    {
        return $this->belongsTo(Cuota::class);
    }
    
    public function recibo()
    {
        return $this->hasOne(Recibo::class);
    }

    public function formaPago()
    {
        return $this->belongsTo(FormaPago::class);
    }

    public function estadoCobro()
    {
        return $this->belongsTo(EstadoCobro::class);
    }

}