<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ObservacionAsistencia extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'observacion'
    ];

    /**
     * Get all of the comments for the ObservacionAsistencia
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detallesAsistencias(): HasMany
    {
        return $this->hasMany(DetalleAsistencia::class);
    }

    
}
