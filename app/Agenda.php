<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Agenda extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $table = "agenda";

    protected $fillable = [
        "disciplina_id",
        "espacio_id",
        'color_id',
        "fecha_inicial",
        "fecha_final",
        "hora_inicial",
        "hora_final",
        'domingo',
        'lunes',
        'martes',
        'miercoles',
        'jueves',
        'viernes',
        'sabado',
        "descripcion",
        "estado",
        'personal_id',
        'monto',
        'cupo_actividad'
    ];

    public function calendario(){
        return $this->descripcion . " " . $this->hora_inicial . " - " . $this->hora_final;
    }

    public function disciplina()
    {
        return $this->belongsTo(Disciplina::class);
    }

    public function espacio()
    {
        return $this->belongsTo(Espacio::class);
    }

    public function color()
    {
        return $this->belongsTo(Color::class);
    }

    public function personal()
    {
        return $this->belongsTo(Personal::class);
    }

    public function actividad()
    {
        return $this->hasOne(Actividad::class);
    }

    /**
     * Get all of the membresias for the Agenda
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function membresias()
    {
        return $this->hasMany(Membresia::class);
    }

    public function agendaDelDia(){
        $now=now()->translatedFormat('l');

        if ($now=='lunes') {
            return $this->lunes;
        } elseif ($now=='martes') {
            return $this->martes;
        } elseif ($now=='miercoles') {
            return $this->miercoles;
        } elseif ($now=='jueves') {
            return $this->jueves;
        } elseif ($now=='viernes') {
            return $this->viernes;
        } elseif ($now=='sabado') {
            return $this->sabado;
        }
        

    }

    public function actividadesDelDia(){
        //$actividades=Actividad::whereAgenda_id($this->id)->get();
        $agendas=Agenda::whereLunes('1')->get();
        return $agenda;
    }

    
}
