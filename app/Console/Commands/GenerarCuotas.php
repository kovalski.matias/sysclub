<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Cuota;
use App\Membresia;
use Carbon\Carbon;

class GenerarCuotas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generar:cuota';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Genera cuotas mensuales a las membresías activas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //toma la fecha de hoy
        $now = now();
        //trae todos los ticket que no hayan sido Resueltos
        $membresias = Membresia::where('estado_membresia_id',1)->get();
        //Recorro los tickets que no hayan sido resueltos
        foreach($membresias as $m)
        {
            $cuota = new Cuota;
            $cuota->membresia_id=$m->id;
            $cuota->estado_cuota_id=1;
            $cuota->monto=$m->precio;
            //$cuota->codigo=str_pad($n, 10, '0', STR_PAD_LEFT);
            $cuota->fecha=now()->addDays(9);
            $cuota->alumno=$m->alumno->persona->nombreCompleto();
            $cuota->actividad=$m->agenda->descripcion;
            $cuota->save();
                

        }
    }
}
