<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Cuota;
use App\Membresia;
use App\Recargo;
use Carbon\Carbon;

class CuotaVencida extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vencer:cuota';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se registran los vencimientos de las cuotas despues de un tiempo de generada la cuoa';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //toma la fecha de hoy
        $now = now();
        
        $recargo=Recargo::whereNombre('Cuotas Vencida')->first();
        //trae todos los ticket que no hayan sido Resueltos
        $cuotas = Cuota::where('estado_cuota_id',1)->get();
        //Recorro los tickets que no hayan sido resueltos
        foreach($cuotas as $cuota)
        {
            $montoVencida=($cuota->monto*$recargo->incremento)/100;
            $cuota->estado_cuota_id=3;
            $cuota->monto+=$montoVencida;
        
            $cuota->save();
            
        }
    }
}
