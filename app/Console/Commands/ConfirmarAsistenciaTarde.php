<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ConfirmarAsistencia;
use App\Agenda;
use App\Actividad;
use App\Membresia;

class ConfirmarAsistenciaTarde extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'confirmar:asistenciaPM';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Confirmacion de asistencia por la tarde';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $month = now()->format('m');
        $year = now()->format('Y');

        $dia = now()->translatedFormat('l');
        
        $agendasLunes=Agenda::whereLunes('1')->get();
        $agendasMartes=Agenda::whereMartes('2')->get();
        $agendasMiercoles=Agenda::whereMiercoles('3')->get();
        $agendasJueves=Agenda::whereJueves('4')->get();
        $agendasViernes=Agenda::whereViernes('5')->get();
        $agendasSabado=Agenda::whereSabado('6')->get();

        //$agendas=Agenda::all();
        //$grafico[][][]= array();
            $lunesmañana=0;$lunestarde=0;$lunesnoche=0;
            $martesmañana=0;$martestarde=0;$martesnoche=0;
            $miercolesmañana=0;$miercolestarde=0;$miercolesnoche=0;
            $juevesmañana=0;$juevestarde=0;$juevesnoche=0;
            $viernesmañana=0;$viernestarde=0;$viernesnoche=0;
            $sabadomañana=0;$sabadotarde=0;$sabadonoche=0;

        if ($dia=='lunes') {
            foreach ($agendasLunes as $agenda) {
                $hi = (int)$agenda->hora_inicial;
                $hf = (int)$agenda->hora_final;
                $membresias=Membresia::whereAgenda_id($agenda->id)->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->whereEstado_membresia_id(1)->get();
                foreach ($membresias as $membresia) {
                    if (($hi>=13)&&($hf<=23)) {
                        $confirmacion = new ConfirmarAsistencia();
                        $confirmacion->asistencia = true;
                        $confirmacion->fecha=date('Y-m-d');
                        $confirmacion->estado = 1;
                        $confirmacion->membresia_id=$membresia->id;
                        $confirmacion->save();
                    }
                } 
                
            }
    
        }
        
        if ($dia=='martes') {
            foreach ($agendasMartes as $agenda) {
                $hi = (int)$agenda->hora_inicial;
                $hf = (int)$agenda->hora_final;
                $membresias=Membresia::whereAgenda_id($agenda->id)->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->whereEstado_membresia_id(1)->get();
                foreach ($membresias as $membresia) {
                    if (($hi>=13)&&($hf<=23)) {
                        $confirmacion = new ConfirmarAsistencia();
                        $confirmacion->asistencia = true;
                        $confirmacion->fecha=date('Y-m-d');
                        $confirmacion->estado = 1;
                        $confirmacion->membresia_id=$membresia->id;
                        $confirmacion->save();
                    }
                } 
                
            }
        }
        
        if ($dia=='miércoles') {
            foreach ($agendasMiercoles as $agenda) {
                $hi = (int)$agenda->hora_inicial;
                $hf = (int)$agenda->hora_final;
                $actividad=Actividad::find($agenda->id);
                $membresias=Membresia::whereAgenda_id($agenda->id)->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->whereEstado_membresia_id(1)->get();
                foreach ($membresias as $membresia) {
                    if (($hi>=13)&&($hf<=23)) {
                        $confirmacion = new ConfirmarAsistencia();
                        $confirmacion->asistencia = true;
                        $confirmacion->fecha=date('Y-m-d');
                        $confirmacion->estado = 1;
                        $confirmacion->membresia_id=$membresia->id;
                        $confirmacion->save();
                    }
                } 
                
            }
        }
        
        if ($dia=='jueves') {
            foreach ($agendasJueves as $agenda) {
                $hi = (int)$agenda->hora_inicial;
                $hf = (int)$agenda->hora_final;
                $actividad=Actividad::find($agenda->id);
                $membresias=Membresia::whereAgenda_id($agenda->id)->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->whereEstado_membresia_id(1)->get();
                foreach ($membresias as $membresia) {
                    if (($hi>=13)&&($hf<=23)) {
                        $confirmacion = new ConfirmarAsistencia();
                        $confirmacion->asistencia = true;
                        $confirmacion->fecha=date('Y-m-d');
                        $confirmacion->estado = 1;
                        $confirmacion->membresia_id=$membresia->id;
                        $confirmacion->save();
                    }
                } 
                
            }
        }
        
        if ($dia=='viernes') {
            foreach ($agendasViernes as $agenda) {
                $hi = (int)$agenda->hora_inicial;
                $hf = (int)$agenda->hora_final;
                $actividad=Actividad::find($agenda->id);
                $membresias=Membresia::whereAgenda_id($agenda->id)->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->whereEstado_membresia_id(1)->get();
                foreach ($membresias as $membresia) {
                    if (($hi>=13)&&($hf<=23)) {
                        $confirmacion = new ConfirmarAsistencia();
                        $confirmacion->asistencia = true;
                        $confirmacion->fecha=date('Y-m-d');
                        $confirmacion->estado = 1;
                        $confirmacion->membresia_id=$membresia->id;
                        $confirmacion->save();
                    }
                } 
                
            }
        }
        
        if ($dia=='sábado') {
            foreach ($agendasSabado as $agenda) {
                $hi = (int)$agenda->hora_inicial;
                $hf = (int)$agenda->hora_final;
                $actividad=Actividad::find($agenda->id);
                $membresias=Membresia::whereAgenda_id($agenda->id)->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->whereEstado_membresia_id(1)->get();
                foreach ($membresias as $membresia) {
                    if (($hi>=13)&&($hf<=23)) {
                        $confirmacion = new ConfirmarAsistencia();
                        $confirmacion->asistencia = true;
                        $confirmacion->fecha=date('Y-m-d');
                        $confirmacion->estado = 1;
                        $confirmacion->membresia_id=$membresia->id;
                        $confirmacion->save();
                    }
                } 
                
            }
        }
        
    }
}
