<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Agenda;
use App\Cronograma;
use Carbon\Carbon;

class GenerarCronograma extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generar:cronograma';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se generan los cronogramas diarios';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $agendasLunes=Agenda::whereLunes('1')->get();
        $agendasMartes=Agenda::whereMartes('2')->get();
        $agendasMiercoles=Agenda::whereMiercoles('3')->get();
        $agendasJueves=Agenda::whereJueves('4')->get();
        $agendasViernes=Agenda::whereViernes('5')->get();
        $agendasSabado=Agenda::whereSabado('6')->get();

        $now=now()->translatedFormat('l');

        if ($now=='lunes') {
            $agendas=$agendasLunes;

            foreach ($agendas as $agenda) {
                $cronograma = new Cronograma();
                $cronograma->agenda_id=$agenda->id;
                $cronograma->fecha=date('Y-m-d');
                $cronograma->hora_inicial=$agenda->hora_inicial;
                $cronograma->hora_final=$agenda->hora_final;
                $cronograma->domingo=$agenda->domingo;
                $cronograma->lunes=$agenda->lunes;
                $cronograma->martes=$agenda->martes;
                $cronograma->miercoles=$agenda->miercoles;
                $cronograma->jueves=$agenda->jueves;
                $cronograma->viernes=$agenda->viernes;
                $cronograma->sabado=$agenda->sabado;
                $cronograma->descripcion=$agenda->descripcion;

                $cronograma->save();

            }

        } elseif ($now=='martes') {
            $agendas=$agendasMartes;

            foreach ($agendas as $agenda) {
                $cronograma = new Cronograma();
                $cronograma->agenda_id=$agenda->id;
                $cronograma->fecha=date('Y-m-d');
                $cronograma->hora_inicial=$agenda->hora_inicial;
                $cronograma->hora_final=$agenda->hora_final;
                $cronograma->domingo=$agenda->domingo;
                $cronograma->lunes=$agenda->lunes;
                $cronograma->martes=$agenda->martes;
                $cronograma->miercoles=$agenda->miercoles;
                $cronograma->jueves=$agenda->jueves;
                $cronograma->viernes=$agenda->viernes;
                $cronograma->sabado=$agenda->sabado;
                $cronograma->descripcion=$agenda->descripcion;

                $cronograma->save();

            }
        } elseif ($now=='miércoles') {
            $agendas=$agendasMiercoles;

            foreach ($agendas as $agenda) {
                $cronograma = new Cronograma();
                $cronograma->agenda_id=$agenda->id;
                $cronograma->fecha=date('Y-m-d');
                $cronograma->hora_inicial=$agenda->hora_inicial;
                $cronograma->hora_final=$agenda->hora_final;
                $cronograma->domingo=$agenda->domingo;
                $cronograma->lunes=$agenda->lunes;
                $cronograma->martes=$agenda->martes;
                $cronograma->miercoles=$agenda->miercoles;
                $cronograma->jueves=$agenda->jueves;
                $cronograma->viernes=$agenda->viernes;
                $cronograma->sabado=$agenda->sabado;
                $cronograma->descripcion=$agenda->descripcion;

                $cronograma->save();

            }
        } elseif ($now=='jueves') {
            $agendas=$agendasJueves;

            foreach ($agendas as $agenda) {
                $cronograma = new Cronograma();
                $cronograma->agenda_id=$agenda->id;
                $cronograma->fecha=date('Y-m-d');
                $cronograma->hora_inicial=$agenda->hora_inicial;
                $cronograma->hora_final=$agenda->hora_final;
                $cronograma->domingo=$agenda->domingo;
                $cronograma->lunes=$agenda->lunes;
                $cronograma->martes=$agenda->martes;
                $cronograma->miercoles=$agenda->miercoles;
                $cronograma->jueves=$agenda->jueves;
                $cronograma->viernes=$agenda->viernes;
                $cronograma->sabado=$agenda->sabado;
                $cronograma->descripcion=$agenda->descripcion;

                $cronograma->save();

            }
        } elseif ($now=='viernes') {
            $agendas=$agendasViernes;

            foreach ($agendas as $agenda) {
                $cronograma = new Cronograma();
                $cronograma->agenda_id=$agenda->id;
                $cronograma->fecha=date('Y-m-d');
                $cronograma->hora_inicial=$agenda->hora_inicial;
                $cronograma->hora_final=$agenda->hora_final;
                $cronograma->domingo=$agenda->domingo;
                $cronograma->lunes=$agenda->lunes;
                $cronograma->martes=$agenda->martes;
                $cronograma->miercoles=$agenda->miercoles;
                $cronograma->jueves=$agenda->jueves;
                $cronograma->viernes=$agenda->viernes;
                $cronograma->sabado=$agenda->sabado;
                $cronograma->descripcion=$agenda->descripcion;

                $cronograma->save();

            }
        } elseif ($now=='sábado') {
            $agendas=$agendasSabado;

            foreach ($agendas as $agenda) {
                $cronograma = new Cronograma();
                $cronograma->agenda_id=$agenda->id;
                $cronograma->fecha=date('Y-m-d');
                $cronograma->hora_inicial=$agenda->hora_inicial;
                $cronograma->hora_final=$agenda->hora_final;
                $cronograma->domingo=$agenda->domingo;
                $cronograma->lunes=$agenda->lunes;
                $cronograma->martes=$agenda->martes;
                $cronograma->miercoles=$agenda->miercoles;
                $cronograma->jueves=$agenda->jueves;
                $cronograma->viernes=$agenda->viernes;
                $cronograma->sabado=$agenda->sabado;
                $cronograma->descripcion=$agenda->descripcion;

                $cronograma->save();

            }
        }
    }
}
