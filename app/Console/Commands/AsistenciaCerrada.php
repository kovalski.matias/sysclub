<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Asistencia;
use Carbon\Carbon;

class AsistenciaCerrada extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cerrar:asistencia';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Se cierran las asistencias del día';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //toma la fecha de hoy
        $now = now();
        
        //trae todos los ticket que no hayan sido Resueltos
        $asistencias = Asistencia::where('estado_asistencia_id','<>' , 3)->get();
        //Recorro los tickets que no hayan sido resueltos
        foreach($asistencias as $asistencia)
        {
            
            $asistencia->estado_asistencia_id=3;
            
            $asistencia->save();
        }
    }
}
