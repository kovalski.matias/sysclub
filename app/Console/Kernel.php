<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\GenerarCuotas',
        'App\Console\Commands\CuotaVencida',
        //'App\Console\Commands\CuotaVencida',
        'App\Console\Commands\GenerarCronograma',
        'App\Console\Commands\ConfirmarAsistenciaMañana',
        'App\Console\Commands\ConfirmarAsistenciaTarde',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*$schedule->command('inspire')
                ->hourly();*/
        $schedule->command('generar:cuota')->dailyAt('09:56');
        $schedule->command('vencer:cuota')->monthlyOn(10, '10:56');
        $schedule->command('cerrar:asistencia')->dailyAt('00:00');
        $schedule->command('generar:cronograma')->dailyAt('00:00');
        $schedule->command('confirmar:asistenciaAM')->dailyAt('07:00');
        $schedule->command('confirmar:asistenciaPM')->dailyAt('12:00');
        
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
