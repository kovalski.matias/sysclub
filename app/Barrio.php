<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Barrio extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps=false;

    protected $fillable = ['nombre','ciudad_id'];

    protected $table = 'barrios';

    public function ciudad()
    {
        return $this->belongsTo('App\Ciudad');
    }
    
    public function calles()
    {
        return $this->hasMany('App\Calle');
    }
}
