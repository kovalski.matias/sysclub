<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disciplina extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'descripcion'
    ];

    public function actividades()
    {
        return $this->hasMany(Actividad::class);
    }

    public function espacios()
    {
        return $this->belongsToMany(Espacio::class);
    }

    public function agendas()
    {
        return $this->hasMany(Agenda::class);
    }

    public function personals()
    {
        return $this->belongsToMany(Personal::class);
    }

    
}