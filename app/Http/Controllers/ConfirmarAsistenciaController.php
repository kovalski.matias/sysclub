<?php

namespace App\Http\Controllers;

use App\ConfirmarAsistencia;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as Auth;
use App\User;
use App\Membresia;
use App\Actividad;
use App\Agenda;
use App\Socio;
use App\Persona;
use App\Disciplina;

class ConfirmarAsistenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::User();
        $persona=Persona::find($user->persona_id);
        $socio=Socio::wherePersona_id($persona->id)->first();
        $membresias = Membresia::whereSocio_id($socio->id)->get();

        
        return view('confirmar.index',compact('membresias'));
    }

    public function confirmar(Request $request){

        $id=$request->get('confirmacion');
        //dd($id);
        $confirmar=ConfirmarAsistencia::findOrFail($id);
        $confirmar->asistencia=$request->get('asistencia');
        $confirmar->estado=2;
        $confirmar->update();
        //dd($confirmar);

        if($confirmar->asistencia==1){
            return view("confirmar.asistira");
            
        }else{
            $membresia=Membresia::find($confirmar->membresia_id);
            $agenda=Agenda::find($membresia->agenda_id);
            $disciplina=Disciplina::find($agenda->disciplina_id);
            //$agendas=Agenda::whereDisciplina_id($disciplina->id)->where('id','!=', $agenda->id)->get();
            $agendas=Agenda::whereDisciplina_id($disciplina->id)->where('id','!=', $agenda->id)->where('cupo_actividad','>=',1)->get();
            //dd($agendas);

            return view('confirmar.alternativas',compact('agendas'));

        }
    }

    public function alternativas(){
        return view('confirmar.alternativaConfirmada');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ConfirmarAsistencia  $confirmarAsistencia
     * @return \Illuminate\Http\Response
     */
    public function show(ConfirmarAsistencia $confirmarAsistencia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ConfirmarAsistencia  $confirmarAsistencia
     * @return \Illuminate\Http\Response
     */
    public function edit(ConfirmarAsistencia $confirmarAsistencia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ConfirmarAsistencia  $confirmarAsistencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConfirmarAsistencia $confirmarAsistencia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ConfirmarAsistencia  $confirmarAsistencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(ConfirmarAsistencia $confirmarAsistencia)
    {
        //
    }
}
