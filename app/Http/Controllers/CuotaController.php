<?php

namespace App\Http\Controllers;
use App\Cuota;
use App\Socio;
use App\Cobro;
use App\EstadoCuota;
use App\Encuesta;
use App\EstadoEncuesta;
use App\Membresia;
use App\Agenda;
use App\Personal;
use App\Movimiento;
use App\TipoMovimiento;
use App\Disciplina;
use App\Recibo;
use App\Empresa;
use App\Recargo;
use App\FormaPago;
use App\NumerosALetras;  //llamando la clase
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;

class CuotaController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:listar cuotas|crear cuota|editar cuota|eliminar cuota', ['only' => ['index','store']]);
        $this->middleware('permission:crear cuota', ['only' => ['create','store']]);
        $this->middleware('permission:editar cuota', ['only' => ['edit','update']]);
        $this->middleware('permission:eliminar cuota', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cuotas=Cuota::all();
        $estados=EstadoCuota::all();
        $formapagos=FormaPago::all();
        $recargo=Recargo::find(2);

        $desde=$request->desde;
        $hasta=$request->hasta;
        
        if(count($request->all())>1)
        {
            //dd($request->all());
            $sql = Cuota::select('cuotas.*');
            $c = Cuota::all();
            
            if($request->desde)
            {
                $sql = $sql->whereDate('created_at','>=',$request->desde);
            }            
            if($request->hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$request->hasta);
            }
            if($request->estado_cuota_id)
            {
                $sql = $sql->whereEstado_cuota_id($request->estado_cuota_id); //creo la consulta y almaceno en "sql"
            }

            if($request->forma_pago_id)
            {
                
            }
            
            $cuotas=$sql->orderBy('created_at','desc')->get();
            $estado_cuota_id=$request->estado_cuota_id;


        }
        else
        {

            $estado_cuota_id=null;
            $cuotas=Cuota::orderBy('created_at','desc')->get();

        }
        if($request->exists('pdf'))
        {
            $colspan = 0;

        return $this->exportPdf($cuotas, $desde, $hasta);
        }

        return view('cuota.index',[
            "cuotas"                =>  $cuotas,
            "desde"                 =>  $desde,
            "hasta"                 =>  $hasta,
            "estado_cuota_id"       =>  $estado_cuota_id, //si los id son identicos que me mantenga el valor
            "estados"               =>  $estados,
            "formapagos"            =>  $formapagos,
            "recargo"               =>  $recargo,

            ]);
    }

    public function cobrar(Request $request){
        $cobro = new Cobro();
        $cobro->cuota_id = $request->get('cuota_id');
        $cobro->monto = $request->get('monto');
        $cobro->fecha = now();
        $cobro->forma_pago_id = 1;
        $cobro->estado_cobro_id = 1;
        $cobro->save();

        $cuota=Cuota::findOrFail($cobro->cuota_id);
        $cuota->estado_cuota_id = 2;
        $cuota->update();

        $membresia=Membresia::findOrFail($cuota->membresia_id);
        $membresia->ends_at = now()->addDays(30);
        $membresia->update();

        $agenda=Agenda::findOrFail($membresia->agenda_id);
        $personal=Personal::findOrFail($agenda->personal_id);
        $disciplina=Disciplina::findOrFail($agenda->disciplina_id);

        $encuesta = new Encuesta();
        $encuesta->membresia_id=$membresia->id;
        $encuesta->personal_id=$personal->id;
        $encuesta->estado_encuesta_id=1;
        $encuesta->save();

        $m = new Movimiento();
        $m->monto=$cobro->monto;
        $m->fecha = now();
        $m->tipo_movimiento_id=1;
        $m->subtipo_movimiento_id=1;
        $m->disciplina_id=$disciplina->id;
        $m->cuota_id=$cuota->id;
        $m->save();

        $r=Recibo::count() + 1;
        $recibo = new Recibo();
        $recibo->monto=$cobro->monto;
        $recibo->fecha = now();
        $recibo->cobro_id = $cobro->id;
        $recibo->codigo=str_pad($r, 10, '0', STR_PAD_LEFT);
        $recibo->save();

        $importe=NumerosALetras::convertir($recibo->monto, 'pesos');

        $empresa=Empresa::all();

        $recibo=Recibo::find($recibo->id);

        $pdf = PDF::loadView('recibo.pdf',[
            "recibo"   =>  $recibo,
            "empresa"   =>  $empresa,
            "importe"   =>  $importe
            ]);

        $pdf->setPaper('a4','letter');
        
        return redirect()->route('cuota.index');

        $pdf->download('recibo.pdf');
    }

    public function crearReciboPDF($id)
    {
        $recibo=Recibo::find($id);
        $empresa=Empresa::all();

        $importe=NumerosALetras::convertir($recibo->monto, 'pesos');

        $pdf = PDF::loadView('recibo.pdf',[
            "recibo"   =>  $recibo,
            "empresa"   =>  $empresa,
            "importe"   =>  $importe
            ]);

        $pdf->setPaper('a4','letter');

        return $pdf->stream('recibo.pdf');
    }

    public function cancelar($id)
    {
        //Cuota
        $cuota=Cuota::findOrFail($id);
        $cuota->estado_cuota_id=4;
        $cuota->update();
        //dd($cuota);

        //Cobro
        $cobro=Cobro::whereCuota_id($cuota->id)->first();
        //dd($cobro);
        $cobro->estado_cobro_id = 2;
        $cobro->update();
        //dd($cobro);
    
        $membresia=Membresia::findOrFail($cuota->membresia_id);
        $membresia->ends_at = now()->subDays(30);
        $membresia->update();

        $agenda=Agenda::findOrFail($membresia->actividad_id);
        $disciplina=Disciplina::findOrFail($agenda->disciplina_id);

        //Movimiento
        $m = new Movimiento();
        $m->monto=-$cobro->monto;
        $m->fecha = now();
        $m->tipo_movimiento_id=2;
        $m->subtipo_movimiento_id=2;
        $m->disciplina_id=$disciplina->id;
        $m->cuota_id=$cuota->id;
        $m->save();

        return redirect()->route('cuota.index');
    }

    public function cuotaSocio($id){

        $membresias=Membresia::whereSocio_id($id)->whereEstadoMembresia_id(1)->orderBy('created_at','desc')->get();
        
        
        $socio=Socio::find($id);

        return view("cuota.cuotaSocio", compact('membresias','socio'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
