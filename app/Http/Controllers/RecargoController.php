<?php

namespace App\Http\Controllers;

use App\Recargo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RecargoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recargos = Recargo::all();
        return view('recargo.index',["recargos"=> $recargos]);
    }

    public function guardar(Request $request)
    {
        $recargo = new Recargo;
        $recargo->nombre = $request->get('nombre');
        $recargo->incremento = $request->get('incremento');
        $recargo->descripcion = $request->get('descripcion');
        $recargo->save();
        
        return redirect()->route('recargo.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recargo  $recargo
     * @return \Illuminate\Http\Response
     */
    public function show(Recargo $recargo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recargo  $recargo
     * @return \Illuminate\Http\Response
     */
    public function edit(Recargo $recargo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recargo  $recargo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recargo $recargo)
    {
        $recargo->nombre=$request->get('nombre');
        $recargo->incremento=$request->get('incremento');
        $recargo->descripcion=$request->get('descripcion');

        $recargo->update();

        return redirect()->route('recargo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recargo  $recargo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recargo $recargo)
    {
        //
    }
}
