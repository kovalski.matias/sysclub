<?php

namespace App\Http\Controllers;

use App\Exports\AgendaExport;
use App\Agenda;
use App\Actividad;
use App\Personal;
use App\Disciplina;
use App\Espacio;
use App\DisciplinaEspacio;
use App\Color;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redirect;
use PDF;

class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:listar agenda|crear socio|editar socio|eliminar socio', ['only' => ['index','store']]);
    }
    
    public function encontrarEspacio(Request $request)
	{

        $idDisciplina = $request->id;
        $espacios = Espacio::join('disciplina_espacio','disciplina_espacio.espacio_id','=','espacios.id')
                    ->where('disciplina_espacio.disciplina_id','=',$idDisciplina)
                    ->select('espacios.id','espacios.nombre')
                    ->get();
        
        
        return response()->json($espacios);
    }    
    
    public function index()
    {
        $disciplinas=Disciplina::all();
        $colores=Color::all();
        $instructores=Personal::orderBy('valoracion', 'DESC')->get();

        return view("agenda.index", compact('disciplinas','colores','instructores'));
    }

    public function listar()
    {
        $agenda = Agenda::all();
        $nueva_agenda = [];
        //$color= Color::all()


        foreach ($agenda as $value) {
            $nueva_agenda[] = [
                "id" => $value->id,
                /*"start" => $value->fecha_inicial . " " . $value->hora_inicio,
                "end" => $value->fecha_final . " " . $value->hora_final,*/
                "daysOfWeek" => [ $value->domingo, $value->lunes, $value->martes, $value->miercoles, $value->jueves, $value->viernes, $value->sabado],
                "startTime" => $value->hora_inicial,
                "endTime" => $value->hora_final,
                "startRecur" => $value->fecha_inicial,
                "endRecur" => $value->fecha_final,
                "title" => /*$value->espacio_id . " " .*/ $value->descripcion,
                "backgroundColor" => $value->color->valor,
                "textColor" => $value->color->texto,
                "extendedProps" => [
                    "espacio_id" => $value->espacio->nombre,
                    "instructor_id" => $value->personal->persona->nombreCompleto(),
                    "monto" => $value->monto,
                    "cupo"  => $value->cupo_actividad,
                ],
            ];
        }

        return response()->json($nueva_agenda);
    }

    /*public function show()
    {
        $data['agendas']=Agenda::all();
        return response()->json($data['agendas']);
        
    }*/


    public function validarFechaLunesPersonal($horaInicial, $horaFinal, $instructor_id)
    {
        $agenda = Agenda::whereLunes("1")
        ->wherePersonal_id($instructor_id)
        ->whereBetween('hora_inicial', [$horaInicial, $horaFinal])
        ->whereBetween('hora_final', [$horaInicial, $horaFinal])
        //->Where('martes', 'LIKE', "2")
            ->first();
        //dd($agenda);

        return $agenda == null ? true : false;
    }

    public function validarFechaLunesEspacio($horaInicial, $horaFinal, $espacio_id)
    {
        $agenda = Agenda::whereLunes("1")
        ->whereEspacio_id($espacio_id)
        ->whereBetween('hora_inicial', [$horaInicial, $horaFinal])
        ->whereBetween('hora_final', [$horaInicial, $horaFinal])
        //->Where('martes', 'LIKE', "2")
            ->first();
        //dd($agenda);

        return $agenda == null ? true : false;
    }

    public function validarFechaMartesPersonal($horaInicial, $horaFinal, $instructor_id)
    {
        $agenda = Agenda::whereMartes("3")
        ->wherePersonal_id($instructor_id)
        ->whereBetween('hora_inicial', [$horaInicial, $horaFinal])
        ->whereBetween('hora_final', [$horaInicial, $horaFinal])
        //->Where('martes', 'LIKE', "2")
            ->first();
        //dd($agenda);

        return $agenda == null ? true : false;
    }

    public function validarFechaMartesEspacio($horaInicial, $horaFinal, $espacio_id)
    {
        $agenda = Agenda::whereMartes("3")
        ->whereEspacio_id($espacio_id)
        ->whereBetween('hora_inicial', [$horaInicial, $horaFinal])
        ->whereBetween('hora_final', [$horaInicial, $horaFinal])
        //->Where('martes', 'LIKE', "2")
            ->first();
        //dd($agenda);

        return $agenda == null ? true : false;
    }

    public function validarFechaMiercolesPersonal($horaInicial, $horaFinal, $instructor_id)
    {
        $agenda = Agenda::whereMiercoles("3")
        ->wherePersonal_id($instructor_id)
        ->whereBetween('hora_inicial', [$horaInicial, $horaFinal])
        ->whereBetween('hora_final', [$horaInicial, $horaFinal])
        //->Where('martes', 'LIKE', "2")
            ->first();
        //dd($agenda);

        return $agenda == null ? true : false;
    }

    public function validarFechaMiercolesEspacio($horaInicial, $horaFinal, $espacio_id)
    {
        $agenda = Agenda::whereMiercoles("3")
        ->whereEspacio_id($espacio_id)
        ->whereBetween('hora_inicial', [$horaInicial, $horaFinal])
        ->whereBetween('hora_final', [$horaInicial, $horaFinal])
        //->Where('martes', 'LIKE', "2")
            ->first();
        //dd($agenda);

        return $agenda == null ? true : false;
    }

    public function validarFechaJuevesPersonal($horaInicial, $horaFinal, $instructor_id)
    {
        $agenda = Agenda::whereJueves("4")
        ->wherePersonal_id($instructor_id)
        ->whereBetween('hora_inicial', [$horaInicial, $horaFinal])
        ->whereBetween('hora_final', [$horaInicial, $horaFinal])
        //->Where('martes', 'LIKE', "2")
            ->first();
        //dd($agenda);

        return $agenda == null ? true : false;
    }

    public function validarFechaJuevesEspacio($horaInicial, $horaFinal, $espacio_id)
    {
        $agenda = Agenda::whereJueves("4")
        ->whereEspacio_id($espacio_id)
        ->whereBetween('hora_inicial', [$horaInicial, $horaFinal])
        ->whereBetween('hora_final', [$horaInicial, $horaFinal])
        //->Where('martes', 'LIKE', "2")
            ->first();
        //dd($agenda);

        return $agenda == null ? true : false;
    }
    

    public function validarFechaViernesPersonal($horaInicial, $horaFinal, $instructor_id)
    {
        $agenda = Agenda::whereViernes("5")
        ->wherePersonal_id($instructor_id)
        ->whereBetween('hora_inicial', [$horaInicial, $horaFinal])
        ->whereBetween('hora_final', [$horaInicial, $horaFinal])
        //->Where('martes', 'LIKE', "2")
            ->first();
        //dd($agenda);

        return $agenda == null ? true : false;
    }

    public function validarFechaViernesEspacio($horaInicial, $horaFinal, $espacio_id)
    {
        $agenda = Agenda::whereViernes("5")
        ->whereEspacio_id($espacio_id)
        ->whereBetween('hora_inicial', [$horaInicial, $horaFinal])
        ->whereBetween('hora_final', [$horaInicial, $horaFinal])
        //->Where('martes', 'LIKE', "2")
            ->first();
        //dd($agenda);

        return $agenda == null ? true : false;
    }

    public function validarFechaSabadoPersonal($horaInicial, $horaFinal, $instructor_id)
    {
        $agenda = Agenda::whereSabado("6")
        ->wherePersonal_id($instructor_id)
        ->whereBetween('hora_inicial', [$horaInicial, $horaFinal])
        ->whereBetween('hora_final', [$horaInicial, $horaFinal])
        //->Where('martes', 'LIKE', "2")
            ->first();
        //dd($agenda);

        return $agenda == null ? true : false;
    }

    public function validarFechaSabadoEspacio($horaInicial, $horaFinal, $espacio_id)
    {
        $agenda = Agenda::whereSabado("6")
        ->whereEspacio_id($espacio_id)
        ->whereBetween('hora_inicial', [$horaInicial, $horaFinal])
        ->whereBetween('hora_final', [$horaInicial, $horaFinal])
        //->Where('martes', 'LIKE', "2")
            ->first();
        //dd($agenda);

        return $agenda == null ? true : false;
    }

    public function guardar(Request $request)
    {
        $input = $request->all();
        //Lunes
        if ($request->get("lunes")=="1") {
            //dd($request->get("lunes"));
            if (($this->validarFechaLunesPersonal($request->get("txtHoraInicial"), $request->get("txtHoraFinal"), $request->get("instructor_id"))) && ($this->validarFechaLunesEspacio($request->get("txtHoraInicial"), $request->get("txtHoraFinal"), $request->get("espacio_id")))) {
                
                $espacio=Espacio::find($request->get('espacio_id'));
                
                $agenda = new Agenda;
                $agenda->disciplina_id=$request->get('disciplina_id');
                $agenda->espacio_id=$request->get('espacio_id');
                $agenda->color_id=$request->get('color_id');
                $agenda->personal_id=$request->get('instructor_id');
                $agenda->fecha_inicial=$request->get('txtFechaInicial');
                $agenda->fecha_final=$request->get('txtFechaFinal');
                $agenda->hora_inicial=$request->get('txtHoraInicial');
                $agenda->hora_final=$request->get('txtHoraFinal');
                $agenda->domingo=$request->get('domingo');
                $agenda->lunes=$request->get('lunes');
                $agenda->martes=$request->get('martes');
                $agenda->miercoles=$request->get('miercoles');
                $agenda->jueves=$request->get('jueves');
                $agenda->viernes=$request->get('viernes');
                $agenda->sabado=$request->get('sabado');
                $agenda->estado=1;
                $agenda->descripcion=$request->get('txtDescripcion');
                $agenda->monto=$request->get('monto');
                $agenda->cupo_actividad=$espacio->cupo_espacio;
                $agenda->save();
    
                return response()->json($agenda);
            } else {
                return response()->json($agenda);
            }
        }

        //Martes
        if ($request->get("martes")=="2") {
            //dd("martesssss");
            if (($this->validarFechaMartesPersonal($request->get("txtHoraInicial"), $request->get("txtHoraFinal"), $request->get("instructor_id"))) && ($this->validarFechaMartesEspacio($request->get("txtHoraInicial"), $request->get("txtHoraFinal"), $request->get("espacio_id")))) {
                $espacio=Espacio::find($request->get('espacio_id'));
                
                $agenda = new Agenda;
                $agenda->disciplina_id=$request->get('disciplina_id');
                $agenda->espacio_id=$request->get('espacio_id');
                $agenda->color_id=$request->get('color_id');
                $agenda->personal_id=$request->get('instructor_id');
                $agenda->fecha_inicial=$request->get('txtFechaInicial');
                $agenda->fecha_final=$request->get('txtFechaFinal');
                $agenda->hora_inicial=$request->get('txtHoraInicial');
                $agenda->hora_final=$request->get('txtHoraFinal');
                $agenda->domingo=$request->get('domingo');
                $agenda->lunes=$request->get('lunes');
                $agenda->martes=$request->get('martes');
                $agenda->miercoles=$request->get('miercoles');
                $agenda->jueves=$request->get('jueves');
                $agenda->viernes=$request->get('viernes');
                $agenda->sabado=$request->get('sabado');
                $agenda->estado=1;
                $agenda->descripcion=$request->get('txtDescripcion');
                $agenda->monto=$request->get('monto');
                $agenda->cupo_actividad=$espacio->cupo_espacio;
                $agenda->save();
    
                return response()->json($agenda);
            } else {
                return response()->json($agenda);
            }
        }

        //Miercoles
        if ($request->get("miercoles")=="3") {
            //dd($request->get("lunes"));
            if (($this->validarFechaMiercolesPersonal($request->get("txtHoraInicial"), $request->get("txtHoraFinal"), $request->get("instructor_id"))) && ($this->validarFechaMiercolesEspacio($request->get("txtHoraInicial"), $request->get("txtHoraFinal"), $request->get("espacio_id")))) {
                $espacio=Espacio::find($request->get('espacio_id'));
                
                $agenda = new Agenda;
                $agenda->disciplina_id=$request->get('disciplina_id');
                $agenda->espacio_id=$request->get('espacio_id');
                $agenda->color_id=$request->get('color_id');
                $agenda->personal_id=$request->get('instructor_id');
                $agenda->fecha_inicial=$request->get('txtFechaInicial');
                $agenda->fecha_final=$request->get('txtFechaFinal');
                $agenda->hora_inicial=$request->get('txtHoraInicial');
                $agenda->hora_final=$request->get('txtHoraFinal');
                $agenda->domingo=$request->get('domingo');
                $agenda->lunes=$request->get('lunes');
                $agenda->martes=$request->get('martes');
                $agenda->miercoles=$request->get('miercoles');
                $agenda->jueves=$request->get('jueves');
                $agenda->viernes=$request->get('viernes');
                $agenda->sabado=$request->get('sabado');
                $agenda->estado=1;
                $agenda->descripcion=$request->get('txtDescripcion');
                $agenda->monto=$request->get('monto');
                $agenda->cupo_actividad=$espacio->cupo_espacio;
                $agenda->save();
    
                return response()->json($agenda);
            } else {
                return response()->json($agenda);
            }
        }

        //jueves
        if ($request->get("jueves")=="4") {
            //dd($request->get("lunes"));
            if (($this->validarFechaJuevesPersonal($request->get("txtHoraInicial"), $request->get("txtHoraFinal"), $request->get("instructor_id"))) && ($this->validarFechaJuevesEspacio($request->get("txtHoraInicial"), $request->get("txtHoraFinal"), $request->get("espacio_id")))) {
                $espacio=Espacio::find($request->get('espacio_id'));
                
                $agenda = new Agenda;
                $agenda->disciplina_id=$request->get('disciplina_id');
                $agenda->espacio_id=$request->get('espacio_id');
                $agenda->color_id=$request->get('color_id');
                $agenda->personal_id=$request->get('instructor_id');
                $agenda->fecha_inicial=$request->get('txtFechaInicial');
                $agenda->fecha_final=$request->get('txtFechaFinal');
                $agenda->hora_inicial=$request->get('txtHoraInicial');
                $agenda->hora_final=$request->get('txtHoraFinal');
                $agenda->domingo=$request->get('domingo');
                $agenda->lunes=$request->get('lunes');
                $agenda->martes=$request->get('martes');
                $agenda->miercoles=$request->get('miercoles');
                $agenda->jueves=$request->get('jueves');
                $agenda->viernes=$request->get('viernes');
                $agenda->sabado=$request->get('sabado');
                $agenda->estado=1;
                $agenda->descripcion=$request->get('txtDescripcion');
                $agenda->monto=$request->get('monto');
                $agenda->cupo_actividad=$espacio->cupo_espacio;
                $agenda->save();
    
                return response()->json($agenda);
            } else {
                return response()->json($agenda);
            }
        }

        //viernes
        if ($request->get("viernes")=="5") {
            //dd($request->get("lunes"));
            if (($this->validarFechaViernesPersonal($request->get("txtHoraInicial"), $request->get("txtHoraFinal"), $request->get("instructor_id"))) && ($this->validarFechaViernesEspacio($request->get("txtHoraInicial"), $request->get("txtHoraFinal"), $request->get("espacio_id")))) {
                $espacio=Espacio::find($request->get('espacio_id'));
                
                $agenda = new Agenda;
                $agenda->disciplina_id=$request->get('disciplina_id');
                $agenda->espacio_id=$request->get('espacio_id');
                $agenda->color_id=$request->get('color_id');
                $agenda->personal_id=$request->get('instructor_id');
                $agenda->fecha_inicial=$request->get('txtFechaInicial');
                $agenda->fecha_final=$request->get('txtFechaFinal');
                $agenda->hora_inicial=$request->get('txtHoraInicial');
                $agenda->hora_final=$request->get('txtHoraFinal');
                $agenda->domingo=$request->get('domingo');
                $agenda->lunes=$request->get('lunes');
                $agenda->martes=$request->get('martes');
                $agenda->miercoles=$request->get('miercoles');
                $agenda->jueves=$request->get('jueves');
                $agenda->viernes=$request->get('viernes');
                $agenda->sabado=$request->get('sabado');
                $agenda->estado=1;
                $agenda->descripcion=$request->get('txtDescripcion');
                $agenda->monto=$request->get('monto');
                $agenda->cupo_actividad=$espacio->cupo_espacio;
                $agenda->save();
    
                return response()->json($agenda);
            } else {
                return response()->json($agenda);
            }
        }

        //sabado
        if ($request->get("sabado")=="6") {
            //dd($request->get("lunes"));
            if (($this->validarFechaSabadoPersonal($request->get("txtHoraInicial"), $request->get("txtHoraFinal"), $request->get("instructor_id"))) && ($this->validarFechaSabadoEspacio($request->get("txtHoraInicial"), $request->get("txtHoraFinal"), $request->get("espacio_id")))) {
                $espacio=Espacio::find($request->get('espacio_id'));
                
                $agenda = new Agenda;
                $agenda->disciplina_id=$request->disciplina_id;
                $agenda->espacio_id=$request->espacio_id;
                $agenda->color_id=$request->color_id;
                $agenda->personal_id=$request->instructor_id;
                $agenda->fecha_inicial=$request->txtFechaInicial;
                $agenda->fecha_final=$request->txtFechaFinal;
                $agenda->hora_inicial=$request->txtHoraInicial;
                $agenda->hora_final=$request->txtHoraFinal;
                $agenda->domingo=$request->domingo;
                $agenda->lunes=$request->lunes;
                $agenda->martes=$request->martes;
                $agenda->miercoles=$request->miercoles;
                $agenda->jueves=$request->jueves;
                $agenda->viernes=$request->viernes;
                $agenda->sabado=$request->sabado;
                $agenda->estado=1;
                $agenda->descripcion=$request->txtDescripcion;
                $agenda->monto=$request->get('monto');
                $agenda->cupo_actividad=$espacio->cupo_espacio;
                $agenda->save();
    
                return response()->json($agenda);
            } else {
                return response()->json($agenda);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $actividad=Actividad::findOrFail($id);
        $actividad->monto=$request->precio;
        $actividad->update();

        dd($actividad);

        return response()->json($respuesta);
    }

    public function informe()
    {
        return view("agenda.informe");
    }

    public function generar_informe(Request $request)
    {
        $input = $request->all();
        $agenda = Agenda::select("*")
            ->whereBetween('fecha', [$input["txtFechaInicial"], $input["txtFechaFinal"]])
            ->get();
        if (count($agenda) > 0) {
            if (isset($input["pdf"])) {
                return $this->generar_pdf($agenda, $input);
            } else if (isset($input["excel"])) {
                return $this->generar_excel($agenda, $input);
            }
        } else {
            return redirect("/agenda/informe");
        }
    }

    private function generar_pdf($agenda, $input)
    {
        $pdf = PDF::loadView('pdf.agenda', compact("agenda", "input"));
        Mail::send('email.agenda', compact("agenda"), function ($mail) use ($pdf) {
            $email = \Auth::user()->email;
            $mail->to([$email, "j-deiby@hotmail.com"]);
            $mail->attachData($pdf->output(), 'informe.pdf');
        });
        return $pdf->download('informe.pdf');
    }

    private function generar_excel($agenda, $input)
    {
        $agenda = new AgendaExport($agenda);
        return Excel::download($agenda, 'agenda.xlsx');
    }
}
