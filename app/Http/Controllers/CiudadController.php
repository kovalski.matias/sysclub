<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CiudadCreateRequest;
use App\Pais;
use App\Provincia;
use App\Ciudad;

class CiudadController extends Controller
{
    public function encontrarProvincia(Request $request)
	{
	    $provincias=Provincia::select('nombre','id')
			->where('pais_id',$request->id)
            ->get();
        return response()->json($provincias);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paises=Pais::all();
        $provincias=Provincia::all();
        $ciudades=Ciudad::all();

        return view('ciudad.index', ["paises"=> $paises, "provincias"=> $provincias, "ciudades"=> $ciudades]);
    }


    public function guardar(CiudadCreateRequest $request)
    {
        $ciudad=new Ciudad;
        $ciudad->nombre=$request->get('nombre');
        $ciudad->codigo_postal=$request->get('codigo_postal');
        $ciudad->provincia_id=$request->get('provincia_id');
        $ciudad->save();
        
        return redirect()->route('ciudad.index');
    }
}
