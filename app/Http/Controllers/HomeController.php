<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
Use App\Empresa;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::all();
        $cantUser = $users->count();

        

        $empresa = Empresa::find(1);

        $dia=\Carbon\Carbon::parse(now())->format('d-m');

        $fecha=\Carbon\Carbon::parse(auth()->user()->persona->fecha_nacimiento)->format('d-m');

        


        return view('welcome',[
            "cantUser"          => $cantUser,
            "dia"               => $dia,
            "fecha"             => $fecha,
            "empresa"           => $empresa,

        ]);
    }
}
