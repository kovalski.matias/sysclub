<?php

namespace App\Http\Controllers;

use App\Asistencia;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Agenda;
use App\Cronograma;
use App\Socio;
use App\Actividad;
use App\Membresia;
use App\DetalleAsistencia;
use App\ObservacionAsistencia;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class AsistenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$agenda=Agenda::all();
        /*$agendasLunes=Agenda::whereLunes('1')->get();
        $agendasMartes=Agenda::whereMartes('2')->get();
        $agendasMiercoles=Agenda::whereMiercoles('3')->get();
        $agendasJueves=Agenda::whereJueves('4')->get();
        $agendasViernes=Agenda::whereViernes('5')->get();
        $agendasSabado=Agenda::whereSabado('6')->get();

        $now=now()->translatedFormat('l');

        if ($now=='lunes') {
            $agendas=$agendasLunes;
            return view("asistencia.index",compact('agendas'));
        } elseif ($now=='martes') {
            $agendas=$agendasMartes;
            return view("asistencia.index",compact('agendas'));
        } elseif ($now=='miércoles') {
            $agendas=$agendasMiercoles;
            //dd($agendas);
            return view("asistencia.index",compact('agendas'));
        } elseif ($now=='jueves') {
            $agendas=$agendasJueves;
            return view("asistencia.index",compact('agendas'));
        } elseif ($now=='viernes') {
            $agendas=$agendasViernes;
            //dd($agendas);
            return view("asistencia.index",compact('agendas'));
        } elseif ($now=='sábado') {
            $agendas=$agendasSabado;
            return view("asistencia.index",compact('agendas'));
        }*/
        
        //return view("asistencia.index",compact('agendas'));
        $hoy=date('Y-m-d');
        
        $cronogramas=Cronograma::where("fecha","=",$hoy)->orderBy('created_at','desc')->get();
        //dd($cronogramas);
        return view("asistencia.index",compact('cronogramas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    public function asistir($id){

        //observaciones
        $observaciones=ObservacionAsistencia::all();
        //$agenda=Agenda::find($id);
        $cronograma=Cronograma::find($id);
        //dd($cronograma);
        $agenda=Agenda::find($cronograma->agenda_id)->get();
        //dd($agenda);
        //$actividad=Actividad::whereAgenda_id($cronograma->agenda_id)->get();
        //dd($actividad);
        $membresias=Membresia::whereAgenda_id($cronograma->agenda_id)->whereEstado_membresia_id(1)->get();
        //dd($membresias);

        return view("agenda.asistir",compact('cronograma','membresias','observaciones'));
    }

    public function guardar(Request $request)
    {
        $asistencia = new Asistencia();
        $asistencia->cronograma_id = $request->cronograma_id;
        
        $date = Carbon::now();
        
        $asistencia->fecha= date('Y-m-d');
        $asistencia->estado_asistencia_id=1;

        $asistencia->save();

        $cronograma=Cronograma::findOrFail($asistencia->cronograma_id);
        $agenda=Agenda::findOrFail($cronograma->agenda_id);
        //dd($agenda);
        //$actividad=Actividad::findOrFail($agenda->id);
        //dd($actividad);
        $membresias = Membresia::whereAgenda_id($agenda->id)->whereEstado_membresia_id(1)->get();
        //dd($membresias);
        $cantidad = $membresias->count();
        $cont = 0;

        while ($cont < $cantidad)
        {
            $detalleAsistencia = new DetalleAsistencia();
            $detalleAsistencia->membresia_id = $request->membresia_id[$cont];
            $detalleAsistencia->observacion_asistencia_id = $request->observacion_asistencia_id[$cont];
            $detalleAsistencia->asistencia_id = $asistencia->id;

            $detalleAsistencia->presente=$request->presente[$detalleAsistencia->membresia_id];

            $detalleAsistencia->save();
            $cont=$cont+1;

        }

        return redirect()->route('asistencia.index');/*->with('store_control_vacaciones', 'Control de vacaciones generado exitosamente')*/;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asistencia  $asistencia
     * @return \Illuminate\Http\Response
     */
    public function show(Asistencia $asistencia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asistencia  $asistencia
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $observaciones=ObservacionAsistencia::all();
        $cronograma=Cronograma::findOrFail($id);
        $agenda=Agenda::findOrFail($cronograma->agenda_id);
       // $agenda=Agenda::findOrFail($id);
        //$actividad=Actividad::whereAgenda_id($agenda->id)->first();
        $asistencia = Asistencia::whereCronograma_id($cronograma->id)->first();
        $detalles_asistencias=DetalleAsistencia::whereAsistencia_id($asistencia->id)->get();

        return view("agenda.editarasistencia",["observaciones"=>$observaciones,"cronograma"=>$cronograma,"agenda"=>$agenda,"asistencia"=>$asistencia,"detalles_asistencias"=>$detalles_asistencias]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asistencia  $asistencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $asistencia = Asistencia::findOrFail($id);
        $asistencia->estado_asistencia_id=2;

        $asistencia->update();

        $cronograma=Cronograma::findOrFail($asistencia->cronograma_id);
        $agenda=Agenda::findOrFail($cronograma->agenda_id);
        //dd($agenda);
        //$actividad=Actividad::whereAgenda_id($agenda->id)->first();
        //dd($actividad);
        $membresias = Membresia::whereAgenda_id($agenda->id)->get();
        $detalles_asistencias=DetalleAsistencia::whereAsistencia_id($asistencia->id)->get();
        //dd($detalles_asistencias);
        //$m=$membresias;
        //dd($request);

        $cantidad = $detalles_asistencias->count();
        //dd($cantidad);
        $cont = 0;
        
        //for($i=0;$i<count($values['e_id']);$i++)
    
        foreach($detalles_asistencias as $key=>$value)
        {
            
            $detalle=$detalles_asistencias->find($value);
            $m=$detalle->membresia_id;
            $o=$detalle->observacion_asistencia_id; 
            //dd($m);
            $detalle->update([
                'presente'=>$request->presente[$m],
                'observacion_asistencia_id'=>$request->observacion_asistencia_id[$key],
                ]);

        }

        return redirect()->route('asistencia.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asistencia  $asistencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asistencia $asistencia)
    {
        //
    }
}
