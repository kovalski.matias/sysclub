<?php

namespace App\Http\Controllers;

use App\PreguntaEncuesta;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PreguntaEncuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $preguntas=PreguntaEncuesta::all();

        return view("pregunta.index", compact('preguntas'));
    }

    public function guardar(Request $request)
    {
        $pregunta = new PreguntaEncuesta;
        $pregunta->interrogante = $request->get('interrogante');
        $pregunta->save();
        
        return redirect()->route('pregunta.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PreguntaEncuesta  $preguntaEncuesta
     * @return \Illuminate\Http\Response
     */
    public function show(PreguntaEncuesta $preguntaEncuesta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PreguntaEncuesta  $preguntaEncuesta
     * @return \Illuminate\Http\Response
     */
    public function edit(PreguntaEncuesta $preguntaEncuesta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PreguntaEncuesta  $preguntaEncuesta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PreguntaEncuesta $pregunta)
    {
        $pregunta->interrogante=$request->get('interrogante');

        $pregunta->update();

        return redirect()->route('pregunta.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PreguntaEncuesta  $preguntaEncuesta
     * @return \Illuminate\Http\Response
     */
    public function destroy(PreguntaEncuesta $preguntaEncuesta)
    {
        //
    }
}
