<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Persona;
use App\Estado;
use App\Empresa;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests\UserUpdateFormRequest;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:listar usuarios|crear usuario|editar usuario|eliminar usuario', ['only' => ['index','store']]);
         $this->middleware('permission:crear usuario', ['only' => ['create','store']]);
         $this->middleware('permission:editar usuario', ['only' => ['edit','update']]);
         $this->middleware('permission:eliminar usuario', ['only' => ['delete']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $estados=Estado::all(); //obteneme todas las categorias
        $empresa=Empresa::all();

        $desde=$request->desde;
        $hasta=$request->hasta;


        if(count($request->all())>=1) //si existe algun request(es decir, si uso el "Filtrar")
        {
            //dd($request->all());
            $sql = User::select('users.*'); //inicio la consulta sobre una determinada tabla

            if($request->desde)
            {
                $sql = $sql->whereDate('created_at','>=',$request->desde);
            }            
            if($request->hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$request->hasta);
            }
            if($request->estado_id)
            {
                $sql = $sql->whereEstado_id($request->estado_id); //creo la consulta y almaceno en "sql"
            }


            $users=$sql->orderBy('created_at','desc')->get(); //ejecuto la consulta
            $estado_id=$request->estado_id; //mantengo el id de la categoria del tiquet


        }
        else //si nunca filtre, (si no existió request)
        {

            $estado_id=null; //en el select2 que me aparesca " -- Todas las Categorias --"
            $users=User::whereEstado_id(1)->orderBy('created_at','desc')->get(); //que me obtenga directamente todos los grupos


        }/*
        if($request->exists('pdf'))
        {
            $colspan = 0;

        return $this->exportPdf($users, $desde, $hasta, $empresa);
        }*/

        return view('user.index',[
            "users"             =>  $users,         //los grupos de trabajo
            "desde"             =>  $desde,
            "hasta"             =>  $hasta,
            "estado_id"         =>  $estado_id, //si los id son identicos que me mantenga el valor
            "estados"           =>  $estados, //si los id son identicos que me mantenga el valor

            ]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$pacientes=Paciente::where('historia_clinica',false)->get();
        //$personas=Persona::all();
        $personas=Persona::where('cuenta',false)->get();
        $roles=DB::table('roles')->get();
        return view("user.create", [
            "personas"   =>  $personas,
            "roles"      =>  $roles
            ]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //Asociar los datos del personal a la cuenta del usuario
        $user = new User;
        $user->name=$request->get('name');
        $user->email=$request->get('email');
        $user->password=bcrypt($request->password);
        $user->persona_id=$request->persona_id;
        //Persona::find($user->persona_id)->update(['cuenta'=>true]);
        $persona=Persona::find($user->persona_id);
        $persona->cuenta=true;
        $persona->update();

        if($request->file('foto')){

            $image = $request->foto;
            $image->move(public_path() . '/imagenes/perfil/', $image->getClientOriginalName());
            $user->foto = $image->getClientOriginalName();

        }

        $user->save();

        //Paciente::find($historia_clinica->paciente_id)->update(['historia_clinica'=>true]);
        Persona::find($user->persona_id)->update(['cuenta'=>true]);

        //Sincronizar al usuario creado con el rol y guardarlo
        $user->roles()->sync($request->get('roles'));

        Session::flash('store_user','El usuario '.$user->name. ' ha sido registrdo con éxito');
        return Redirect::to('user');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles=DB::table('roles')->get();
        //$roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();
        return view('user.edit',compact('user','roles','userRole'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user=User::findOrFail($id);
        $user->password=bcrypt($request->password);
        $user->name=$request->get('name');
        $user->email=$request->get('email');

        $user->update();

        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->assignRole($request->input('roles'));

        Session::flash('update_user','La persona '.$user->name. ' ha sido actualizada con éxito');
        return Redirect::to('user');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $user=User::find($id);
        $nombre=$user->persona->nombreCompleto();
        $user->update(['estado_id'=>2]);
        $persona=Persona::find($user->persona_id);
        $persona->cuenta=false;
        $persona->update();
        return redirect()->route('user.index')/*->withMessage("El user $nombre ha sido dado de baja correctamente")*/;

    }

    /*public function eliminados()
    {
        $userEliminados=userClinica::where('habilitado',false)->get();
        return view("user.eliminados",compact('userEliminados'));

    }*/


    public function restaurar($id)
    {
        $userRestaurar = User::find($id);
        $userRestaurar->update(['estado_id'=>1]);
        $persona=Persona::find($userRestaurar->persona_id);
        $persona->cuenta=true;
        $persona->update();
        return redirect()->route('user.index');

    }
}
