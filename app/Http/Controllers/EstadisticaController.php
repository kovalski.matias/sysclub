<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Disciplina;
use App\Agenda;
use App\Actividad;
use App\Membresia;
use App\Movimiento;
use App\IngresoDisciplina;
use App\Empresa;
use Barryvdh\DomPDF\Facade as PDF;

class EstadisticaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('estadistica.index');
    }

    public function exportPdfRecaudacion($disciplinas, $desde, $hasta, $empresa)
    {
       
        $pdf = PDF::loadView('estadistica.estadisticarecaudacionpdf',[
            "disciplinas"   =>  $disciplinas,
            "desde"         =>  $desde,
            "hasta"         =>  $hasta,
            "empresa"       =>  $empresa,
            ]);


        $pdf->setPaper('a4','letter');            

        return $pdf->stream('recaudacion-list-pdf');
                
    }

    public function recaudacion(Request $request)
    {
        $empresa=Empresa::all();

        $dis=Disciplina::all();
        foreach ($dis as $d) {
            $d->recaudacion=0;
            $d->update();
        }

        $desde=$request->desde;
        $hasta=$request->hasta;

        if(count($request->all())>1)
        {
            //dd($request->all());
            $sql = Movimiento::select('movimientos.*');

            if($desde)
            {
                //dd($request->desde);
                $sql = $sql->whereDate('created_at','>=',$request->desde);
            }            
            if($request->hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$request->hasta);
            }
            
            $movimientos=$sql->orderBy('created_at','desc')->get();
            //dd($movimientos);
            foreach ($movimientos as $m) {
                $d=Disciplina::find($m->disciplina_id);
                $d->recaudacion= $d->recaudacion + $m->monto;
                $d->update();
            }
    
            $disciplinas=Disciplina::orderBy('recaudacion','DESC')->get();

        }
        else
        {
            $movimientos=Movimiento::all();

            foreach ($movimientos as $m) {
                $d=Disciplina::find($m->disciplina_id);
                $d->recaudacion= $d->recaudacion + $m->monto;
                $d->update();
            }
    
            $disciplinas=Disciplina::orderBy('recaudacion','DESC')->get();

        }
        //parametros para el pdf
        if($request->exists('pdf'))
        {
            $colspan = 0;

        return $this->exportPdfRecaudacion($disciplinas, $desde, $hasta, $empresa);
        }

        return view('estadistica.recaudacion',[
            "disciplinas"           =>  $disciplinas,
            "desde"                 =>  $desde,
            "hasta"                 =>  $hasta,

            ]);
    }



    public function horarios()
    {

        
        $month = now()->format('m');
        $year = now()->format('Y');
        
        $agendasLunes=Agenda::whereLunes('1')->get();
        $agendasMartes=Agenda::whereMartes('2')->get();
        $agendasMiercoles=Agenda::whereMiercoles('3')->get();
        $agendasJueves=Agenda::whereJueves('4')->get();
        $agendasViernes=Agenda::whereViernes('5')->get();
        $agendasSabado=Agenda::whereSabado('6')->get();

        //$agendas=Agenda::all();
        //$grafico[][][]= array();
            $lunesmañana=0;$lunestarde=0;$lunesnoche=0;
            $martesmañana=0;$martestarde=0;$martesnoche=0;
            $miercolesmañana=0;$miercolestarde=0;$miercolesnoche=0;
            $juevesmañana=0;$juevestarde=0;$juevesnoche=0;
            $viernesmañana=0;$viernestarde=0;$viernesnoche=0;
            $sabadomañana=0;$sabadotarde=0;$sabadonoche=0;

        foreach ($agendasLunes as $agenda) {
            $hi = (int)$agenda->hora_inicial;
            $hf = (int)$agenda->hora_final;
            $membresias=Membresia::whereAgenda_id($agenda->id)->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();
            foreach ($membresias as $membresia) {
                if (($hi>=0)&&($hf<=12)) {
                    $lunesmañana+=1;
                }elseif(($hi>=13)&&($hf<=19)) {
                    $lunestarde+=1;
                }elseif(($hi>19)){
                    $lunesnoche+=1;
                }
            } //array_push($grafico,$mañana,$tarde,$noche);
            
        }

        foreach ($agendasMartes as $agenda) {
            $hi = (int)$agenda->hora_inicial;
            $hf = (int)$agenda->hora_final;
            $membresias=Membresia::whereAgenda_id($agenda->id)->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();
            foreach ($membresias as $membresia) {
                if (($hi>=0)&&($hf<=12)) {
                    $martesmañana+=1;
                }elseif(($hi>=13)&&($hf<=19)) {
                    $martestarde+=1;
                }elseif(($hi>19)){
                    $martesnoche+=1;
                }
            } //array_push($grafico,$mañana,$tarde,$noche);
            
        }

        foreach ($agendasMiercoles as $agenda) {
            $hi = (int)$agenda->hora_inicial;
            $hf = (int)$agenda->hora_final;
            $membresias=Membresia::whereAgenda_id($agenda->id)->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();
            foreach ($membresias as $membresia) {
                if (($hi>=0)&&($hf<=12)) {
                    $miercolesmañana+=1;
                }elseif(($hi>=13)&&($hf<=19)) {
                    $miercolestarde+=1;
                }elseif(($hi>19)){
                    $miercolesnoche+=1;
                }
            } //array_push($grafico,$mañana,$tarde,$noche);
            
        }

        foreach ($agendasJueves as $agenda) {
            $hi = (int)$agenda->hora_inicial;
            $hf = (int)$agenda->hora_final;
            $membresias=Membresia::whereAgenda_id($agenda->id)->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();
            foreach ($membresias as $membresia) {
                if (($hi>=0)&&($hf<=12)) {
                    $juevesmañana+=1;
                }elseif(($hi>=13)&&($hf<=19)) {
                    $juevestarde+=1;
                }elseif(($hi>19)){
                    $juevesnoche+=1;
                }
            } //array_push($grafico,$mañana,$tarde,$noche);
            
        }

        foreach ($agendasViernes as $agenda) {
            $hi = (int)$agenda->hora_inicial;
            $hf = (int)$agenda->hora_final;
            $membresias=Membresia::whereAgenda_id($agenda->id)->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();
            foreach ($membresias as $membresia) {
                if (($hi>=0)&&($hf<=12)) {
                    $viernesmañana+=1;
                }elseif(($hi>=13)&&($hf<=19)) {
                    $viernestarde+=1;
                }elseif(($hi>19)){
                    $viernesnoche+=1;
                }
            } //array_push($grafico,$mañana,$tarde,$noche);
            
        }

        foreach ($agendasSabado as $agenda) {
            $hi = (int)$agenda->hora_inicial;
            $hf = (int)$agenda->hora_final;
            $membresias=Membresia::whereAgenda_id($agenda->id)->whereMonth('updated_at',$month)->whereYear('updated_at',$year)->get();
            foreach ($membresias as $membresia) {
                if (($hi>=0)&&($hf<=12)) {
                    $sabadomañana+=1;
                }elseif(($hi>=13)&&($hf<=19)) {
                    $sabadotarde+=1;
                }elseif(($hi>19)){
                    $sabadonoche+=1;
                }
            } 
            
        }
        
    return view('estadistica.horarios',[
        "lunesmañana"           =>  $lunesmañana,
        "lunestarde"                 =>  $lunestarde,
        "lunesnoche"                 =>  $lunesnoche,
        "martesmañana"           =>  $martesmañana,
        "martestarde"                 =>  $martestarde,
        "martesnoche"                 =>  $martesnoche,
        "miercolesmañana"           =>  $miercolesmañana,
        "miercolestarde"                 =>  $miercolestarde,
        "miercolesnoche"                 =>  $miercolesnoche,
        "juevesmañana"           =>  $juevesmañana,
        "juevestarde"                 =>  $juevestarde,
        "juevesnoche"                 =>  $juevesnoche,
        "viernesmañana"           =>  $viernesmañana,
        "viernestarde"                 =>  $viernestarde,
        "viernesnoche"                 =>  $viernesnoche,
        "sabadomañana"           =>  $sabadomañana,
        "sabadotarde"                 =>  $sabadotarde,
        "sabadonoche"                 =>  $sabadonoche,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
