<?php

namespace App\Http\Controllers;

use App\Socio;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PersonaCreateRequest;
use App\Persona;
use App\Estado;
use App\Disciplina;
use App\TipoDocumento;
use App\Sexo;
use App\Domicilio;
use App\Ciudad;
use App\Pais;
use App\Provincia;
use App\Barrio;
use App\Calle;
use App\User;
use App\Empresa;
use App\Membresia;
use App\Actividad;
use App\Agenda;
use App\Cuota;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;
use App\Mail\EnviarCorreo;
use Illuminate\Support\Facades\Hash;


class SocioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:listar socios|crear socio|editar socio|eliminar socio', ['only' => ['index','store']]);
        $this->middleware('permission:crear socio', ['only' => ['create','store']]);
        $this->middleware('permission:editar socio', ['only' => ['edit','update']]);
        $this->middleware('permission:eliminar socio', ['only' => ['delete']]);
    }

    public function exportPdf($socios, $desde, $hasta, $empresa)
    {
       
        $pdf = PDF::loadView('socio.pdf',[
            "socios"        =>  $socios,
            "desde"         =>  $desde,
            "hasta"         =>  $hasta,
            "empresa"       =>  $empresa,

            ]);

        $pdf->setPaper('a4','letter');
            

        return $pdf->stream('socio-list-pdf');
                
    }

    public function encontrarProvincia(Request $request)
	{
	    $provincias=Provincia::select('nombre','id')
			->where('pais_id',$request->id)
            ->get();
        return response()->json($provincias);
    }

    public function encontrarCiudad(Request $request)
	{
	    $ciudades=Ciudad::select('nombre','id')
			->where('provincia_id',$request->id)
            ->get();
        return response()->json($ciudades);
    }

    public function encontrarBarrio(Request $request)
	{
	    $barrios=Barrio::select('nombre','id')
			->where('ciudad_id',$request->id)
            ->get();
        return response()->json($barrios);
    }


    public function encontrarCalle(Request $request)
	{
	    $calles=Calle::select('*','id')
			->where('barrio_id',$request->id)
            ->get();
        return response()->json($calles);
	}

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $socios=Socio::all();
        $estados=Estado::all();
        $empresa=Empresa::all();

        $desde=$request->desde;
        $hasta=$request->hasta;
        
        if(count($request->all())>1)
        {
            //dd($request->all());
            $sql = Socio::select('socios.*');

            if($request->desde)
            {
                $sql = $sql->whereDate('created_at','>=',$request->desde);
            }            
            if($request->hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$request->hasta);
            }
            if($request->estado_id)
            {
                $sql = $sql->whereEstado_id($request->estado_id); //creo la consulta y almaceno en "sql"
            }
            
            $socios=$sql->orderBy('created_at','desc')->get();
            $estado_id=$request->estado_id;


        }
        else
        {

            $estado_id=null;
            $socios=Socio::whereEstado_id(1)->orderBy('created_at','desc')->get();


        }
        if($request->exists('pdf'))
        {
            $colspan = 0;

        return $this->exportPdf($socios, $desde, $hasta, $empresa);
        }

        return view('socio.index',[
            "socios"                =>  $socios,
            "desde"                 =>  $desde,
            "hasta"                 =>  $hasta,
            "estado_id"             =>  $estado_id, //si los id son identicos que me mantenga el valor
            "estados"               =>  $estados,
            "empresa"               =>  $empresa,

            ]);      

    }

    public function cuotasSocio(){


        
        return view();
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $tipo_documentos=TipoDocumento::all();
        $sexos=Sexo::all();
        $paises=Pais::all();

        return view("socio.create", [
            "tipo_documentos"   =>  $tipo_documentos,
            "sexos"             =>  $sexos,
            "paises"            =>  $paises,
            ]);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PersonaCreateRequest $request)
    {
        $domicilio = new Domicilio;
        $domicilio->direccion = $request->get('direccion');
        $domicilio->departamento = $request->get('departamento');
        $domicilio->piso = $request->get('piso');
        $domicilio->ciudad_id = $request->get('ciudad_id');
        $domicilio->save();
        
        //Creo los datos de la persona
        $persona = new Persona;
        /*$persona->nombres=strtoupper($request->get('nombres'));
        $persona->apellidos=strtoupper($request->get('apellidos'));*/
        $persona->nombres=$request->get('nombres');
        $persona->apellidos=$request->get('apellidos');
        $persona->tipo_documento_id=$request->get('tipo_documento_id');
        $persona->documento=$request->get('documento');
        $persona->fecha_nacimiento=$request->get('fecha_nacimiento');
        $persona->sexo_id=$request->get('sexo_id');
        $persona->cuenta=false;
        $persona->email=$request->get('email');
        $persona->telefono=$request->get('telefono');
        $persona->domicilio_id=$domicilio->id;
        $persona->save();

        $socio = new Socio;
        $socio->persona_id = $persona->id;
        $socio->estado_id = 1;
        $socio->save();

        $user = new User;
        $user->name=$request->get('nombres');
        $user->email=$request->get('email');
        $user->password=substr( md5(microtime()), 1, 8);
        $mensaje=$user;
        Mail::to($persona->email)->send(new EnviarCorreo($mensaje));
        $user->persona_id=$persona->id;
        $user->estado_id= 1;
        
        /*$c=decrypt($user->password);
        dd($c);*/
        $user->password=bcrypt($user->password);
        $user->save();

        $roles=Role::all();
        $user->assignRole([$roles->name='socio']);

        return redirect()->route('socio.index');

    }
    
    public function paisCreate(Request $request)
    {
        $input = $request->all();

        $pais = Pais::create([
            "nombre" => $input["nombre"],
        ]);
        return response()->json($pais);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {

        

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {

        $socio=Socio::findOrFail($id);
        $sexos=Sexo::all();
        $paises=Pais::all();
        $provincias=Provincia::all();
        $ciudades=Ciudad::all();

        return view("socio.edit",["socio"=>$socio,"sexos"=>$sexos,"paises"=>$paises,"provincias"=>$provincias,"ciudades"=>$ciudades]);

    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $socio=Socio::findOrFail($id);
        $persona=Persona::findOrFail($socio->persona_id);
        $persona->nombres=$request->get('nombres');
        $persona->apellidos=$request->get('apellidos');
        $persona->documento=$request->get('documento');
        $persona->fecha_nacimiento=$request->get('fecha_nacimiento');
        $persona->sexo_id=$request->get('sexo_id');
        $persona->email=$request->get('email');
        $persona->telefono=$request->get('telefono');

        $domicilio=Domicilio::find($persona->domicilio_id);
        $domicilio->direccion=$request->get('direccion');
        $domicilio->departamento = $request->get('departamento');
        $domicilio->piso = $request->get('piso');
        $domicilio->ciudad_id=$request->get('ciudad_id');
        $domicilio->update();

        $persona->update();
        $socio->update();

        return redirect()->route('socio.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $socio=socio::find($id);
        $nombre=$socio->persona->nombreCompleto();
        $socio->update(['estado_id'=>2]);


        //buscar membresias activas y darlas de baja
        $membresias=Membresia::wheresocio_id($socio->id)->whereRenovacion(1)->get();

        foreach ($membresias as $m) {
            $cuotas = Cuota::whereMembresia_id($m->id)->where('estado_cuota_id','!=',2)->get();
            foreach ($cuotas as $cuota) {
                $cuota->estado_cuota_id=4;
                $cuota->update();
            }
            $m->renovacion=0;
            $m->update();
            $agenda=Agenda::find($m->agenda_id);
            $agenda->cupo_actividad++;
            $agenda->update();
        }
        


        return redirect()->route('socio.index')/*->withMessage("El socio $nombre ha sido dado de baja correctamente")*/;

    }


    public function restaurar($id)
    {
        $socioRestaurar = socio::find($id);
        $socioRestaurar->update(['estado_id'=>1]);
        return redirect()->route('socio.index');

    }
}
