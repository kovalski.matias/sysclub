<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\TipoDocumento;

class TipoDocumentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipodocumentos = TipoDocumento::all();
        return view('tipodocumento.index',["tipodocumentos"=> $tipodocumentos]);
    
    }
    
    public function guardar(Request $request)
    {
        $tipodocumento = new TipoDocumento;
        $tipodocumento->definicion = $request->get('definicion');
        $tipodocumento->abreviatura = $request->get('abreviatura');
        $tipodocumento->save();
        
        return redirect()->route('tipodocumento.index');
    }

    public function update(Request $request, TipoDocumento $tipodocumento)
    {
        $tipodocumento->abreviatura=$request->get('abreviatura');
        $tipodocumento->definicion=$request->get('definicion');

        $tipodocumento->update();

        return redirect()->route('tipodocumento.index');
    }
}
