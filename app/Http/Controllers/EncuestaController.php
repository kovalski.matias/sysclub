<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Encuesta;
use App\PreguntaEncuesta;
use App\DetalleEncuesta;
use App\Membresia;
use App\Actividad;
use App\Agenda;
use App\Personal;

class EncuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $encuesta=Encuesta::findOrFail($id);
        $preguntas=PreguntaEncuesta::all();
        //dd($preguntas);

        return view("encuesta.edit",compact('encuesta','preguntas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $encuesta = Encuesta::findOrFail($id);
        $encuesta->estado_encuesta_id=2;

        $encuesta->update();

        $preguntas=PreguntaEncuesta::all();
        //dd($detalles_asistencias);
        //$m=$membresias;
        //dd($request);

        $cantidad = $preguntas->count();
        //dd($cantidad);
        $cont = 0;
        $valor = 0;

        while ($cont < $cantidad)
        {
            $detalleEncuesta = new DetalleEncuesta();
            $detalleEncuesta->pregunta_encuesta_id = $request->pregunta_id[$cont];
            //dd($cont);
            //$detalleEncuesta->observacion = $request->respuesta[$cont];
            $detalleEncuesta->encuesta_id = $encuesta->id;

            $detalleEncuesta->respuesta=$request->respuesta[$detalleEncuesta->pregunta_encuesta_id];

            $detalleEncuesta->save();
            $cont=$cont+1;
            $valor = $valor + $detalleEncuesta->respuesta;

        }

        $valor=$valor/$cantidad;

        $encuesta->valoracion=$valor;
        $encuesta->estado_encuesta_id=2;
        $encuesta->update();

        $personal=Personal::find($encuesta->personal_id);
        $personal->valoracion=$encuesta->promedioValoracion();
        $personal->update();

        return redirect()->route('perfil.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
