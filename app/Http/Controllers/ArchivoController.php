<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Archivo;
use App\Socio;
use Barryvdh\DomPDF\Facade as PDF;
class ArchivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $archivos=Archivo::whereSocio_id($id)->orderBy('created_at','desc')->get();
        $socio=Socio::find($id);

        return view("archivo.index", compact('archivos','socio'));
    }

    public function guardar(Request $request)
    {
        $n=Archivo::count() + 1;

        $archivo = new Archivo;
        $archivo->socio_id = $request->get('socio_id');
        $archivo->descripcion = $request->get('descripcion');
        $archivo->codigo=str_pad($n, 10, '0', STR_PAD_LEFT);
        
        if($request->file('documento')){

            //dd('carga archivo');
            $image = $request->documento;
            $image->move(public_path() . '/imagenes/fichas/', $image->getClientOriginalName());
            $archivo->documento = $image->getClientOriginalName();

        }
        
        
        
        $archivo->save();
        
        
        return redirect()->route('socio.index');
    }

    public function downloadFile($file){
        $pathtoFile = public_path().'/imagenes/fichas/'.$file;
        return response()->download($pathtoFile);
    }

    public function crearFichaPDF()
    {

        $ficha_medica="Ficha Médica";
        $pdf = PDF::loadView('ficha_medica.pdf',[
            "ficha_medica"   =>  $ficha_medica
            ]);

        $pdf->setPaper('a4','letter');



        return $pdf->stream('ficha-medica.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
