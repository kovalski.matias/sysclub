<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Cuota;
use App\Membresia;
use Carbon\Carbon;
use App\Cobro;
use Redirect;
use MercadoPago\Payment;
use MercadoPago\SDK;



class MercadoPagoController extends Controller
{
    public function generarPreference($id){
        try{
        \MercadoPago\SDK::setAccessToken('TEST-6372037545540326-102120-6fa94893369d9c6806a76fc693cff90b-661819817');
        $preference= new \MercadoPago\Preference();
        $cuota = Cuota::find($id);
        
        /*$cobro = new Cobro;
        $cobro->cuota_id = $cuota->id;
        $cobro->monto    = $cuota->monto;
        $cobro->fecha    = now();*/
        
        $preference->external_reference=$cuota->id;
        
        $monto=$cuota->monto;
        
        $items = [];
        
        $itemAux = new \MercadoPago\Item();
        $itemAux->title = 'Pago suscripción mensual';
        $itemAux->quantity = 1;
        $itemAux->unit_price = $monto+($monto*0.10);
        
        array_push($items,$itemAux);
        
        $preference->items = $items;
        
        //url de retorno al sistema;
        $preference->back_urls = array(
            "success" => "http://sysfit.test/mercadopago/notificacion/".$cuota->id."/pagoexitoso",
            "failure" => "http://sysfit.test/mercadopago/notificacion/".$cuota->id."/pagofallado",
        );
        
        $preference->auto_return = "approved";
        
        //$preference->notification_url = route('mercadopago.notificacion');

        $preference->binary_mode = true;
        $preference->save();
        //dd($preference->collector_id);
    
        $url = $preference->init_point;
    
        return Redirect::to($url);

/*
curl -X POST \
-H "Content-Type: application/json" \
-H 'Authorization: Bearer TEST-3314731271428854-092100-4e711ea7c64a21cfdc307e1be13fb797-457324398' \
"https://api.mercadopago.com/users/test_user" \
-d '{"site_id":"MLA"}'

Vendedor
{"id":661819817,"nickname":"TETE9408115","password":"qatest6603","site_status":"active","email":"test_user_40585766@testuser.com"} TEST-6372037545540326-102120-6fa94893369d9c6806a76fc693cff90b-661819817

Comprador
{"id":661816034,"nickname":"TETE4580974","password":"qatest9867","site_status":"active","email":"test_user_34897039@testuser.com"}
*/
        
        }
        catch(\Illuminate\Database\QueryException $e){
            
        }

    }

    /*public function ipn_mercadopago()
    {

        //SDK::setAccessToken('TEST-6372037545540326-102120-6fa94893369d9c6806a76fc693cff90b-661819817');

        if ( $_GET['topic'] === 'payment')
        {
            $payment = Payment::find_by_id( $_GET['id'] );

            if ( ! array_key_exists('id', $payment->order) )
                return response()->noContent(400, []);

            //$merchant_order = MerchantOrder::find_by_id($payment->order->id);
        }
            if ($payment->status == 'approved'){
                //$monto += $payment->transaction_amount;
                $cobro = new Cobro;
                $cobro->cuota_id = $cuota->id;
                $cobro->monto    = $cuota->monto;
                $cobro->fecha    = now();
                $cobro->preference_id = $preference->id;
                $cobro->save();
                $cuota->estado_cuota_id = 2;
                $cuota->update();
        }
    }*/
}
/*
try{
    User::destroy($id);
    $user->update();
    Session::flash('delete_user','El Usuario '.$user->name. ' ha sido dado de baja correctamente');
    return Redirect::to('users/gestion');
}
catch(\Illuminate\Database\QueryException $e){
    Session::flash('delete_user_error','El Usuario '.$user->name. ' no puede ser eliminado');
    return Redirect::to('users/gestion');

}*/
