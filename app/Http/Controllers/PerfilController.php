<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Membresia;
use App\Socio;
use App\Persona;
use App\Cuota;
use App\Encuesta;
use Illuminate\Support\Facades\Auth as Auth;
use Spatie\Permission\Models\Role;
use Hash;
use Illuminate\Support\Facades\Hash as FacadesHash;

class PerfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(Auth::User()->id);
        return view('perfil.index',compact('user'));
    }

    public function membresias()
    {
        $user=Auth::User();
        $persona=Persona::find($user->persona_id);
        $socio=Socio::wherePersona_id($persona->id)->first();
        $membresias = Membresia::whereSocio_id($socio->id)->get();
        return view('perfil.membresias',compact('membresias'));
    }


    public function cuotas($id)
    {
        $m=Membresia::findOrFail($id);
        $cuotas = Cuota::whereMembresia_id($m->id)->orderBy('created_at','desc')->get();
        
        return view('perfil.cuotas',compact('cuotas'));
    }

    

    public function encuestas($id)
    {

        $m=Membresia::findOrFail($id);
        $encuestas = Encuesta::whereMembresia_id($m->id)->whereEstado_encuesta_id(1)->orderBy('created_at','desc')->get();

        return view('perfil.encuestas',compact('encuestas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = User::find(Auth::User()->id);
        if(empty($user)){
          //Flash::error('mensaje error');
            return redirect()->back();
        }
        return view('perfil.edit')->with('user', $user);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

    $usuario = User::find(Auth::User()->id);
    $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$usuario->id,
            'password' => 'same:confirm-password',
        ]);
    if (!empty($request['password'])) {
        $request['password'] = FacadesHash::make($request['password']);
    }

    if (!empty($request['roles'])) {
        //DB::table('model_has_roles')->where('model_id',$id)->delete();
        $usuario->assignRole($request->input('roles'));
    }

    if($request->file('foto')){

        $image = $request->foto;
        $image->move(public_path() . '/imagenes/perfil/', $image->getClientOriginalName());
        $usuario->foto = $image->getClientOriginalName();

    }

    $usuario->update(array_filter($request->all())); 


    return redirect()->route('perfil.index')
                    ->with('success','User updated successfully');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
