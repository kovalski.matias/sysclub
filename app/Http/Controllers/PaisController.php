<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use App\Http\Requests\ArticleRequest;
use App\Http\Requests\PaisCreateRequest;
use App\Pais;

class PaisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paises = Pais::all();
        return view('pais.index',["paises"=> $paises]);
    }

    public function guardar(PaisCreateRequest $request)
    {
        $pais = new Pais;
        $pais->nombre = $request->get('nombre');
        $pais->save();
        
        return redirect()->route('pais.index');
    }
}
