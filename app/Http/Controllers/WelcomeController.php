<?php

namespace App\Http\Controllers;
use App\User;
Use App\Empresa;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{

    public function index()
    {


        $users = User::all();
        $cantUser = $users->count();

        $empresa = Empresa::find(1);

        $dia=\Carbon\Carbon::parse(now())->format('d-m');

        $fecha=\Carbon\Carbon::parse(auth()->user()->persona->fecha_nacimiento)->format('d-m');

        


        return view('welcome',[
            "cantUser"          => $cantUser,
            "dia"               => $dia,
            "fecha"             => $fecha,
            "empresa"           => $empresa,

        ]);
    }

}
