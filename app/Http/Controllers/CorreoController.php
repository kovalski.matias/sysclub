<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\EnviarCorreo;

class CorreoController extends Controller
{
    public function contacto()
    {
        

        return view("email.contacto");

    }
    
    public function store(){

        $mensaje = request()->validate([
            'nombre' => 'required',
            'email' => 'required|email',
            'asunto' => 'required',
            'mensaje' => 'required'
        ],
        [
            'nombre.required' => __('I need your name')
        ]);
        
        Mail::to('kovalski.matias@gmail.com')->send(new EnviarCorreo($mensaje));

        return "validado";
    }
}
