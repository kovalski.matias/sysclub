<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Espacio;
use App\Disciplina;
use DB;

class EspacioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $espacios = Espacio::all();
        $disciplinas = Disciplina::all();
        /*$espacioDisciplina = DB::table("disciplina_espacio")->where("disciplina_espacio.espacio_id",$id)
            ->pluck('disciplina_espacio.disciplina_id','disciplina_espacio.disciplina_id')
            ->all();*/

        return view('espacio.index',["espacios"=> $espacios, "disciplinas"=> $disciplinas/*, "espacioDisciplina"=>$espacioDisciplina*/]);
    
    }
    
    public function guardar(Request $request)
    {
        $espacio = new Espacio;
        $espacio->nombre = $request->get('nombre');
        $espacio->descripcion = $request->get('descripcion');
        $espacio->cupo_espacio = $request->get('cupo_espacio');
        //$espacio->disciplina_id = $request->get('disciplina_id');
        $espacio->save();

        //Sincronizar al personal creado con la disciplina y guardarlo
        $espacio->disciplinas()->sync($request->get('disciplinas'));
        
        return redirect()->route('espacio.index');
    }

    public function update(Request $request, Espacio $espacio)
    {
        $espacio->nombre=$request->get('nombre');
        $espacio->cupo_espacio=$request->get('cupo_espacio');
        $espacio->descripcion=$request->get('descripcion');

        $espacio->update();

        //$personal->syncDisciplinas($request->input('disciplinas'));
        $espacio->disciplinas()->sync($request->get('disciplinas'));

        return redirect()->route('espacio.index');
    }
}
