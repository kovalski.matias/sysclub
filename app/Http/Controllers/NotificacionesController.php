<?php

namespace App\Http\Controllers;

//use App\Apuesta;
use App\Cuota;
use App\Cobro;
use App\EstadoCuota;
use App\Encuesta;
use App\EstadoEncuesta;
use App\Membresia;
use App\Agenda;
use App\Personal;
use App\Disciplina;
use App\Movimiento;
use App\TipoMovimiento;
use App\User;
use Illuminate\Support\Facades\Auth as Auth;
/*use App\FuncionesTrait;
use App\Movimiento;*/
use Illuminate\Http\Request;
use MercadoPago\MerchantOrder;
use MercadoPago\Payment;
use MercadoPago\SDK;

class NotificacionesController extends Controller
{

    public function pagoexitoso($id){
        $cuota=Cuota::findOrFail($id);

        return view("mercadopago.notificacion", ["cuota"=>$cuota]);
    }
    
    
    public function ipn_mercadopago($id)
    {
        $cuota=Cuota::findOrFail($id);
        $cobro = new Cobro;
        $cobro->cuota_id = $cuota->id;
        $cobro->monto    = $cuota->monto;
        $cobro->forma_pago_id = 2;
        $cobro->estado_cobro_id = 1;
        $cobro->fecha    = now();
        //$cobro->preference_id = $preference->id;
        $cobro->save();
        $cuota->estado_cuota_id = 2;
        $cuota->update();

        $membresia=Membresia::findOrFail($cuota->membresia_id);
        $membresia->ends_at = now()->addDays(30);
        $membresia->update();

        $agenda=Agenda::findOrFail($membresia->agenda_id);
        $personal=Personal::findOrFail($agenda->personal_id);
        $disciplina=Disciplina::findOrFail($agenda->disciplina_id);

        $encuesta = new Encuesta();
        $encuesta->membresia_id=$membresia->id;
        $encuesta->personal_id=$personal->id;
        $encuesta->estado_encuesta_id=1;
        $encuesta->save();

        $m = new Movimiento();
        $m->monto=$cobro->monto;
        $m->fecha = now();
        $m->tipo_movimiento_id=1;
        $m->subtipo_movimiento_id=1;
        $m->disciplina_id=$disciplina->id;
        $m->cuota_id=$cuota->id;
        $m->save();
        
        $user = User::find(Auth::User()->id);
        return view('perfil.index',compact('user'));
    }
    
    public function pagofallado($id){

        return view("mercadopago.fallado");
    }
}

