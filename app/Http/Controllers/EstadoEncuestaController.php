<?php

namespace App\Http\Controllers;

use App\EstadoEncuesta;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EstadoEncuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EstadoEncuesta  $estadoEncuesta
     * @return \Illuminate\Http\Response
     */
    public function show(EstadoEncuesta $estadoEncuesta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EstadoEncuesta  $estadoEncuesta
     * @return \Illuminate\Http\Response
     */
    public function edit(EstadoEncuesta $estadoEncuesta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EstadoEncuesta  $estadoEncuesta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstadoEncuesta $estadoEncuesta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstadoEncuesta  $estadoEncuesta
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstadoEncuesta $estadoEncuesta)
    {
        //
    }
}
