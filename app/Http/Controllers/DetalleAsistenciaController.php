<?php

namespace App\Http\Controllers;

use App\DetalleAsistencia;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DetalleAsistenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DetalleAsistencia  $detalleAsistencia
     * @return \Illuminate\Http\Response
     */
    public function show(DetalleAsistencia $detalleAsistencia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DetalleAsistencia  $detalleAsistencia
     * @return \Illuminate\Http\Response
     */
    public function edit(DetalleAsistencia $detalleAsistencia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DetalleAsistencia  $detalleAsistencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetalleAsistencia $detalleAsistencia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DetalleAsistencia  $detalleAsistencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetalleAsistencia $detalleAsistencia)
    {
        //
    }
}
