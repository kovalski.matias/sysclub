<?php

namespace App\Http\Controllers;

use App\Actividad;
use App\Agenda;
use App\Socio;
use App\Disciplina;
use App\Inscripcion;
use App\Empresa;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ActividadController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:listar actividades|crear actividad|editar actividad|eliminar actividad', ['only' => ['index','store']]);
        $this->middleware('permission:crear actividad', ['only' => ['create','store']]);
        $this->middleware('permission:editar actividad', ['only' => ['edit','update']]);
        $this->middleware('permission:eliminar actividad', ['only' => ['destroy']]);
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $socios=Socio::whereEstado_id(1)->get();
        $disciplinas=Disciplina::all();
        $agendas=Agenda::all();
        $empresa=Empresa::all();

        $desde=$request->desde;
        $hasta=$request->hasta;
        
        if(count($request->all())>1)
        {
            //dd($request->all());
            $sql = Agenda::select('agenda.*');

            if($request->desde)
            {
                $sql = $sql->whereDate('created_at','>=',$request->desde);
            }            
            if($request->hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$request->hasta);
            }
            if($request->disciplina_id)
            {
                $sql = $sql->whereDisciplina_id($request->disciplina_id); //creo la consulta y almaceno en "sql"
            }
            
            $agendas=$sql->orderBy('created_at','desc')->get();

            $disciplina_id=$request->disciplina_id;


        }
        else
        {

            $disciplina_id=null;
            $agendas=Agenda::orderBy('created_at','desc')->get();


        }/*
        if($request->exists('pdf'))
        {
            $colspan = 0;

        return $this->exportPdf($actividades, $desde, $hasta, $empresa);
        }*/

        return view('actividad.index',[
            "socios"                =>  $socios,
            "agendas"               =>  $agendas,
            "desde"                 =>  $desde,
            "hasta"                 =>  $hasta,
            "disciplina_id"         =>  $disciplina_id, //si los id son identicos que me mantenga el valor
            "disciplinas"           =>  $disciplinas,
            "empresa"               =>  $empresa,

            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Actividad  $actividad
     * @return \Illuminate\Http\Response
     */
    public function show(Actividad $actividad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Actividad  $actividad
     * @return \Illuminate\Http\Response
     */
    public function edit(Actividad $actividad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Actividad  $actividad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Actividad $actividad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Actividad  $actividad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Actividad $actividad)
    {
        //
    }
}
