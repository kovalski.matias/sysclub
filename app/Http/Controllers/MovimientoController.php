<?php

namespace App\Http\Controllers;

use App\Movimiento;
use App\TipoMovimiento;
use App\SubTipoMovimiento;
use App\Empresa;
use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MovimientoController extends Controller
{
    
    function __construct()
    {
        $this->middleware('permission:listar movimientos|crear movimiento|editar movimiento|eliminar movimiento', ['only' => ['index','store']]);
        $this->middleware('permission:crear movimiento', ['only' => ['create','store']]);
        $this->middleware('permission:editar movimiento', ['only' => ['edit','update']]);
        $this->middleware('permission:eliminar movimiento', ['only' => ['destroy']]);
    }
    
    
    public function exportPdf($movimientos, $desde, $hasta, $empresa)
    {
       
        $pdf = PDF::loadView('movimiento.pdf',[
            "movimientos"   =>  $movimientos,
            "desde"         =>  $desde,
            "hasta"         =>  $hasta,
            "empresa"       =>  $empresa,

            ]);

        $pdf->setPaper('a4','letter');
            

        return $pdf->stream('movimientos-list-pdf');
                
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $movimientos=Movimiento::all();
        $subtipomovimientos=SubTipoMovimiento::all();
        $tipomovimientos=TipoMovimiento::all();
        $empresa=Empresa::all();

        $desde=$request->desde;
        $hasta=$request->hasta;
        
        if(count($request->all())>1)
        {
            //dd($request->all());
            $sql = Movimiento::select('movimientos.*');

            if($request->desde)
            {
                $sql = $sql->whereDate('created_at','>=',$request->desde);
            }            
            if($request->hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$request->hasta);
            }
            if($request->tipo_movimiento_id)
            {
                $sql = $sql->whereTipo_movimiento_id($request->tipo_movimiento_id); //creo la consulta y almaceno en "sql"
            }
            if($request->subtipo_movimiento_id)
            {
                $sql = $sql->whereSubtipo_movimiento_id($request->subtipo_movimiento_id); //creo la consulta y almaceno en "sql"
            }
            
            $movimientos=$sql->orderBy('created_at','desc')->get();
            $tipo_movimiento_id=$request->tipo_movimiento_id;
            $subtipo_movimiento_id=$request->tipo_movimiento_id;


        }
        else
        {

            $tipo_movimiento_id=null;
            $subtipo_movimiento_id=null;

            $hoy=date('Y-m-d');
            //$movimientos=Movimiento::whereTipo_movimiento_id(1)->orderBy('created_at','desc')->get();
            $movimientos=Movimiento::whereFecha($hoy)->orderBy('created_at','desc')->get();
            //dd($hoy);
        }
        if($request->exists('pdf'))
        {
            $colspan = 0;

        return $this->exportPdf($movimientos, $desde, $hasta, $empresa);
        }

        return view('movimiento.index',[
            "movimientos"                =>  $movimientos,
            "tipomovimientos"            =>  $tipomovimientos,
            "subtipomovimientos"         =>  $subtipomovimientos,
            "desde"                      =>  $desde,
            "hasta"                      =>  $hasta,
            "tipo_movimiento_id"         =>  $tipo_movimiento_id, //si los id son identicos que me mantenga el valor
            "subtipo_movimiento_id"      =>  $subtipo_movimiento_id,
            "empresa"               =>  $empresa,

            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movimiento  $movimiento
     * @return \Illuminate\Http\Response
     */
    public function show(Movimiento $movimiento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movimiento  $movimiento
     * @return \Illuminate\Http\Response
     */
    public function edit(Movimiento $movimiento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movimiento  $movimiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movimiento $movimiento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movimiento  $movimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movimiento $movimiento)
    {
        //
    }
}
