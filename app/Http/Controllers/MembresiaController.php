<?php

namespace App\Http\Controllers;

use App\Membresia;
use App\Actividad;
use App\Disciplina;
use App\Agenda;
use App\Socio;
use App\Cuota;
use App\Estado;
use App\Empresa;
use App\EstadoMembresia;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MembresiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:listar membresias|guardar membresia|editar membresia|eliminar membresia', ['only' => ['index','store']]);
        $this->middleware('permission:guardar membresia', ['only' => ['create','store']]);
        $this->middleware('permission:editar membresia', ['only' => ['edit','update']]);
        $this->middleware('permission:eliminar membresia', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $agendas=Agenda::all();
        $membresias=Membresia::all();
        $empresa=Empresa::all();
        $estadomembresias=EstadoMembresia::all();

        $desde=$request->desde;
        $hasta=$request->hasta;
        
        if(count($request->all())>1)
        {
            //dd($request->all());
            $sql = Membresia::select('membresias.*');

            if($request->desde)
            {
                $sql = $sql->whereDate('created_at','>=',$request->desde);
            }            
            if($request->hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$request->hasta);
            }
            if($request->actividad_id)
            {
                $sql = $sql->whereAgenda_id($request->agenda_id); //creo la consulta y almaceno en "sql"
            }
            if($request->estado_membresia_id)
            {
                $sql = $sql->whereEstado_membresia_id($request->estado_membresia_id); //creo la consulta y almaceno en "sql"
            }
            
            $membresias=$sql->orderBy('created_at','desc')->get();

            $agenda_id=$request->agenda_id;
            $estado_membresia_id=$request->estado_membresia_id;


        }
        else
        {

            $agenda_id=null;
            $estado_membresia_id=null;
            $membresias=Membresia::whereEstado_membresia_id(1)->orderBy('created_at','desc')->get();


        }/*
        if($request->exists('pdf'))
        {
            $colspan = 0;

        return $this->exportPdf($membresias, $desde, $hasta, $empresa);
        }*/

        return view('membresia.index',[
            "membresias"            =>  $membresias,
            "desde"                 =>  $desde,
            "hasta"                 =>  $hasta,
            "agenda_id"             =>  $agenda_id, //si los id son identicos que me mantenga el valor
            "agendas"               =>  $agendas,
            "estado_membresia_id"   =>  $estado_membresia_id,
            "estadomembresias"      =>  $estadomembresias,
            "empresa"               =>  $empresa,

            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $socios=Socio::whereEstado_id(1)->get();
        $agendas=Agenda::all();

        return view("membresia.create", [
            "socios"             =>  $socios,
            "agendas"            =>  $agendas,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function guardar(Request $request)
    {
        $n=Membresia::count() + 1;

        $membresia = new Membresia;
        $membresia->agenda_id = $request->get('agenda_id');
        $membresia->socio_id = $request->get('socio_id');
        $membresia->precio = $membresia->agenda->monto;
        $membresia->estado_membresia_id = 1;
        $membresia->renovacion = true;
        $membresia->ends_at = now()->addDays(30);
        $membresia->codigo=str_pad($n, 10, '0', STR_PAD_LEFT);
        $membresia->save();


        $agenda=Agenda::find($membresia->agenda_id);
        $agenda->cupo_actividad=$agenda->cupo_actividad-1;
        $agenda->recaudacion += $agenda->monto;
        $agenda->update();

        $agenda=Agenda::find($membresia->agenda_id)->first();

        $d=Disciplina::find($agenda->disciplina_id);
        $d->recaudacion+=$agenda->monto;
        $d->update();
        

        
        return redirect()->route('actividad.index');
    }

    public function cancelar(Membresia $membresia)
    {
        $membresia->estado_membresia_id=2;
        $membresia->update();

        $cuotas=Cuota::whereMembresia_id($membresia->id)->where('estado_cuota_id', '!=', 2)->get();

        foreach ($cuotas as $cuota) {
            $cuota->estado_cuota_id=4;
            $cuota->update();
        }

        $agenda=Agenda::findOrFail($membresia->agenda_id);
        $agenda->cupo_actividad+=1;
        $agenda->update();

        return redirect()->route('membresia.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Membresia  $membresia
     * @return \Illuminate\Http\Response
     */
    public function show(Membresia $membresia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Membresia  $membresia
     * @return \Illuminate\Http\Response
     */
    public function edit(Membresia $membresia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Membresia  $membresia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Membresia $membresia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Membresia  $membresia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Membresia $membresia)
    {
        //
    }
}
