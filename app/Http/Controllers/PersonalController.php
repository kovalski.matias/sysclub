<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PersonaCreateRequest;
use App\Personal;
use App\Persona;
use App\User;
use App\Estado;
use App\Disciplina;
use App\TipoDocumento;
use App\Sexo;
use App\Domicilio;
use App\Ciudad;
use App\Pais;
use App\Provincia;
use App\Barrio;
use App\Calle;
use App\Empresa;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;
use App\Mail\EnviarCorreo;
use Illuminate\Support\Facades\Hash;
use DB;

class PersonalController extends Controller
{   
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:listar personal|crear personal|editar personal|eliminar personal', ['only' => ['index','store']]);
        $this->middleware('permission:crear personal', ['only' => ['create','store']]);
        $this->middleware('permission:editar personal', ['only' => ['edit','update']]);
        $this->middleware('permission:eliminar personal', ['only' => ['delete']]);
    }

    public function exportPdf($personals, $desde, $hasta, $empresa)
    {
       
        $pdf = PDF::loadView('personal.pdf',[
            "personals"   =>  $personals,
            "desde"       =>  $desde,
            "hasta"       =>  $hasta,
            "empresa"     =>  $empresa,

            ]);

        $pdf->setPaper('a4','letter');
            

        return $pdf->stream('personal-list-pdf');
                
    }
    
    public function encontrarProvincia(Request $request)
	{
	    $provincias=Provincia::select('nombre','id')
			->where('pais_id',$request->id)
            ->get();
        return response()->json($provincias);
    }

    public function encontrarCiudad(Request $request)
	{
	    $ciudades=Ciudad::select('nombre','id')
			->where('provincia_id',$request->id)
            ->get();
        return response()->json($ciudades);
    }

    public function encontrarBarrio(Request $request)
	{
	    $barrios=Barrio::select('nombre','id')
			->where('ciudad_id',$request->id)
            ->get();
        return response()->json($barrios);
    }


    public function encontrarCalle(Request $request)
	{
	    $calles=Calle::select('*','id')
			->where('barrio_id',$request->id)
            ->get();
        return response()->json($calles);
	}

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $personals=Personal::all();
        $estados=Estado::all();
        $empresa=Empresa::all();

        $desde=$request->desde;
        $hasta=$request->hasta;
        
        if(count($request->all())>1)
        {
            //dd($request->all());
            $sql = Personal::select('personal.*');

            if($request->desde)
            {
                $sql = $sql->whereDate('created_at','>=',$request->desde);
            }            
            if($request->hasta)
            {
                $sql = $sql->whereDate('created_at','<=',$request->hasta);
            }
            if($request->estado_id)
            {
                $sql = $sql->whereEstado_id($request->estado_id); //creo la consulta y almaceno en "sql"
            }
            
            $personals=$sql->orderBy('created_at','desc')->get();
            $estado_id=$request->estado_id;


        }
        else
        {

            $estado_id=null;
            $personals=Personal::whereEstado_id(1)->orderBy('created_at','desc')->get();


        }
        if($request->exists('pdf'))
        {
            $colspan = 0;

        return $this->exportPdf($personals, $desde, $hasta, $empresa);
        }

        return view('personal.index',[
            "personals"             =>  $personals,
            "desde"                 =>  $desde,
            "hasta"                 =>  $hasta,
            "estado_id"             =>  $estado_id, //si los id son identicos que me mantenga el valor
            "estados"               =>  $estados,
            "empresa"               =>  $empresa,

            ]);       

    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $tipo_documentos=TipoDocumento::all();
        $sexos=Sexo::all();
        $paises=Pais::all();
        $disciplinas=Disciplina::all();

        return view("personal.create", [
            "tipo_documentos"   =>  $tipo_documentos,
            "sexos"             =>  $sexos,
            "paises"            =>  $paises,
            "disciplinas"       =>  $disciplinas,
            ]);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PersonaCreateRequest $request)
    {
        $domicilio = new Domicilio;
        $domicilio->direccion = $request->get('direccion');
        $domicilio->departamento = $request->get('departamento');
        $domicilio->piso = $request->get('piso');
        $domicilio->ciudad_id = $request->get('ciudad_id');
        $domicilio->save();
        
        //Creo los datos de la persona
        $persona = new Persona;
        $persona->nombres=$request->get('nombres');
        $persona->apellidos=$request->get('apellidos');
        $persona->tipo_documento_id=$request->get('tipo_documento_id');
        $persona->documento=$request->get('documento');
        $persona->fecha_nacimiento=$request->get('fecha_nacimiento');
        $persona->sexo_id=$request->get('sexo_id');
        $persona->cuenta=false;
        $persona->email=$request->get('email');
        $persona->telefono=$request->get('telefono');
        $persona->domicilio_id=$domicilio->id;
        $persona->save();


        $personal = new Personal;
        $personal->persona_id = $persona->id;
        $personal->estado_id = 1;
        $personal->save();

        $user = new User;
        $user->name=$request->get('nombres');
        $user->email=$request->get('email');
        $user->password=substr( md5(microtime()), 1, 8);
        $mensaje=$user;
        Mail::to($persona->email)->send(new EnviarCorreo($mensaje));
        $user->persona_id=$persona->id;
        $user->estado_id= 1;
        
        /*$c=decrypt($user->password);
        dd($c);*/
        $user->password=bcrypt($user->password);
        $user->save();

        $roles=Role::all();
        $user->assignRole([$roles->name='Instructor']);


         //Sincronizar al personal creado con la disciplina y guardarlo
        $personal->disciplinas()->sync($request->get('disciplinas'));
    
        return redirect()->route('personal.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {

        

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {

        $personal=Personal::findOrFail($id);
        $sexos=Sexo::all();
        $tipo_documentos=TipoDocumento::all();
        $paises=Pais::all();
        $provincias=Provincia::all();
        $ciudades=Ciudad::all();
        $disciplina = Disciplina::all();
        $personalDisciplina = DB::table("disciplina_personal")->where("disciplina_personal.personal_id",$id)
            ->pluck('disciplina_personal.disciplina_id','disciplina_personal.disciplina_id')
            ->all();

        return view("personal.edit",["personal"=>$personal,"sexos"=>$sexos,"tipo_documentos"=>$tipo_documentos,"paises"=>$paises,"provincias"=>$provincias,"ciudades"=>$ciudades,"disciplina"=>$disciplina,"personalDisciplina"=>$personalDisciplina]);

    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $personal=Personal::findOrFail($id);
        $persona=Persona::findOrFail($personal->persona_id);
        $persona->nombres=$request->get('nombres');
        $persona->apellidos=$request->get('apellidos');
        $persona->documento=$request->get('documento');
        $persona->fecha_nacimiento=$request->get('fecha_nacimiento');
        $persona->sexo_id=$request->get('sexo_id');
        $persona->tipo_documento_id=$request->get('tipo_documento_id');
        $persona->email=$request->get('email');
        $persona->telefono=$request->get('telefono');

        $domicilio=Domicilio::find($persona->domicilio_id);
        $domicilio->direccion=$request->get('direccion');
        $domicilio->departamento = $request->get('departamento');
        $domicilio->piso = $request->get('piso');
        $domicilio->ciudad_id=$request->get('ciudad_id');
        $domicilio->update();

        $persona->update();
        $personal->update();

        //$personal->syncDisciplinas($request->input('disciplinas'));
        $personal->disciplinas()->sync($request->get('disciplina'));

        return redirect()->route('personal.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $personal=Personal::find($id);
        $nombre=$personal->persona->nombreCompleto();
        $personal->update(['estado_id'=>2]);
        return redirect()->route('personal.index')/*->withMessage("El personal $nombre ha sido dado de baja correctamente")*/;

    }

    /*public function eliminados()
    {
        $personalEliminados=PersonalClinica::where('habilitado',false)->get();
        return view("personal.eliminados",compact('personalEliminados'));

    }*/


    public function restaurar($id)
    {
        $personalRestaurar = Personal::find($id);
        $personalRestaurar->update(['estado_id'=>1]);
        return redirect()->route('personal.index');

    }
}
