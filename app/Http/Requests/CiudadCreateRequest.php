<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CiudadCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'         => 'required',//cuando creo
            'codigo_postal'  => 'required|unique:ciudades,codigo_postal,except,id',//cuando creo

        ];
    }

    public function messages()
    {
        return [
            'nombre.required'            =>  'El nombre de la ciudad es requerido',
            'codigo_postal.required'     =>  'El nombre del país es requerido',
            'codigo_postal.unique'       =>  'Ya existe una ciudad con este codigo postal',

        ];
    }
}
