<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaisCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'  => 'required|unique:paises,nombre,except,id',//cuando creo
            /*
                si quiero editar seria:
                'nombre' => "required|unique:paises,nombre,{$this->pais->id}"

            */

        ];
    }

    public function messages()
    {
        return [
            'nombre.required'     =>  'El nombre del país es requerido',
            'nombre.unique'       =>  'Ya existe un país con ese nombre',

        ];
    }
}
