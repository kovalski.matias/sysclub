<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonaCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'apellidos'        => 'required',
            'nombres'          => 'required',
            'documento'        => 'required|unique:personas,documento,except,id',
            'email'            => 'required|unique:personas,email,except,id',
            'telefono'         => 'required',
            'fecha_nacimiento' => 'required',
            'direccion'        => 'required',
            'sexo_id'          => 'required',
            'tipo_documento_id'=> 'required',
            

        ];
    }

    public function messages()
    {
        return [
            'apellidos.required'            =>  'El apellido de la persona es requerido',
            'nombres.required'              =>  'El nombre de la persona es requerido',
            'documento.required'            =>  'Ya existe otra persona con el mismo número de documento',
            'email.required'                =>  'El email de la persona es requerido',
            'email.unique'                  =>  'Ya existe otra persona con el mismo email',
            'telefono.required'             =>  'El telefono de la persona es requerido',
            'direccion.required'            =>  'La dirección de la persona es requerida',
            'fecha_nac.required'            =>  'La fecha de nacimiento de la persona es requerida',
            'sexo_id.required'              =>  'El sexo de la persona es requerido',
            'tipo_documento_id.required'    =>  'El sexo de la persona es requerido',


        ];
    }
}
