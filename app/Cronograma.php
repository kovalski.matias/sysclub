<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cronograma extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "agenda_id",
        "fecha",
        "hora_inicial",
        "hora_final",
        'domingo',
        'lunes',
        'martes',
        'miercoles',
        'jueves',
        'viernes',
        'sabado',
        "descripcion",
    ];

    public function agenda()
    {
        return $this->belongsTo(Agenda::class);
    }

    public function asistencia()
    {
        return $this->hasOne(Asistencia::class);
    }
}
