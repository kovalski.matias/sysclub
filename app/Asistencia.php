<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Asistencia extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cronograma_id', 'fecha', 'estado_asistencia_id'
    ];

    /*public function actividad(){
        return $this->belongsTo(Actividad::class);
    }*/

    public function cronograma(){
        return $this->belongsTo(Cronograma::class);
    }

    public function estadoAsistencias(){
        return $this->belongsTo(EstadoAsistencia::class);
    }

    public function detallesasistencias()
    {
        return $this->hasMany(DetalleAsistencia::class);
    }
    
}
