<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DetalleEncuesta extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'respuesta', 'encuesta_id', 'pregunta_encuesta_id'
    ];

    public function encuestas()
    {
        return $this->belongsTo(Encuesta::class);
    }

    public function preguntasEncuestas()
    {
        return $this->belongsTo(PreguntaEncuesta::class);
    }
}
