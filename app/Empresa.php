<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Empresa extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $fillable = ['razon_social', 'fecha_creacion', 'domicilio_id', 'telefono', 'email', 'logo', 'cuit'];

    public function domicilio()
    {
        return $this->belongsTo(Domicilio::class);
    }
}
