<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Domicilio extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps=true;

    protected $fillable = [
        'piso',
        'departamento',
        'direccion',
        'ciudad_id',
        //'persona_id'
        ];

    protected $table = 'domicilios';

    public function ciudad()
    {
        return $this->belongsTo(Ciudad::class);
    }

    /*public function persona()
    {
        return $this->belongsTo(Persona::class);
    }*/

    public function persona()
    {
        return $this->hasOne(Persona::class);
    }

    public function empresa()
    {
        return $this->hasOne(Empresa::class);
    }


}
