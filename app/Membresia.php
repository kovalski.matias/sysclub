<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Membresia extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'codigo', 'precio', 'ends_at', 'renovacion', 'agenda_id', 'socio_id', 'estado_membresia_id'
    ];

    public function cuotas()
    {
        return $this->hasMany(Cuota::class);
    }

    public function actividad()
    {
        return $this->belongsTo(Actividad::class);
    }

    public function agenda()
    {
        return $this->belongsTo(Agenda::class);
    }

    public function socio()
    {
        return $this->belongsTo(Socio::class);
    }

    public function estado()
    {
        return $this->belongsTo(Estado::class);
    }

    public function estadoMembresia()
    {
        return $this->belongsTo(EstadoMembresia::class);
    }

    public function detallesasistencias()
    {
        return $this->hasMany(DetalleAsistencia::class);
    }

    public function encuestas()
    {
        return $this->hasMany(Encuesta::class);
    }

    public function confirmacionesasistencias()
    {
        return $this->hasMany(ConfirmarAsistencia::class);
    }
    
}
