<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class TipoMovimiento extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'nombre', 'descripcion'
    ];

    protected $table = 'tipo_movimientos';

    public function movimientos()
    {
        return $this->hasMany(Movimiento::class);
    }

    public function subtipos()
    {
        return $this->hasMany(SubTipoMovimiento::class);
    }
}
