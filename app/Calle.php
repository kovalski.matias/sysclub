<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Calle extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps=true;

    protected $fillable = [
        'nombre',//imagen
        'barrio_id'];

    protected $table = 'calles';

    public function barrio()
    {
        return $this->belongsTo('App\Barrio');
    }

}
