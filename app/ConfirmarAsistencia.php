<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfirmarAsistencia extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'asistencia', 'membresia_id', 'estado', 'fecha'
    ];

    public function membresia()
    {
        return $this->belongsTo(Membresia::class);
    }
}
