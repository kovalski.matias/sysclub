<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Espacio extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre'/*, 'disciplina_id'*/, 'cupo_espacio', 'descripcion'
    ];

    public function actividades()
    {
        return $this->hasMany(Actividad::class);
    }

    public function disciplinas()
    {
        return $this->belongsToMany(Disciplina::class);
    }
    
    public function agendas()
    {
        return $this->hasMany(Agenda::class);
    }

    public function getDisciplinaNames()
    {
        return $this->disciplinas->pluck('nombre');
    }
}
