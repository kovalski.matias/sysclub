<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisciplinaEspacio extends Model
{
    protected $fillable = [
        'disciplina_id', 'espacio_id'
    ];

    protected $table = 'disciplina_espacio';
}
