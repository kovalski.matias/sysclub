<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PreguntaEncuesta extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'interrogante'
    ];

    public function detallesEncuestas()
    {
        return $this->hasMany(DetalleEncuesta::class);
    }
}
