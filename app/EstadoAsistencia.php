<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoAsistencia extends Model
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'nombre'
    ];

    public function asistencias()
    {
        return $this->hasMany(Asistencia::class);
    }
}
