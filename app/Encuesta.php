<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Encuesta extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'membresia_id', 'personal_id', 'estado_encuesta_id', 'valoracion',
    ];

    /*public function actividad(){
        return $this->belongsTo(Actividad::class);
    }*/

    public function personal(){
        return $this->belongsTo(Personal::class);
    }

    public function estadoEncuestas(){
        return $this->belongsTo(EstadoEncuesta::class);
    }

    public function membresia()
    {
        return $this->belongsTo(Membresia::class);
    }


    //Función que calcula el tiempo estimado de realización de un ticket en (dias)
    //toma los ultimos 5 (por eso es que minExperience tiene que ser mayor a 4)
    public function promedioValoracion()
    {
    return ceil(Encuesta::wherePersonal_id($this->personal_id)
            ->whereEstado_encuesta_id(2)
            ->latest('updated_at')
            ->take(3)
            ->get()
            ->avg('valoracion'));             
    }
}
