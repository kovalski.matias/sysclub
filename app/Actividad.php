<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Actividad extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'codigo', 'nombre', 'cupo_actividad', 'monto', 'agenda_id', 'disciplina_id'
    ];
    
    protected $table = 'actividades';

   /* public function disciplina()
    {
        return $this->belongsTo(Disciplina::class);
    }

    public function espacio()
    {
        return $this->belongsTo(Espacio::class);
    }

    public function personal()
    {
        return $this->belongsTo(Personal::class);
    }*/

    public function agenda()
    {
        return $this->belongsTo(Agenda::class);
    }

    public function cuotas()
    {
        return $this->hasMany(Cuota::class);
    }

    public function membresias()
    {
        return $this->hasMany(Membresia::class);
    }

    public function asistencia()
    {
        return $this->hasOne(Asistencia::class);
    }

    public function disciplina()
    {
        return $this->belongsTo(Disciplina::class);
    }

}
