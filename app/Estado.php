<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


class Estado extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps=true;

    protected $fillable = ['nombre'];

    protected $table = 'estados';

    public function personal()
    {
        return $this->hasMany(Personal::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function socios()
    {
        return $this->hasMany(Socio::class);
    }

}
