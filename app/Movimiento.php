<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Movimiento extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'monto', 'fecha', 'tipo_movimiento_id', 'disciplina_id', 'subtipo_movimiento_id', 'cuota_id'
    ];

    public function subtipo_movimiento()
    {
        return $this->belongsTo(SubTipoMovimiento::class);
    }

    public function tipo_movimiento()
    {
        return $this->belongsTo(TipoMovimiento::class);
    }

    public function disciplina()
    {
        return $this->belongsTo(Disciplina::class);
    }
    
    public function cuota()
    {
        return $this->belongsTo(Cuota::class);
    }
}
