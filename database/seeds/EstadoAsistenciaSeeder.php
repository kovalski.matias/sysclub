<?php

use Illuminate\Database\Seeder;
use App\EstadoAsistencia;

class EstadoAsistenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EstadoAsistencia::create(['nombre' => 'Creada']);
        EstadoAsistencia::create(['nombre' => 'Actualizada']);
        EstadoAsistencia::create(['nombre' => 'Cerrada']);
    }
}
