<?php

use Illuminate\Database\Seeder;
use App\Agenda;

class AgendaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Pilates
        Agenda::create([
            'hora_inicial'         =>  '08:00:00',
            'hora_final'           =>  '08:59:00',
            'domingo'              =>  'NULL',
            'lunes'                =>  '1',
            'martes'               =>  'NULL',
            'miercoles'            =>  '3',
            'jueves'               =>  'NULL',
            'viernes'               =>  '5',
            'sabado'               =>  'NULL',
            'descripcion'          =>  'Pilates Azul',
            'estado'               =>  1,
            'espacio_id'           =>  1, 
            'color_id'             =>  2,
            'disciplina_id'        =>  1, 
            'personal_id'          =>  1,
        ]);
        
        //Pilates
        Agenda::create([
            'hora_inicial'         =>  '08:00:00',
            'hora_final'           =>  '08:59:00',
            'domingo'              =>  'NULL',
            'lunes'                =>  'NULL',
            'martes'               =>  '2',
            'miercoles'            =>  'NULL',
            'jueves'               =>  '4',
            'viernes'              =>  'NULL',
            'sabado'               =>  'NULL',
            'descripcion'          =>  'Pilates Rojo',
            'estado'               =>  1,
            'espacio_id'           =>  1, 
            'color_id'             =>  3,
            'disciplina_id'        =>  1, 
            'personal_id'          =>  1,
        ]);
        
        //Yoga
        Agenda::create([
            'hora_inicial'         =>  '16:00:00',
            'hora_final'           =>  '16:59:00',
            'domingo'              =>  'NULL',
            'lunes'                =>  '1',
            'martes'               =>  'NULL',
            'miercoles'            =>  '3',
            'jueves'               =>  'NULL',
            'viernes'              =>  '5',
            'sabado'               =>  'NULL',
            'descripcion'          =>  'Yoga Verde',
            'estado'               =>  1,
            'espacio_id'           =>  2, 
            'color_id'             =>  4,
            'disciplina_id'        =>  2, 
            'personal_id'          =>  1,
        ]);

        //Yoga
        Agenda::create([
            'hora_inicial'         =>  '16:00:00',
            'hora_final'           =>  '16:59:00',
            'domingo'              =>  'NULL',
            'lunes'                =>  'NULL',
            'martes'               =>  '2',
            'miercoles'            =>  'NULL',
            'jueves'               =>  '4',
            'viernes'              =>  'NULL',
            'sabado'               =>  'NULL',
            'descripcion'          =>  'Yoga Naranja',
            'estado'               =>  1,
            'espacio_id'           =>  2, 
            'color_id'             =>  5,
            'disciplina_id'        =>  2, 
            'personal_id'          =>  1,
        ]);

        //Funcional Inicial
        Agenda::create([
            'hora_inicial'         =>  '19:00:00',
            'hora_final'           =>  '19:59:00',
            'domingo'              =>  'NULL',
            'lunes'                =>  'NULL',
            'martes'               =>  '2',
            'miercoles'            =>  'NULL',
            'jueves'               =>  '4',
            'viernes'              =>  'NULL',
            'sabado'               =>  'NULL',
            'descripcion'          =>  'FInicial Violeta',
            'estado'               =>  1,
            'espacio_id'           =>  3, 
            'color_id'             =>  6,
            'disciplina_id'        =>  3, 
            'personal_id'          =>  1,
        ]);

        //Funcional Intenso
        Agenda::create([
            'hora_inicial'         =>  '19:00:00',
            'hora_final'           =>  '19:59:00',
            'domingo'              =>  'NULL',
            'lunes'                =>  '1',
            'martes'               =>  'NULL',
            'miercoles'            =>  '3',
            'jueves'               =>  'NULL',
            'viernes'              =>  '5',
            'sabado'               =>  'NULL',
            'descripcion'          =>  'FIntensivo Salmón',
            'estado'               =>  1,
            'espacio_id'           =>  4, 
            'color_id'             =>  8,
            'disciplina_id'        =>  4, 
            'personal_id'          =>  1,
        ]);
    }
}
