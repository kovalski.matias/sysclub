<?php

use Illuminate\Database\Seeder;
use App\Personal;

class PersonalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Personal::create(['persona_id' => 1, 'estado_id' => 1, 'valoracion' => 3]);

        Personal::create(['persona_id' => 2, 'estado_id' => 1, 'valoracion' => 4]);

        Personal::create(['persona_id' => 3, 'estado_id' => 1, 'valoracion' => 5]);

    }
}
