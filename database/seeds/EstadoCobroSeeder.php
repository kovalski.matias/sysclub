<?php

use Illuminate\Database\Seeder;
use App\EstadoCobro;

class EstadoCobroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EstadoCobro::create(['nombre' => 'Realizado']);
        EstadoCobro::create(['nombre' => 'Anulado']);
    }
}
