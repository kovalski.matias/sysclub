<?php

use Illuminate\Database\Seeder;
use App\Movimiento;

class MovimientoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'                 =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                              =>  2000, //Masculino
            'disciplina_id'                      =>  1,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  1500, //Masculino
            'disciplina_id'         =>  2,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  1200, //Masculino
            'disciplina_id'         =>  3,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  1000, //Masculino
            'disciplina_id'         =>  4,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  2000, //Masculino
            'disciplina_id'         =>  1,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  1500, //Masculino
            'disciplina_id'         =>  2,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  1200, //Masculino
            'disciplina_id'         =>  3,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  1000, //Masculino
            'disciplina_id'         =>  4,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  2000, //Masculino
            'disciplina_id'         =>  1,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  1500, //Masculino
            'disciplina_id'         =>  2,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  1200, //Masculino
            'disciplina_id'         =>  3,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  1000, //Masculino
            'disciplina_id'         =>  4,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  2000, //Masculino
            'disciplina_id'         =>  1,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  1500, //Masculino
            'disciplina_id'         =>  2,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  1200, //Masculino
            'disciplina_id'         =>  3,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  1000, //Masculino
            'disciplina_id'         =>  4,
        ]);


        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  2000, //Masculino
            'disciplina_id'         =>  1,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  1500, //Masculino
            'disciplina_id'         =>  2,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  1200, //Masculino
            'disciplina_id'         =>  3,
        ]);

        Movimiento::create([
            'subtipo_movimiento_id'              =>  1,
            'tipo_movimiento_id'    =>  1,
            'fecha'                              =>  '2021-01-12',
            'monto'                 =>  1000, //Masculino
            'disciplina_id'         =>  4,
        ]);

        
    }
}
