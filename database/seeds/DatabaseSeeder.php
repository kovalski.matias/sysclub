<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionTableSeeder::class);
        $this->call(EstadoSeeder::class);
        $this->call(EstadoMembresiaSeeder::class);
        $this->call(EstadoCuotaSeeder::class);
        $this->call(EstadoCobroSeeder::class);
        $this->call(PaisSeeder::class);
        $this->call(ProvinciaSeeder::class);
        $this->call(CiudadSeeder::class);
        $this->call(SexoSeeder::class);
        $this->call(TipoDocumentoSeeder::class);
        $this->call(DomicilioSeeder::class);
        $this->call(PersonaSeeder::class);
        $this->call(PersonalSeeder::class);
        $this->call(SocioSeeder::class);
        $this->call(CreateAdminUserSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(RolSeeder::class);
        $this->call(DisciplinaSeeder::class);
        $this->call(PlanSeeder::class);
        $this->call(EspacioSeeder::class);
        $this->call(ColorSeeder::class);
        //$this->call(AgendaSeeder::class);
        $this->call(ActividadSeeder::class);
        $this->call(EmpresaSeeder::class);
        $this->call(RecargoSeeder::class);
        $this->call(TipoMovimientoSeeder::class);
        $this->call(SubTipoMovimientoSeeder::class);
        //$this->call(MovimientoSeeder::class);
        $this->call(EstadoAsistenciaSeeder::class);
        $this->call(EstadoEncuestaSeeder::class);
        $this->call(PreguntaEncuestaSeeder::class);
        $this->call(FormaPagoSeeder::class);
        $this->call(ObservacionAsistenciaSeeder::class);
    }
}
