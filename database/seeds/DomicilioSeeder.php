<?php

use Illuminate\Database\Seeder;
use App\Domicilio;

class DomicilioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Domicilio::create([
            'departamento'  =>  '2000',
            'piso'          =>   '2',
            'direccion'     => 'Lopez y Planes, 500',
            'ciudad_id'     =>  1,
            ]);

        Domicilio::create([
            'departamento'  =>  '654',
            'piso'          =>   '2',
            'direccion'     => 'Las Heras, 1300',
            'ciudad_id'     =>  2,
            ]);

        Domicilio::create([
            'departamento'  =>  '654',
            'piso'          =>   '2',
            'direccion'     => 'Avellaneda, S/N',
            'ciudad_id'     =>  3,
            ]);

            Domicilio::create([
                'departamento'  =>  '2000',
                'piso'          =>   '2',
                'direccion'     => 'Lopez y Planes, 500',
                'ciudad_id'     =>  4,
                ]);
    
            Domicilio::create([
                'departamento'  =>  '654',
                'piso'          =>   '2',
                'direccion'     => 'Las Heras, 1300',
                'ciudad_id'     =>  5,
                ]);
    
            Domicilio::create([
                'departamento'  =>  '654',
                'piso'          =>   '2',
                'direccion'     => 'Avellaneda, S/N',
                'ciudad_id'     =>  6,
                ]);

            Domicilio::create([
                'departamento'  =>  '',
                'piso'          =>  '',
                'direccion'     =>  'Catamarca, ',
                'ciudad_id'     =>  6,
                ]);

            Domicilio::create([
                'departamento'  =>  '',
                'piso'          =>  '',
                'direccion'     =>  'Ayacucho, ',
                'ciudad_id'     =>  6,
                ]);

            Domicilio::create([
                'departamento'  =>  '',
                'piso'          =>  '',
                'direccion'     =>  'Lyon, ',
                'ciudad_id'     =>  6,
                ]);

            Domicilio::create([
                'departamento'  =>  '',
                'piso'          =>  '',
                'direccion'     =>  'Sauce, ',
                'ciudad_id'     =>  6,
                ]);

            Domicilio::create([
                'departamento'  =>  '',
                'piso'          =>  '',
                'direccion'     =>  'La Rioja, ',
                'ciudad_id'     =>  6,
                ]);

            Domicilio::create([
                'departamento'  =>  '',
                'piso'          =>  '',
                'direccion'     =>  'Rivadavia, ',
                'ciudad_id'     =>  6,
                ]);

            Domicilio::create([
                'departamento'  =>  '',
                'piso'          =>  '',
                'direccion'     =>  'San Lorenzo, ',
                'ciudad_id'     =>  6,
                ]);

            Domicilio::create([
                'departamento'  =>  '',
                'piso'          =>  '',
                'direccion'     =>  'Libertador, ',
                'ciudad_id'     =>  6,
                ]);

            Domicilio::create([
                'departamento'  =>  '',
                'piso'          =>  '',
                'direccion'     =>  'Catamarca, ',
                'ciudad_id'     =>  1,
                ]);

    }
}
