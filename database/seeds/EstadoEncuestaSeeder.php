<?php

use Illuminate\Database\Seeder;
use App\EstadoEncuesta;
class EstadoEncuestaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EstadoEncuesta::create(['nombre' => 'Generada']);
        EstadoEncuesta::create(['nombre' => 'Completada']);
    }
}
