<?php

use Illuminate\Database\Seeder;
use App\Persona;

class PersonaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Personal

        Persona::create([
            'documento'         =>  '38063440',
            'nombres'           =>  'Matias Nicolas',
            'apellidos'         =>  'Kovalski',
            'fecha_nacimiento'  =>  '1994-02-27',
            'email'             =>  'admin@email.com',
            'cuenta'            =>  true,
            'tipo_documento_id' =>  1, //Documento Nacional de Identidad
            'sexo_id'           =>  2, //Masculino
            'domicilio_id'      =>  1,
        ]);

        Persona::create([
            'documento'         =>  '37555888',
            'nombres'           =>  'Charly',
            'apellidos'         =>  'Villalba',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1976-05-12',
            'email'             =>  'elcarlosvillalba@gmail.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  2, //Masculino
            'domicilio_id'      =>  2,
        ]);

        Persona::create([
            'documento'         =>  '36555888',
            'nombres'           =>  'Juan',
            'apellidos'         =>  'Musso',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1988-05-12',
            'email'             =>  'juanse@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  2, //Masculino
            'domicilio_id'      =>  3,
        ]);

        //Alumnos

        Persona::create([
            'documento'         =>  '20111222',
            'nombres'           =>  'Nicolas',
            'apellidos'         =>  'Gomez',
            'fecha_nacimiento'  =>  '1990-03-03',
            'email'             =>  'nico@email.com',
            'cuenta'            =>  true,
            'tipo_documento_id' =>  1, //Documento Nacional de Identidad
            'sexo_id'           =>  2, //Masculino
            'domicilio_id'      =>  4,
        ]);

        Persona::create([
            'documento'         =>  '20111333',
            'nombres'           =>  'Carolina',
            'apellidos'         =>  'Perez',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1976-08-15',
            'email'             =>  'caroa@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  1, //Femenino
            'domicilio_id'      =>  5,
        ]);

        Persona::create([
            'documento'         =>  '20111444',
            'nombres'           =>  'Agustina',
            'apellidos'         =>  'Franco',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1988-09-12',
            'email'             =>  'agus@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  1, //Femenino
            'domicilio_id'      =>  6,
        ]);

        Persona::create([
            'documento'         =>  '20111555',
            'nombres'           =>  'Agostina',
            'apellidos'         =>  'Gonzalez',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1988-05-12',
            'email'             =>  'alumn4@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  1, //Femenino
            'domicilio_id'      =>  8,
        ]);

        Persona::create([
            'documento'         =>  '20222666',
            'nombres'           =>  'Maria',
            'apellidos'         =>  'Rosso',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1988-04-12',
            'email'             =>  'alumn5@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  1, //Femenino
            'domicilio_id'      =>  9,
        ]);

        Persona::create([
            'documento'         =>  '20222777',
            'nombres'           =>  'Carla',
            'apellidos'         =>  'Franco',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1988-02-12',
            'email'             =>  'alumn6@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  1, //Femenino
            'domicilio_id'      =>  10,
        ]);

        Persona::create([
            'documento'         =>  '20222888',
            'nombres'           =>  'Gastón',
            'apellidos'         =>  'Darmian',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1988-09-10',
            'email'             =>  'alumn7@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  2, //Masculino
            'domicilio_id'      =>  11,
        ]);

        Persona::create([
            'documento'         =>  '20222999',
            'nombres'           =>  'Agustin',
            'apellidos'         =>  'Peréz',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1988-09-22',
            'email'             =>  'alumn8@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  2, //Masculino
            'domicilio_id'      =>  12,
        ]);

        Persona::create([
            'documento'         =>  '20222000',
            'nombres'           =>  'Damila',
            'apellidos'         =>  'Frets',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1988-09-15',
            'email'             =>  'alumn9@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  1, //Femenino
            'domicilio_id'      =>  13,
        ]);

        Persona::create([
            'documento'         =>  '20333111',
            'nombres'           =>  'Rocío',
            'apellidos'         =>  'Darmian',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1988-08-17',
            'email'             =>  'alumn10@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  1, //Femenino
            'domicilio_id'      =>  14,
        ]);

        Persona::create([
            'documento'         =>  '20333222',
            'nombres'           =>  'Juana',
            'apellidos'         =>  'Franco',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1988-02-18',
            'email'             =>  'alumn11@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  1, //Femenino
            'domicilio_id'      =>  10,
        ]);

        Persona::create([
            'documento'         =>  '20333333',
            'nombres'           =>  'Gastón',
            'apellidos'         =>  'Altamirano',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1988-05-10',
            'email'             =>  'alumn12@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  2, //Masculino
            'domicilio_id'      =>  11,
        ]);

        Persona::create([
            'documento'         =>  '20333444',
            'nombres'           =>  'Agustin',
            'apellidos'         =>  'Gonzalez',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1988-09-28',
            'email'             =>  'alumn13@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  2, //Masculino
            'domicilio_id'      =>  12,
        ]);

        Persona::create([
            'documento'         =>  '20333555',
            'nombres'           =>  'Danila',
            'apellidos'         =>  'Fretes',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1988-09-15',
            'email'             =>  'alumn14@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  1, //Femenino
            'domicilio_id'      =>  13,
        ]);

        Persona::create([
            'documento'         =>  '30821231',
            'nombres'           =>  'Rocío',
            'apellidos'         =>  'Tabarez',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1988-08-17',
            'email'             =>  'alumn15@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  1, //Femenino
            'domicilio_id'      =>  14,
        ]);

        Persona::create([
            'documento'         =>  '20444666',
            'nombres'           =>  'Aurora',
            'apellidos'         =>  'Franco',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1978-02-12',
            'email'             =>  'alumn16@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  1, //Femenino
            'domicilio_id'      =>  10,
        ]);

        Persona::create([
            'documento'         =>  '20444777',
            'nombres'           =>  'Cesar',
            'apellidos'         =>  'Darmian',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1988-09-10',
            'email'             =>  'alumn17@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  2, //Masculino
            'domicilio_id'      =>  11,
        ]);

        Persona::create([
            'documento'         =>  '20444888',
            'nombres'           =>  'Fernando',
            'apellidos'         =>  'Peréz',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1988-09-22',
            'email'             =>  'alumn18@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  2, //Masculino
            'domicilio_id'      =>  12,
        ]);

        Persona::create([
            'documento'         =>  '20444999',
            'nombres'           =>  'Tania',
            'apellidos'         =>  'Fritz',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1989-07-15',
            'email'             =>  'alumn19@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  1, //Femenino
            'domicilio_id'      =>  13,
        ]);

        Persona::create([
            'documento'         =>  '20444000',
            'nombres'           =>  'Rocío',
            'apellidos'         =>  'Baez',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1993-08-17',
            'email'             =>  'alumn20@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  1, //Femenino
            'domicilio_id'      =>  14,
        ]);

        //SuperUsuario

        Persona::create([
            'documento'         =>  '20555111',
            'nombres'           =>  'César',
            'apellidos'         =>  'Tavarez',
            'cuenta'            =>  true,
            'fecha_nacimiento'  =>  '1961-07-09',
            'email'             =>  'super@email.com',
            'tipo_documento_id' =>  1,
            'sexo_id'           =>  2, //Masculino
            'domicilio_id'      =>  15,
        ]);

    }
}
