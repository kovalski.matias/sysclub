<?php

use Illuminate\Database\Seeder;
use App\FormaPago;

class FormaPagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $formapago = FormaPago::create([

        	'nombre' => 'Efectivo',
        	'descripcion' => 'Cobros realizados en efectivo en el centro',
            'logo'                  =>  'efectivo.jpg',

        ]);

        $formapago = FormaPago::create([

        	'nombre' => 'Mercado Pago',
        	'descripcion' => 'Cobros realizados por medio de Mercado Pago',
            'logo'                  =>  'mp.jpg',

        ]);
    }
}
