<?php

use Illuminate\Database\Seeder;
use App\ObservacionAsistencia;

class ObservacionAsistenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ObservacionAsistencia::create(['observacion' => 'Sin observación']);
        ObservacionAsistencia::create(['observacion' => 'Por enfermedad']);
        ObservacionAsistencia::create(['observacion' => 'Llego tarde']);
        ObservacionAsistencia::create(['observacion' => 'Sin aviso']);
        ObservacionAsistencia::create(['observacion' => 'Problemas de horario']);
        ObservacionAsistencia::create(['observacion' => 'Se retiró']);
    }
}
