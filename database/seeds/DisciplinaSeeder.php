<?php

use Illuminate\Database\Seeder;
use App\Disciplina;

class DisciplinaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $disciplina = Disciplina::create([

        	'nombre' => 'Futbol',

        	'Descripcion' => 'Futbol Cancha 11'

        ]);

        $disciplina = Disciplina::create([

        	'nombre' => 'Futbol 5',

        	'Descripcion' => 'Futbol de Salón'

        ]);

        $disciplina = Disciplina::create([

        	'nombre' => 'Voley',

        	'Descripcion' => 'Voley de piso'

        ]);

        $disciplina = Disciplina::create([

        	'nombre' => 'Patin Artistico',

        	'Descripcion' => 'Patinaje Artistico'

        ]);

        $disciplina = Disciplina::create([

        	'nombre' => 'Tenis',

        	'Descripcion' => 'Tenis de polvo de ladrillo'

        ]);
    }
}
