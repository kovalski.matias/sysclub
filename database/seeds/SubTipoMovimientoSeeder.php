<?php

use Illuminate\Database\Seeder;
use App\SubTipoMovimiento;

class SubTipoMovimientoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
            SubTipoMovimiento::create([
                                    'nombre' => 'Cobro Cuota',
                                    'descripcion' => 'Ingreso por cobro de cuotas',
                                    'tipo_movimiento_id' => 1
                                    ]);
            SubTipoMovimiento::create([
                                    'nombre' => 'Devolución',
                                    'descripcion' => 'Devolución por un cobro erroneo',//suplementos nutricionales, etc
                                    'tipo_movimiento_id' => 1
                                    ]);
            /*SubTipoMovimiento::create([
                'nombre' => 'Devolución',
                'descripcion' => 'Devolución',//suplementos nutricionales, etc
                'tipo_movimiento_id' => 1
                ]);*/
}
}
