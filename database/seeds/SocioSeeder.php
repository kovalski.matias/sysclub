<?php

use Illuminate\Database\Seeder;
use App\Socio;

class SocioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Socio::create(['persona_id' => 4, 'estado_id' => 1]);

        Socio::create(['persona_id' => 5, 'estado_id' => 1]);

        Socio::create(['persona_id' => 6, 'estado_id' => 2]);

        Socio::create(['persona_id' => 7, 'estado_id' => 1]);

        Socio::create(['persona_id' => 8, 'estado_id' => 1]);

        Socio::create(['persona_id' => 9, 'estado_id' => 1]);

        Socio::create(['persona_id' => 10, 'estado_id' => 1]);

        Socio::create(['persona_id' => 11, 'estado_id' => 1]);

        Socio::create(['persona_id' => 12, 'estado_id' => 1]);

        Socio::create(['persona_id' => 13, 'estado_id' => 1]);

        Socio::create(['persona_id' => 14, 'estado_id' => 2]);

        Socio::create(['persona_id' => 15, 'estado_id' => 1]);

        Socio::create(['persona_id' => 16, 'estado_id' => 1]);

        Socio::create(['persona_id' => 17, 'estado_id' => 1]);

        Socio::create(['persona_id' => 18, 'estado_id' => 1]);

        Socio::create(['persona_id' => 19, 'estado_id' => 1]);

        Socio::create(['persona_id' => 20, 'estado_id' => 1]);

        Socio::create(['persona_id' => 21, 'estado_id' => 1]);

        Socio::create(['persona_id' => 22, 'estado_id' => 1]);

        Socio::create(['persona_id' => 23, 'estado_id' => 1]);
    }
}
