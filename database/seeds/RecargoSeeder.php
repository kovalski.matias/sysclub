<?php

use Illuminate\Database\Seeder;
use App\Recargo;

class RecargoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Recargo::create(['nombre'     => 'Mercado Pago',
                        'incremento'  => 5, 
                        'descripcion' => 'Al abonar por Mercado Pago']);

        Recargo::create(['nombre'     => 'Cuotas Vencida', 
                        'incremento'  => 10, 
                        'descripcion' => 'Por abonar cuotas vencidas']);
    }
}
