<?php

use Illuminate\Database\Seeder;
use App\Empresa;

class EmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Empresa::create([
            'razon_social'      =>  'Centro Pilates Integral',
            'cuit'              =>  '00-00000000-0',
            'telefono'          =>  '(0000) 00-0000',
            'email'             =>  'pilates@email.com',
            'fecha_creacion'    =>  '2020-08-03',
            'logo'              =>  'logo.png',
            'domicilio_id'      =>  7,
        ]);
    }
}
