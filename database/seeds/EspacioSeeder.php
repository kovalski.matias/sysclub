<?php

use Illuminate\Database\Seeder;
use App\Espacio;

class EspacioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Espacio::create([

            'nombre' => 'Cancha 11',

            'cupo_espacio' => 30,

            'Descripcion' => 'Cancha de pasto'

        ]);

        Espacio::create([

            'nombre' => 'Playon 1',

            'cupo_espacio' => 20,

            'Descripcion' => 'Playon de futbol 5'

        ]);

        Espacio::create([

            'nombre' => 'Cancha de Tenis',

            'cupo_espacio' => 10,

            'Descripcion' => 'Cancha de polvo de ladrillo'

        ]);

    }
}
