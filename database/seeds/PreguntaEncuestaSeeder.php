<?php

use Illuminate\Database\Seeder;
use App\PreguntaEncuesta;

class PreguntaEncuestaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PreguntaEncuesta::create(['interrogante' => '¿comienza puntualmente?']);
        PreguntaEncuesta::create(['interrogante' => 'El modo de impartir la clase del instructor ¿motiva la asistencia?']);
        PreguntaEncuesta::create(['interrogante' => '¿es correcto en el trato con los alumnos?']);
        PreguntaEncuesta::create(['interrogante' => '¿se observa que conoce los ejercicios a realizar?']);
        PreguntaEncuesta::create(['interrogante' => '¿es atento a como está haciendo el ejercicio?']);
        PreguntaEncuesta::create(['interrogante' => '¿considera las lesiones en los alumnos?']);
    }
}
