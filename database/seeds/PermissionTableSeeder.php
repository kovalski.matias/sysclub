<?php


use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Permission;


class PermissionTableSeeder extends Seeder

{

    /**

     * Run the database seeds.

     *

     * @return void

     */

    public function run()

    {

        $permissions = [

            /*'role-list',
            'role-create',
            'role-edit',
            'role-delete',*/

            'listar roles',
            'crear rol',
            'editar rol',
            'eliminar rol',

            'listar personal',
            'crear personal',
            'editar personal',
            'eliminar personal',

            'listar usuarios',
            'crear usuario',
            'editar usuario',
            'eliminar usuario',

            'listar socios',
            'crear socio',
            'editar socio',
            'eliminar socio',

            'listar actividades',
            'crear actividad',
            'editar actividad',
            'eliminar actividad',

            'listar membresias',
            'guardar membresia',
            'editar membresia',
            'eliminar membresia',

            'listar cuotas',
            'cobrar cuota',
            'crear cuota',
            'editar cuota',
            'eliminar cuota',
            'anular cuota',

            'listar agenda',

            'listar movimientos',

        ];

   

        foreach ($permissions as $permission) {

             Permission::create(['name' => $permission]);

        }

    }

}