<?php

use Illuminate\Database\Seeder;
use App\EstadoCuota;

class EstadoCuotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EstadoCuota::create(['nombre' => 'Generada']);
        EstadoCuota::create(['nombre' => 'Pagada']);
        EstadoCuota::create(['nombre' => 'Vencida']);
        EstadoCuota::create(['nombre' => 'Cancelada']);
    }
}
