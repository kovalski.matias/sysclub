<?php

use Illuminate\Database\Seeder;
use App\EstadoMembresia;

class EstadoMembresiaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EstadoMembresia::create(['nombre' => 'Activa']);
        EstadoMembresia::create(['nombre' => 'Inactiva']);
    }
}
