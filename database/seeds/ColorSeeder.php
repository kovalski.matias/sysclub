<?php

use Illuminate\Database\Seeder;
use App\Color;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $color = Color::create([

        	'nombre' => 'Amarillo',

            'valor' => '#FFFF00',
            
            'texto' => '#000000'

        ]);

        $color = Color::create([

        	'nombre' => 'Azul',

        	'valor' => '#0000FF',
            
            'texto' => '#ffffff'

        ]);

        $color = Color::create([

        	'nombre' => 'Rojo',

        	'valor' => '#FF0000',
            
            'texto' => '#ffffff'

        ]);

        $color = Color::create([

        	'nombre' => 'Verde',

        	'valor' => '#0B5345',
            
            'texto' => '#ffffff'

        ]);

        $color = Color::create([

        	'nombre' => 'Naranja',

        	'valor' => '#F4D03F',
            
            'texto' => '#000'

        ]);

        $color = Color::create([

        	'nombre' => 'Violeta',

        	'valor' => '#5B2C6F',
            
            'texto' => '#ffffff'

        ]);

        $color = Color::create([

        	'nombre' => 'Gris',

        	'valor' => '#5B5D5E',
            
            'texto' => '#ffffff'

        ]);

        $color = Color::create([

        	'nombre' => 'Salmón',

        	'valor' => '#FA8072',
            
            'texto' => '#ffffff'

        ]);
    }
}
