<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisciplinaPersonalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disciplina_personal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('disciplina_id');
            $table->unsignedBigInteger('personal_id');
            $table->timestamps();

            $table->foreign('disciplina_id')->references('id')->on('disciplinas');
            $table->foreign('personal_id')->references('id')->on('personal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disciplina_personal');
    }
}
