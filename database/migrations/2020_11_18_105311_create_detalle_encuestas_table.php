<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleEncuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_encuestas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('respuesta');
            $table->unsignedBigInteger('encuesta_id');
            $table->foreign('encuesta_id')->references('id')->on('encuestas')->onDelete('restrict');
            $table->unsignedBigInteger('pregunta_encuesta_id');
            $table->foreign('pregunta_encuesta_id')->references('id')->on('pregunta_encuestas')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_encuestas');
    }
}
