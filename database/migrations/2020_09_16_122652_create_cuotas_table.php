<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuotas', function (Blueprint $table) {
            $table->bigIncrements('id');
            //$table->string('codigo')->unique();
            $table->string('socio');
            $table->string('actividad');
            $table->date('fecha');
            $table->double('monto');
            $table->text('preference_id')->nullable();
            $table->unsignedBigInteger('estado_cuota_id');
            $table->foreign('estado_cuota_id')->references('id')->on('estado_cuotas')->onDelete('restrict');
            $table->unsignedBigInteger('membresia_id');
            $table->foreign('membresia_id')->references('id')->on('membresias')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuotas');
    }
}
