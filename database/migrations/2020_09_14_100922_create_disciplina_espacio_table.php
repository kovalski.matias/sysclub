<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisciplinaEspacioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disciplina_espacio', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('disciplina_id');
            $table->unsignedBigInteger('espacio_id');
            $table->foreign('disciplina_id')->references('id')->on('disciplinas');
            $table->foreign('espacio_id')->references('id')->on('espacios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disciplina_espacio');
    }
}
