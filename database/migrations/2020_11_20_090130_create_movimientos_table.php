<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimientos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('monto');
            $table->date('fecha');
            $table->unsignedBigInteger('cuota_id')->nullable();
            $table->foreign('cuota_id')->references('id')->on('cuotas')->onDelete('restrict');
            $table->unsignedBigInteger('disciplina_id')->nullable();
            $table->foreign('disciplina_id')->references('id')->on('disciplinas')->onDelete('restrict');
            $table->unsignedBigInteger('tipo_movimiento_id');
            $table->foreign('tipo_movimiento_id')->references('id')->on('tipo_movimientos')->onDelete('restrict');
            $table->unsignedBigInteger('subtipo_movimiento_id');
            $table->foreign('subtipo_movimiento_id')->references('id')->on('sub_tipo_movimientos')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimientos');
    }
}
