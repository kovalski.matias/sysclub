<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha_inicial')->nullable();
            $table->date('fecha_final')->nullable();
            $table->string('hora_inicial');
            $table->string('hora_final');
            $table->string('domingo');
            $table->string('lunes');
            $table->string('martes');
            $table->string('miercoles');
            $table->string('jueves');
            $table->string('viernes');
            $table->string('sabado');
            $table->string('descripcion');
            $table->integer('estado');
            $table->double('monto');
            $table->integer('cupo_actividad');
            $table->double('recaudacion')->nullable();
            $table->unsignedBigInteger('espacio_id');
            $table->foreign('espacio_id')->references('id')->on('espacios')->onDelete('restrict');
            $table->unsignedBigInteger('color_id');
            $table->foreign('color_id')->references('id')->on('colores')->onDelete('restrict');
            $table->unsignedBigInteger('disciplina_id');
            $table->foreign('disciplina_id')->references('id')->on('disciplinas')->onDelete('restrict');
            $table->unsignedBigInteger('personal_id');
            $table->foreign('personal_id')->references('id')->on('personal')->onDelete('restrict');
            $table->timestamps();
        });
    }

        //"espacio_id",
        

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda');
    }
}
