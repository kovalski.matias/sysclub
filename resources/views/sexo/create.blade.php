@extends('layouts.admin')
  <!-- Extiende de layout -->

@section('content')

<style>
    .select2-container .select2-selection {
	line-height: 1.6 !important;
	height: 2.375rem !important;
	border-radius: 3px !important;
    }

</style>

<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h3>Registrar Organización</h3>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/sexo">Indice de tipos de sexo</a></li>
            <li class="breadcrumb-item active">Registrar Tipo de Sexo</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
    
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        @include('errors.request')
        @include('empresa.mensaje')
        {!!Form::open(array('url'=>'sexo','method'=>'POST','autocomplete'=>'off','files' => true,))!!}
        {{Form::token()}}

        <div class="card card-dark">
            <div class="card-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="nomrazon_socialbres"> Definicion</label>
                        <input type="string"name="definicion"maxlength="30"value="{{old('definicion')}}"class="form-control"
                            placeholder="Ingrese la definicion..."title="Introduzca la Definicion">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="cuit">Abreviatura</label>
                        <input type="string"name="abreviatura"maxlength="30"value="{{old('abreviatura')}}"class="form-control"
                            placeholder="Ingrese la abreviatura..."title="Introduzca la abreviatura">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group" style="text-align:center">
                        <button title="Limpiar" class="btn btn-danger" type="reset"><i class="fa fa-eraser"></i> Limpiar</button>
                        <button title="Guardar" id="confirmar" class="btn btn-primary" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
</div>

{!!Form::close()!!}

@push('scripts')

@endpush

@endsection

