{{--ventanita modal cuando se haga clic en eliminar--}}


<div class="modal fade modal-slide-in-right"
    aria-hidden="true"
    role="dialog"
    tabindex="-1"
    id="modal-show-{{$socio->id}}">



    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="">
                <h3 class="modal-title" style="color: black"><i style="color: black" class="" aria-hidden="true"></i> Detalle del Socio</h3>
                @if($socio->persona->user->foto == null)
                    <img src="{{ asset('imagenes/perfil/default.png')}}" width="150px" class="img-circle elevation-2" alt="User Image">
                @else
                    <img src="{{ asset('imagenes/perfil/'.$socio->persona->user->foto)}}" width="150px" class="img-circle elevation-2" alt="User Image">
                @endif</h3>
                <div class="modal-body">
                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>Apellido</th>
                                <td>{{ $socio->persona->apellidos }}</td>
                            </tr>
                            <tr>
                                <th>Nombres</th>
                                <td>{{ $socio->persona->nombres }}</td>
                            </tr>
                                <th>Documento</th>
                                <td>{{ $socio->persona->documento }}</td>
                            </tr>
                            <tr>
                                <th>Sexo</th>
                                <td>{{ $socio->persona->sexo->definicion }}</td>
                            </tr>
                            <tr>
                                <th>Domicilio</th>
                                <td>{{ $socio->persona->direccion() }}</td>
                            </tr>
                            <tr>
                                <th>Fecha de nacimiento</th>
                                <td>{{Carbon\Carbon::parse($socio->persona->fecha_nacimiento)->format('d/m/Y') }} ({{Carbon\Carbon::parse($socio->persona->fecha_nacimiento)->age }} años)</td>
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>

    </div>


</div>

