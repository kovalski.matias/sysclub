@extends('layouts.admin')
  <!-- Extiende de layout -->

@section('content')
<style>
    .select2-container .select2-selection {
	line-height: 1.6 !important;
	height: 2.375rem !important;
	border-radius: 3px !important;
    }

</style>
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h3>Editar Datos del Socio</h3>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            @can('listar socios')
            <li class="breadcrumb-item"><a href="/socio">Indice de Socio</a></li>
            @endcan
            <li class="breadcrumb-item active">Editar Socio</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
    
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        @include('errors.request')
        @include('socio.mensaje')
        {!!Form::model($socio, ['method'=>'PATCH','route'=>['socio.update',$socio->id]])!!}
        {{Form::token()}}

        <div class="card card-dark">
            <div class="card-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                            <label for="nombres">Nombres</label>
                            <input type="string"name="nombres"value="{{ $socio->persona->nombres}}"class="form-control"
                                title="nombre de la persona" onkeypress="return soloLetras(event)">
                        </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                            <label for="apellidos"> Apellido </label>
                            <input type="string"name="apellidos" value="{{ $socio->persona->apellidos }}" class="form-control"
                                title="apellido de la persona" onkeypress="return soloLetras(event)">
                        </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="documento">Documento</label>
                        <input type="integer"name="documento"value="{{ $socio->persona->documento }}" class="form-control"
                            title="documento de la persona" onkeypress="return soloNumeros(event)">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="fecha_nacimiento">Fecha de nacimiento</label>
                        <input type="date"name="fecha_nacimiento"value="{{ $socio->persona->fecha_nacimiento }}"class="form-control"
                            title="fecha de nacimiento de la persona">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label>
                            Sexo
                        </label>
                        <select
                            id="sexo_id"
                            name="sexo_id"
                            class="form-control">
                                @foreach ($sexos as $sexo)
                                    @if ($sexo->id==$socio->persona->sexo_id)
                                        <option value="{{$sexo->id}}" selected>{{$sexo->definicion}}</option> 
                                    @else
                                            <option value="{{$sexo->id}}">{{$sexo->definicion}}</option>                                                
                                    @endif
                                @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="email">
                        E-mail
                    </label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                        </div>
                        <input
                            type="email"
                            name="email"
                            value="{{$socio->persona->email}}"
                            class="input-group form-control"
                            placeholder="uncorreo@mail.com..."
                            title="Introduzca un correo electrónico">
                    </div>
                </div><br>
                <!--div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group" style="text-align:center">
                        <label>

                        </label>
                        <br>
                        <button title="Limpiar" class="btn btn-danger" type="reset"><i class="fa fa-eraser"></i> Limpiar</button>
                        <button title="Guardar" id="confirmar" class="btn btn-primary" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                    </div>
                </div-->
            </div>
        </div>
    </div>
    <!--div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="card card-dark">
            <div class="card-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label>Pais</label>
                        <select  name="pais_id"id="pais_id"class="pais_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione un pais">
                                -Seleccione un pais-
                            </option>
                            @foreach ($paises as $pais)
                                <option
                                    value="{{$pais->id }}">{{$pais->nombre}}
                                </option>
                            @endforeach
                        </select>
                        <br>
                        <label>Provincia</label>
                        <select name="provincia_id"id="provincia_id"class="provincia_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione una provincia">
                                -Seleccione una provincia-
                            </option>
                        </select>
                        <br>
                        <label>Ciudad</label>
                        <select name="ciudad_id"id="ciudad_id"class="ciudad_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione una ciudad">
                                -Seleccione una ciudad-
                            </option>
                        </select>
                        <br>
                        <label>Barrio</label>
                        <select name="barrio_id"id="barrio_id"class="barrio_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione un barrio">
                                -Seleccione un barrio-
                            </option>
                        </select>
                        <br>
                        <label>Calle</label>
                        <select name="calle_id"id="calle_id"class="calle_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione una calle">
                                -Seleccione una calle-
                            </option>
                        </select>
                        <br>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="piso">
                                        Piso
                                    </label>
                                    <input
                                        type="string"
                                        name="piso"
                                        value="{{old('piso')}}"
                                        class="form-control"
                                        placeholder="Introdusca el piso"
                                        title="Introduzca el piso">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="nro_casa">
                                        Número
                                    </label>
                                    <input
                                        type="string"
                                        name="nro_casa"
                                        value="{{old('nro_casa')}}"
                                        class="form-control"
                                        placeholder="Introdusca el número"
                                        title="Introduzca la número">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="foto de perfil">
                                        Foto de Perfil
                                    </label>
                                    <input
                                        type="file"
                                        name="imagen"
                                        value="{{old('imagen')}}"
                                        class="form-control"
                                        >
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label>

                                    </label>
                                    <br>
                                    <button title="Guardar" class="btn btn-primary btn-responsive" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                                    <button title="Limpiar" class="btn btn-danger btn-responsive" type="reset"><i class="fa fa-remove">X</i> Cancelar</button>
                                </div>
                            </div>
                    </div>
                </div>

            </div>
        </div>
    </div-->
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="card card-dark">
            <div class="card-body">
                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label>Pais</label>
                        <select
                            name="pais_id"
                            id="pais_id"
                            class="pais_id custom-select"
                            required
                            >
                            <option
                                value="0"
                                disabled="true"
                                selected="true"
                                title="Seleccione un pais"
                                >
                                -Seleccione un pais-
                            </option>
                            @foreach($paises as $pais)
                                @if (old('paises', $socio->persona->domicilio->ciudad->provincia->pais_id)== $pais->id)
                                    <option value="{{$pais->id}}" selected>
                                        {{$pais->nombre}}
                                    </option>
                                @else
                                    <option value="{{$pais->id}}">
                                        {{$pais->nombre}}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                        <br>
                        <br>
                        <label>Provincia</label>
                        <select
                            name="provincia_id"
                            id="provincia_id"
                            class="provincia_id custom-select"
                            required
                            >
                            <option
                                value="0"
                                disabled="true"
                                selected="true"
                                title="Seleccione una provincia"
                                >
                                -Seleccione una provincia-
                            </option>
                            @foreach($provincias as $provincia)
                                @if (old('provincias', $socio->persona->domicilio->ciudad->provincia_id)== $provincia->id)
                                    <option value="{{$provincia->id}}" selected>
                                        {{$provincia->nombre}}
                                    </option>
                                @else
                                    <option value="{{$provincia->id}}">
                                        {{$provincia->nombre}}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                        <br>
                        <br>
                        <label>Ciudad</label>
                        <select
                            name="ciudad_id"
                            id="ciudad_id"
                            class="ciudad_id custom-select"
                            required
                            >
                            <option
                                value="0"
                                disabled="true"
                                selected="true"
                                title="Seleccione un barrio"
                                >
                                -Seleccione una ciudad-
                            </option>
                            @foreach($ciudades as $ciudad)
                                @if (old('ciudades', $socio->persona->domicilio->ciudad_id)== $ciudad->id)
                                    <option value="{{$ciudad->id}}" selected>
                                        {{$ciudad->nombre}}
                                    </option>
                                @else
                                    <option value="{{$ciudad->id}}">
                                        {{$ciudad->nombre}}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                        <br>
                        <!--label>Barrio</label>
                        <select name="barrio_id"id="barrio_id"class="barrio_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione un barrio">
                                -Seleccione un barrio-
                            </option>
                        </select>
                        <br>
                        <label>Calle</label>
                        <select name="calle_id"id="calle_id"class="calle_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione una calle">
                                -Seleccione una calle-
                            </option>
                        </select>
                        <br-->
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="direccion">
                                        Dirección
                                    </label>
                                    <input
                                        type="string"
                                        name="direccion"
                                        value="{{$socio->persona->domicilio->direccion}}"
                                        class="form-control"
                                        placeholder="Introdusca la direccion"
                                        title="Introduzca la direccion">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="piso">
                                        Piso
                                    </label>
                                    <input
                                        type="string"
                                        name="piso"
                                        value="{{$socio->persona->domicilio->piso}}"
                                        class="form-control"
                                        placeholder="Introdusca el piso"
                                        title="Introduzca el piso">
                                </div>
                            </div>
                            
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="departamento">
                                        Departamento
                                    </label>
                                    <input
                                        type="string"
                                        name="departamento"
                                        value="{{$socio->persona->domicilio->departamento}}"
                                        class="form-control"
                                        placeholder="Introdusca el departamento"
                                        title="Introduzca la departamento">
                                </div>
                            </div>
                            
                            
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group" style="text-align:center">
                                    <label>

                                    </label>
                                    <br>
                                    <button title="Limpiar" class="btn btn-danger" type="reset"><i class="fa fa-eraser"></i> Limpiar</button>
                                    <button title="Guardar" id="confirmar" class="btn btn-primary" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                                </div>
                            </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
    
</div>

{!!Form::close()!!}

    @push('scripts')
    <script type="text/javascript">

        $(document).ready(function(){

            $("select").select2({width:'100%'});

            $(document).on('change','.pais_id',function(){
                var pais_id=$(this).val();
                var div=$(this).parent();
                var op=" ";

                $.ajax({
                    type:'get',
                    url:'{!!URL::to('socio/create/encontrarProvincia')!!}',
                    data:{'id':pais_id},
                    success:function(data){
                        op+='<option value="0" selected disabled>-Seleccione una provincia-</option>';
                        for(var i=0;i<data.length;i++){
                            op+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                        }
                        div.find('.provincia_id').html(" ");
                        div.find('.provincia_id').append(op);
                    },
                    error:function(){
                    }
                });
            });


            $(document).on('change','.provincia_id',function(){
                var provincia_id=$(this).val();
                var div=$(this).parent();
                var op=" ";



                $.ajax({
                    type:'get',
                    url:'{!!URL::to('socio/create/encontrarCiudad')!!}',
                    data:{'id':provincia_id},
                    success:function(data){
                        op+='<option value="0" selected disabled>-Seleccione una ciudad-</option>';
                        for(var i=0;i<data.length;i++){
                            op+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                        }
                        div.find('.ciudad_id').html(" ");
                        div.find('.ciudad_id').append(op);
                    },
                    error:function(){
                    }
                });
            });



        });



</script>

<script>
    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
        especiales = [8, 37, 39, 46];
    
        tecla_especial = false
        for(var i in especiales) {
            if(key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
    
        if(letras.indexOf(tecla) == -1 && !tecla_especial)
            return false;
    }
  </script>

<script>
    function soloNumeros(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key);
        letras = " 1234567890";
        especiales = [8, 37, 39, 46];
    
        tecla_especial = false
        for(var i in especiales) {
            if(key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
    
        if(letras.indexOf(tecla) == -1 && !tecla_especial)
            return false;
    }
  </script>
@endpush

@endsection

