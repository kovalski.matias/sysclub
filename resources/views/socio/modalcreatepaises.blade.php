{{--ventanita modal cuando se haga clic en eliminar--}}


<div class="modal fade" id="modal-createpaises" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">RegistrarPais</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form id="formulario_agenda">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="nombre">
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button onclick="guardar()" type="button" class="btn btn-success">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>


<script>

    
    function guardar() {
        console.log("entre");
        var fd = new FormData(document.getElementById("formulario_agenda"));
        //let nombre = $("#nombre").val();

        fd.append("nombre", nombre);

        $.ajax({
            url: '{!!URL::to('socio/pais')!!}',
            type: "POST",
            data: fd,
            processData: false, // tell jQuery not to process the data
            contentType: false // tell jQuery not to set contentType
        })
    }
</script>

