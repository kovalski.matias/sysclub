@extends('layouts.admin')  <!-- Extiende de layout -->

@section('navegacion')
    <li class="breadcrumb-item"><a href="/">Menu Principal</a></li>
    <li class="breadcrumb-item active">Indice de Socios</li>
@endsection


@section('content') <!-- Contenido -->

<div class="card ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        <div class="card-body">
            <div class="row">
                <div class="col-10">
                    <p style="font-size:180%"> <i aria-hidden="true"></i> Indice de Socios</p>
                    
                </div>
                <div class="col-2">
                @can('crear socio')
                    <a href= {{ route('socio.create')}}>
                        <button class="btn btn-primary">
                            <i class="fas fa-user-plus"></i> Nuevo
                        </button>
                    </a>
                    @endcan
                </div>
            </div>
            
        <hr>
            <h4><i class="fa fa-filter" aria-hidden="true"></i> Filtrar</h4>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @include('socio.search')
            <br>
            </div>
            <table id="tablaDetalle" style="width:100%" class="table table-striped table-hover">
                <thead style="background-color:#fff">
                    <tr style="text-align:center" class="text-uppercase text-dark">
                        <th width="20%" >Foto de perfil</th>   
                        <th width="40%" >Apellido y Nombre</th>
                        <th width="20%" >Documento</th>
                        <th width="20%" >Opciones</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($socios as $socio)
                    
                    <tr style="text-align:center" onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                        <td style="text-align: center">
                            @if($socio->persona->user->foto == null)
                                <img src="{{ asset('imagenes/default.png')}}" width="50px" class="img-circle elevation-2" alt="">
                            @else
                                <img src="{{ asset('imagenes/perfil/'.$socio->persona->user->foto)}}" width="50px" class="img-circle elevation-2" alt="">
                            @endif
                        </td>
                        <td>{{ $socio->persona->nombreCompleto() }}</td>
                        <td>{{  number_format( (intval($socio->persona->documento)/1000), 3, '.', '.') }}</td>
                        
                        <td style="text-align: center" colspan="3">
                            
                            <a data-keyboard="false" data-target="#modal-show-{{ $socio->id }}" data-toggle="modal">
                                <button title="editar" class="btn btn-info btn-responsive">
                                    <i class="fa fa-eye"></i>
                                </button>

                            </a>
                            @include('socio.modalshow')

                            @can('editar socio')
                            <a href="{{URL::action('SocioController@edit',$socio->id)}}">
                                <button title="editar" class="btn btn-primary btn-responsive">
                                    <i class="fa fa-edit"></i>
                                </button>
                            </a>
                            @endcan
                            @if($socio->estado_id == 1)
                                @can('eliminar socio')
                                <a data-backdrop="static" data-keyboard="false" data-target="#modal-delete-{{ $socio->id }}" data-toggle="modal">
                                    <button title="eliminar" class="btn btn-danger btn-responsive">
                                        <i class="fas fa-user-minus"></i>
                                    </button>
                                </a>
                                @endcan
                            @else
                                <a data-backdrop="static" data-keyboard="false" data-target="#modal-habilitar-{{ $socio->id }}" data-toggle="modal">
                                    <button title="habilitar" class="btn btn-info btn-responsive">
                                        <i class="fas fa-user-plus"></i>
                                    </button>
                                </a>
                            @endif
                            <!--Archivos-->
                            <!--a href="{{URL::action('ArchivoController@index',$socio->id)}}"-->
                            <a href="{{URL::action('CuotaController@cuotaSocio',$socio->id)}}">
                                <button title="seguimiento" class="btn btn-warning btn-responsive">
                                    <i class="fa fa-search"></i>
                                </button>
                            </a>
                        </td>
                    </tr>
                    @include('socio.modaldelete')
                    @include('socio.modalhabilitar')
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@push('scripts')

    <script src="{{asset('js/tablaDetalle.js')}}"></script>



@endpush
@endsection

