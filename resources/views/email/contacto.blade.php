@extends('layouts.admin')
  <!-- Extiende de layout -->

@section('content')



<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h3>Enviar Correo</h3>
        </div>
    </div>
</div><!-- /.container-fluid -->
    
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        {!!Form::open(array('url'=>'email','method'=>'POST','autocomplete'=>'off','files' => true,))!!}
        {{Form::token()}}

        <div class="card card-dark">
            <div class="card-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="nombre"> Nombre</label>
                        <input type="string"name="nombre"maxlength="30"value="{{old('nombre')}}"class="form-control"
                            placeholder="Ingrese el nombre..."title="Introduzca un nombre">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <label for="email">
                            E-mail
                        </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                            </div>
                            <input
                                type="email"
                                name="email"
                                value="{{old('email')}}"
                                class="input-group form-control"
                                placeholder="uncorreo@mail.com..."
                                title="Introduzca un correo electrónico">
                        </div>
                    </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="asunto">Asunto</label>
                        <input type="string"name="asunto"maxlength="30"value="{{old('asunto')}}"class="form-control"
                            placeholder="Ingrese el apellido..."title="Introduzca el apellido">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="mensaje">Mensaje</label>
                        <input type="string"name="mensaje"maxlength="30"value="{{old('mensaje')}}"class="form-control"
                            placeholder="Ingrese el apellido..."title="Introduzca el apellido">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group" style="text-align:center">
                        <label>

                        </label>
                        <br>
                        <button title="Limpiar" class="btn btn-danger" type="reset"><i class="fa fa-eraser"></i> Limpiar</button>
                        <button title="Guardar" id="confirmar" class="btn btn-primary" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
</div>

{!!Form::close()!!}



@endsection

