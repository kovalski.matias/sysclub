
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Datos del Usuario</title>
</head>
<body>
    <h3>{{$asunto}}</h3>
    <label><strong>Nombre de Usuario: </strong></label>
    {{$mensaje['name']}}<br>
    <label><strong>E-Mail: </strong></label>
    {{$mensaje['email']}}<br>
    <label><strong>Contraseña: </strong></label>
    {{$mensaje['password']}}<br><br>
    <p><strong>Recuerde que puede cambiar de contraseña si asi lo desea al iniciar sesión</strong></p>
</body>
</html>