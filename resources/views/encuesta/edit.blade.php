@extends('layouts.admin')
  <!-- Extiende de layout -->
@section('navegacion')
    <li class="breadcrumb-item"><a href="asistencia">Indice de Asistencias</a></li>
    <li class="breadcrumb-item active">Control de Asistencia</li>
@endsection
@section('titulo')
    Encuestas
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    

        @include('errors.request')
        {!! Form::model($encuesta, ['method'=>'POST','route'=>['encuesta.update', $encuesta->id], 'files'=>'true']) !!}
  {!! Form::hidden('_method', 'PATCH') !!}

        <div class="card card-outline card-secondary">
            <div class="card-header">
                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="actividad">Encuesta</label>
                            
                        </div>
                    </div>
                </div>


            </div>
            <div class="card-body">
            <label for="actividad">Listado de Preguntas</label>

            <div class="card-body" > <!--style="display: none;" -->
                <div class="row">
                    <table class="table table-hover table-condensed table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="30%"><p>PREGUNTA </p></th>
                                <th><p>RESPUESTA - (5 = valoración más alta)</p></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($preguntas as $pregunta)
                                    <tr class="text-uppercase" onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                                        <td width="30%">
                                            <input type="hidden" name="pregunta_id[]" value="{{$pregunta->id}}">
                                            <p> {{ $pregunta->interrogante }} </p>

                                        </td>
                                        <td style="text-align: center">

                                            <div class="form-group">
                                                <!--input type="hidden" name="respuesta[{{ $pregunta->id }}]" value=0 class="custom-control-input" id="customSwitch4{{ $pregunta->id }}">

                                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                                <input type="checkbox" name="respuesta[{{ $pregunta->id }}]" value=1  class="custom-control-input" id="customSwitch3{{ $pregunta->id }}">
                                                <label class="custom-control-label" for="customSwitch3{{ $pregunta->id }}"></label>
                                                </div-->

                                                <div class="form-group clearfix">
                                                    <div class="icheck-danger d-inline">
                                                        <input type="radio" id="radioPrimary1{{ $pregunta->id }}" value=1  name="respuesta[{{ $pregunta->id }}]" checked>
                                                            <label class="radio-inline" for="radioPrimary1{{ $pregunta->id }}">1</label>
                                                    </div>
                                                    <div class="icheck-warning d-inline">
                                                        <input type="radio" id="radioPrimary2{{ $pregunta->id }}" value=2  name="respuesta[{{ $pregunta->id }}]">
                                                            <label class="radio-inline" for="radioPrimary2{{ $pregunta->id }}">2</label>
                                                    </div>
                                                    <div class="icheck-secondary d-inline">
                                                        <input type="radio" id="radioPrimary3{{ $pregunta->id }}" value=3  name="respuesta[{{ $pregunta->id }}]">
                                                            <label class="radio-inline" for="radioPrimary3{{ $pregunta->id }}">3</label>
                                                    </div>
                                                    <div class="icheck-info d-inline">
                                                        <input type="radio" id="radioPrimary4{{ $pregunta->id }}" value=4  name="respuesta[{{ $pregunta->id }}]">
                                                            <label class="radio-inline" for="radioPrimary4{{ $pregunta->id }}">4</label>
                                                    </div>
                                                    <div class="icheck-success d-inline">
                                                        <input type="radio" id="radioPrimary5{{ $pregunta->id }}" value=5  name="respuesta[{{ $pregunta->id }}]">
                                                            <label class="radio-inline" for="radioPrimary5{{ $pregunta->id }}">5</label>
                                                    </div>
                                                </div>
                                                
                                            </div>

                                        </td>


                                    </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>
            </div>




                            </div>
            <div class="card-footer">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group" style="text-align:center">
                        <label>

                        </label>
                        <br>
                        <a href="/perfil/membresias">
                            <button title="Cancelar" class="btn btn-secondary" type="button"><i class="fas fa-arrow-left"></i> Cancelar</button>
                        </a>
                        <button title="Guardar" id="confirmar" class="btn btn-primary" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                    </div>
                </div>

            </div>
        </div>
    </div>


</div>

{!!Form::close()!!}

@push('scripts')
<script>

    $(document).ready(function(){

        
        $("select").select2({width:'100%'});

    });

</script>
@endpush

@endsection

