@extends('layouts.admin')
  <!-- Extiende de layout -->
@section('navegacion')
    <li class="breadcrumb-item"><a href="asistencia">Indice de Asistencias</a></li>
    <li class="breadcrumb-item active">Control de Asistencia</li>
@endsection
@section('titulo')
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    

    {{Form::Open(array(
                    'action'=>array('ConfirmarAsistenciaController@alternativas'),
                    'method'=>'post'
                ))}}

        <div class="card card-outline card-secondary">
            <div class="card-header">
                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="actividad">Días para Recuperar</label>
                            
                        </div>
                    </div>
                </div>


            </div>
            <div class="card-body">
            <label for="actividad">Listado de Actividades Posibles</label>

            <div class="card-body" > <!--style="display: none;" -->
                <div class="row">
                    <table class="table table-hover table-condensed table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="30%"><p>Actividad </p></th>
                                <th width="30%"><p>Días </p></th>
                                <th width="30%"><p>Horario </p></th>
                                <th><p>Indicar</p></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($agendas as $agenda)
                                    <tr class="text-uppercase" onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                                        <td width="30%">
                                            <input type="hidden" name="agenda_id[]" value="{{$agenda->id}}">
                                            <p> {{ $agenda->descripcion }} </p>

                                        </td>
                                        <td width="30%">
                                            @if($agenda->lunes==1)
                                                <label style="font-size: 85%" class="badge badge-success">Lunes</label>
                                            @endif
                                            @if($agenda->martes==2)
                                                <label style="font-size: 85%" class="badge badge-success">Martes</label>
                                            @endif
                                            @if($agenda->miercoles==3)
                                                <label style="font-size: 85%" class="badge badge-success">Miércoles</label>
                                            @endif
                                            @if($agenda->jueves==4)
                                                <label style="font-size: 85%" class="badge badge-success">Jueves</label>
                                            @endif
                                            @if($agenda->viernes==5)
                                                <label style="font-size: 85%" class="badge badge-success">Viernes</label>
                                            @endif
                                            @if($agenda->sabado==6)
                                                <label style="font-size: 85%" class="badge badge-success">Sábado</label>
                                            @endif
                                            

                                        </td>
                                        <td width="30%">
                                            <input type="hidden" name="agenda_id[]" value="{{$agenda->id}}">
                                            <p> {{ $agenda->hora_inicial }} - {{ $agenda->hora_final }} </p>

                                        </td>
                                        <td style="text-align: center">

                                            <div class="form-group">

                                                <div class="form-group clearfix">
                                                    <div class="icheck-success d-inline">
                                                        <input type="radio" id="radioPrimary1{{ $agenda->id }}" value=1  name="dia" >
                                                            <label class="radio-inline" for="radioPrimary1{{ $agenda->id }}"></label>
                                                    </div>
                                                </div>
                                                
                                            </div>

                                        </td>


                                    </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>
            </div>




                            </div>
            <div class="card-footer">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group" style="text-align:center">
                        <label>

                        </label>
                        <br>
                        <!--a href="asistencia">
                            <button title="Cancelar" class="btn btn-secondary" type="button"><i class="fas fa-arrow-left"></i> Cancelar</button>
                        </a-->
                        <button title="Guardar" id="confirmar" class="btn btn-primary" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                    </div>
                </div>

            </div>
        </div>
    </div>


</div>

{!!Form::close()!!}

@push('scripts')
<script>

    $(document).ready(function(){

        
        $("select").select2({width:'100%'});

    });

</script>
@endpush

@endsection

