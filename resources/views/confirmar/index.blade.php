@extends('layouts.admin')  <!-- Extiende de layout -->

@section('content') <!-- Contenido -->

<div class="card ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        <div class="card-header">
            <div class="card-title">
                <p style="font-size:130%"> <i aria-hidden="true"></i> Indice de Confirmación Asistencia</p>
            </div>
        </div>
        <div class="card-body">
            

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <p>
                    <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fa fa-filter" aria-hidden="true"></i> Filtrar
                    </a>
                </p>
                <div class="collapse" id="collapseExample">
                    <div class="card card-body">

                        <!-- aca colocar el include-->
                        
                    </div>
                </div>
            </div>
            <table id="tablaDetalle" style="width:100%" class="table table-striped table-hover">
                <thead style="background-color:#fff">
                    <tr style="text-align:center" class="text-uppercase text-dark"> 
                        <th width="30%">Actividad</th>
                        <th width="30%">Alumno</th>
                        <th width="20%">Fecha</th>
                        <th width="20%">Opciones</th>

                    </tr>
                </thead>
                <?php
                     $hoy=date('Y-m-d');
                ?>
                <tbody>
                    @foreach ($membresias as $m)
                        @foreach ($m->confirmacionesasistencias as $c)
                            @if(($c->estado==1)&&($c->fecha==$hoy))
                                <tr style="text-align:center" onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                                    <td>{{ $c->membresia->actividad->nombre }}</td> 
                                    <td>{{ $c->membresia->alumno->persona->nombreCompleto() }}</td>
                                    <td>{{ $c->fecha }}</td>                       
                                    <td style="text-align: center" colspan="3">
                                        <a data-backdrop="static" data-keyboard="false" data-target="#modal-create-{{ $c->id }}" data-toggle="modal">
                                            <button title="confirmar" class="btn btn-primary btn-responsive">
                                                Confirmar
                                            </button>
                                        </a>@include('confirmar.modalcreate')
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@push('scripts')

    <script src="{{asset('js/tablaDetalle.js')}}"></script>



@endpush
@endsection

