
<style>
    .select2-container .select2-selection {
	line-height: 1.6 !important;
	height: 2.375rem !important;
	border-radius: 3px !important;
    }

</style>

<div class="modal fade" id="modal-create-{{$c->id}}" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmar Asistencia</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{Form::Open(array(
                    'action'=>array('ConfirmarAsistenciaController@confirmar'),
                    'method'=>'post'
                ))}}
                    <div class="row">
                        <div class="col-9">
                            <div class="form-group">
                                <label for="actividad">Actividad</label>
                                <input type="string"value="{{ $c->membresia->actividad->nombre }}"class="form-control" disabled>
                                <input name="actividad" type="hidden" value="{{ $c->membresia->actividad->id  }}" class="form-control">
                            
                                <input name="confirmacion" type="hidden" value="{{ $c->id  }}" class="form-control">
                                
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="actividad">Fecha</label>
                                <input type="string"value="{{ $c->fecha }}"class="form-control" disabled>
                                <input name="fecha" type="hidden" value="{{ $c->fecha }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                
                                
                                <div class="icheck-success d-inline">
                                    <input type="radio" id="radioPrimary1{{ $c->id }}" value=1  name="asistencia" checked>
                                    <label class="radio-inline" for="radioPrimary1{{ $c->id }}">SI</label>
                                </div>
                                <div class="icheck-danger d-inline">
                                    <input type="radio" id="radioPrimary2{{ $c->id }}" value=0  name="asistencia">
                                    <label class="radio-inline" for="radioPrimary2{{ $c->id }}">NO</label>
                                </div>
                                
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group" style="text-align:center">
                    <label>

                    </label>
                    <br>
                    <button title="Guardar" id="confirmar" class="btn btn-primary" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                </div>
            </div>
            {{Form::Close()}}
        </div>
    </div>
</div>

@push('scripts')
<script>

    $(document).ready(function(){

        
        $("select").select2({width:'100%'});

    });

</script>
@endpush