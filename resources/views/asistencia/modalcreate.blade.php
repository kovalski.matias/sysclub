
<style>
    .select2-container .select2-selection {
	line-height: 1.6 !important;
	height: 2.375rem !important;
	border-radius: 3px !important;
    }

</style>

<div class="modal fade" id="modal-create-{{$agenda->id}}" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Registrar Asistencia</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{Form::Open(array(
                    'action'=>array('AsistenciaController@guardar'),
                    'method'=>'post'
                ))}}
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="actividad">Actividad</label>
                                <input type="string"value="{{ $agenda->descripcion }}"class="form-control" disabled>
                                <input name="actividad_id" type="hidden" value="{{ $agenda->id }}" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="actividad">Listado de Alumnos</label>

                                <div class="card-body" > <!--style="display: none;" -->
                                    <div class="row">
                                        <table class="table table-hover table-condensed table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="30%"><p>Alumno </p></th>
                                                    <th><p>Asistencia</p></th>
                                                    <th colspan="2"><p>Observación</p></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    @foreach ($alumnos as $alumno)
                                                        <tr class="text-uppercase" onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                                                            <td width="30%">
                                                                <input type="hidden" name="alumno_id[]" value="{{$alumno->id}}">
                                                                <p> {{ $alumno->persona->nombreCompleto() }} </p>

                                                            </td>
                                                            <td style="text-align: center">

                                                                <div class="form-group">
                                                                    <input type="hidden" name="presente[{{ $alumno->id }}]" value=0 class="custom-control-input" id="customSwitch4{{ $alumno->id }}">

                                                                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                                                    <input type="checkbox" name="presente[{{ $alumno->id }}]" value=1 checked class="custom-control-input" id="customSwitch3{{ $alumno->id }}">
                                                                    <label class="custom-control-label" for="customSwitch3{{ $alumno->id }}"></label>
                                                                    </div>
                                                                </div>

                                                            </td>


                                                            <td colspan="2">
                                                                <div class="input-group mb-3">

                                                                    <input type="text" name="observacion[]" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default">
                                                                </div>


                                                        </tr>
                                                    @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>




                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group" style="text-align:center">
                    <label>

                    </label>
                    <br>
                    <button title="Limpiar" class="btn btn-danger" type="reset"><i class="fa fa-eraser"></i> Limpiar</button>
                    <button title="Guardar" id="confirmar" class="btn btn-primary" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                </div>
            </div>
            {{Form::Close()}}
        </div>
    </div>
</div>

@push('scripts')
<script>

    $(document).ready(function(){

        
        $("select").select2({width:'100%'});

    });

</script>
@endpush