@extends('layouts.admin')  <!-- Extiende de layout -->

@section('navegacion')
    <li class="breadcrumb-item"><a href="/">Menu Principal</a></li>
    <li class="breadcrumb-item active">Indice de Asistencias</li>
    @can('listar agenda')
    <li class="breadcrumb-item"><a href="/agenda">Agenda</a></li>
    @endcan
    @can('listar membresias')
    <li class="breadcrumb-item"><a href="/membresia">Membresias</a></li>
    @endcan
    @can('listar cuotas')
    <li class="breadcrumb-item"><a href="/cuota">Cuotas</a></li>
    @endcan
@endsection

@section('content') <!-- Contenido -->


<div class="card ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        <div class="card-header">
            <div class="card-title">
                <p style="font-size:130%"> <i aria-hidden="true"></i>Actividades del Día</p>
            </div>
            <div class="card-tools">
                @can('listar agenda')
                <a href= {{ route('agenda.index')}}>
                    <button class="btn btn-primary">
                        <i class="fas fa-plus"></i> Nueva
                    </button>
                </a>
                @endcan
            </div>
        </div>
        <div class="card-body">
            

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <p>
                    <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fa fa-filter" aria-hidden="true"></i> Filtrar
                    </a>
                </p>
                <div class="collapse" id="collapseExample">
                    <div class="card card-body">

                        <!-- aca colocar el include-->
                        
                    </div>
                </div>
            </div>
            <table id="tablaDetalle" style="width:100%" class="table table-striped table-hover">
                <thead style="background-color:#fff">
                    <tr class="text-uppercase text-dark">
                        <th width="30%">Actividad</th>   
                        <th width="25%">Instructor</th>
                        <th width="10%">Fecha</th>
                        <th width="15%">Sala</th>
                        <th width="20%">Opciones</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($agendas as $agenda)
                    
                    <tr onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                        <td> {{$agenda->descripcion}} </td>
                        <td>{{ $agenda->personal->persona->nombreCompleto() }}</td>
                        <td> {{now()->format('d/m/Y')}} </td>
                        <td> {{$agenda->espacio->nombre}} </td>
                        <td style="text-align: center" colspan="3">
                        
                            @if($agenda->actividad->asistencia==null)
                                
                                <a href="{{URL::action('AsistenciaController@asistir',$agenda->id)}}">
                                    <button title="Asistencia" class="btn btn-primary btn-responsive">
                                        <i class="fas fa-tasks"></i>
                                    </button>
                                </a>
                            @else
                                @if($agenda->actividad->asistencia->fecha!=date('Y-m-d'))
                                <a href="{{URL::action('AsistenciaController@asistir',$agenda->id)}}">
                                    <button title="Asistencia" class="btn btn-primary btn-responsive">
                                        <i class="fas fa-tasks"></i>
                                    </button>
                                </a>
                                    
                                @elseif($agenda->actividad->asistencia->estado_asistencia_id==1)
                                    <a href="{{URL::action('AsistenciaController@edit',$agenda->id)}}">
                                        <button title="Asistencia" class="btn btn-danger btn-responsive">
                                            <i class="fas fa-tasks"></i>
                                        </button>
                                    </a>
                                @elseif($agenda->actividad->asistencia->estado_asistencia_id==2)
                                    <a href="{{URL::action('AsistenciaController@edit',$agenda->id)}}">
                                        <button title="Asistencia" class="btn btn-success btn-responsive">
                                            <i class="fas fa-tasks"></i>
                                        </button>
                                    </a>
                                @endif
                            @endif
                        </td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
            
            
        </div>
    </div>
</div>

@push('scripts')
    
    <script src="{{asset('js/tablaDetalle.js')}}"></script>

    <script>
    
        $(document).ready(function(){


        });

    </script>


@endpush
@endsection

