@extends('layouts.admin')  <!-- Extiende de layout -->

@section('navegacion')
    <li class="breadcrumb-item"><a href="/">Menu Principal</a></li>
    <li class="breadcrumb-item active">Indice de Asistencias</li>
@endsection

@section('content') <!-- Contenido -->


<div class="card ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        <div class="card-header">
            <div class="card-title">
                <p style="font-size:130%"> <i aria-hidden="true"></i>Actividades del Día</p>
            </div>
            <div class="card-tools">
            </div>
        </div>
        <div class="card-body">
            

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="collapse" id="collapseExample">
                    <div class="card card-body">

                        <!-- aca colocar el include-->
                        
                    </div>
                </div>
            </div>
            <table id="tablaDetalle" style="width:100%" class="table table-striped table-hover">
                <thead style="background-color:#fff">
                    <tr style="text-align:center" class="text-uppercase text-dark">
                        <th width="30%">Actividad</th>   
                        <th width="25%">Instructor</th>
                        <th width="10%">Fecha</th>
                        <th width="15%">Sala</th>
                        <th width="20%">Opciones</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($cronogramas as $cronograma)
                    
                    <tr style="text-align:center" onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                        <td> {{$cronograma->descripcion}} </td>
                        <td>{{ $cronograma->agenda->personal->persona->nombreCompleto() }}</td>
                        <td> {{now()->format('d/m/Y')}} </td>
                        <td> {{$cronograma->agenda->espacio->nombre}} </td>
                        <td style="text-align: center" colspan="3">
                        
                            @if($cronograma->asistencia==null)
                                
                                <a href="{{URL::action('AsistenciaController@asistir',$cronograma->id)}}">
                                    <button title="Asistencia" class="btn btn-primary btn-responsive">
                                        <i class="fas fa-tasks"></i>
                                    </button>
                                </a>
                            @else
                                @if($cronograma->asistencia->fecha!=date('Y-m-d'))
                                <a href="{{URL::action('AsistenciaController@asistir',$cronograma->id)}}">
                                    <button title="Asistencia" class="btn btn-info btn-responsive">
                                        <i class="fas fa-tasks"></i>
                                    </button>
                                </a>
                                    
                                @elseif($cronograma->asistencia->estado_asistencia_id==1)
                                    <a href="{{URL::action('AsistenciaController@edit',$cronograma->id)}}">
                                        <button title="Asistencia" class="btn btn-danger btn-responsive">
                                            <i class="fas fa-tasks"></i>
                                        </button>
                                    </a>
                                @elseif($cronograma->asistencia->estado_asistencia_id==2)
                                    <a href="{{URL::action('AsistenciaController@edit',$cronograma->id)}}">
                                        <button title="Asistencia" class="btn btn-success btn-responsive">
                                            <i class="fas fa-tasks"></i>
                                        </button>
                                    </a>
                                @endif
                            @endif
                        </td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
            
            
        </div>
    </div>
</div>

@push('scripts')
    
    <script src="{{asset('js/tablaDetalle.js')}}"></script>

    <script>
    
        $(document).ready(function(){


        });

    </script>


@endpush
@endsection

