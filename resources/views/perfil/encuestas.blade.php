@extends('layouts.admin')  <!-- Extiende de layout -->

@section('navegacion')
<li class="breadcrumb-item"><a href="/">Menu Principal</a></li>
    <li class="breadcrumb-item"><a href="/perfil">Perfil</a></li>
    <li class="breadcrumb-item"><a href="/perfil/membresias">Membresias</a></li>
    <li class="breadcrumb-item active">Cuotas</li>
@endsection

@section('content') <!-- Contenido -->



<!--
    "id": 123456,
    "nickname": "TT123456",
    "password": "qatest123456",
    "site_status": "active",
    "email": "test_user_123456@testuser.com"
-->

<div class="card ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        <div class="card-header">
            <div class="card-title">
                <p style="font-size:130%"> <i aria-hidden="true"></i> Indice de Encuestas</p>
            </div>
            <div class="card-tools">
                <!--a href= {{ route('cuota.create')}}>
                    <button class="btn btn-primary">
                        <i class="fas fa-user-plus"></i> Nuevo
                    </button>
                </a-->
            </div>
        </div>
        <div class="card-body">
            

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <p>
                    <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fa fa-filter" aria-hidden="true"></i> Filtrar
                    </a>
                </p>
                <div class="collapse" id="collapseExample">
                    <div class="card card-body">

                        <!-- aca colocar el include-->
                        
                    </div>
                </div>
            </div>
            <table id="tablaDetalle" style="width:100%" class="table table-striped table-hover">
                <thead style="background-color:#fff">
                    <tr class="text-uppercase text-dark">
                        <th width="10%">Fecha</th>
                        <th width="20%">Alumno</th>   
                        <th width="20%">Actividad</th>
                        <th width="15%">Personal</th>
                        <th width="20%">Opciones</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($encuestas as $encuesta)
                    
                    <tr onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                        
                        <td>{{($encuesta->created_at)->format('d/m/Y') }}</td>
                        <td>{{ $encuesta->membresia->alumno->persona->nombreCompleto()}}</td> 
                        <td>{{ $encuesta->membresia->actividad->nombre }}</td>
                        <td>{{ $encuesta->membresia->actividad->agenda->personal->persona->nombreCompleto() }}</td>                   
                        <td style="text-align: center" colspan="3">
                            
                            <a href="{{URL::action('EncuestaController@edit',$encuesta->id)}}">
                                <button title="Asistencia" class="btn btn-primary btn-responsive">
                                    <i class="">Completar</i>
                                </button>
                            </a>
                            
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            
            
        </div>
    </div>
</div>

@push('scripts')
    <script src="{{asset('js/tablaDetalle.js')}}"></script>

@endpush
@endsection

