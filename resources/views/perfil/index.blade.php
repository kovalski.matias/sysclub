@extends('layouts.admin')  <!-- Extiende de layout -->

@section('navegacion')
    <li class="breadcrumb-item"><a href="/">Menu Principal</a></li>
    <li class="breadcrumb-item active">Perfil</li>
    <li class="breadcrumb-item"><a href="/perfil/membresias">Membresias</a></li>
    <!--li class="breadcrumb-item"><a href="perfil/{membresia}/cuotas">Cuotas</a></li-->
@endsection

@section('content') <!-- Contenido -->
<div class="card">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        <div class="card-header">
            <div class="card-title">
                <p style="font-size:130%"> <i aria-hidden="true"></i> Mi Perfil</p>
            </div>
        </div>
        <div class="card-body">
            <table id="tablaDetalle" style="border:1px solid black; width:100%" class="table table-striped table-hover">
                <thead style="background-color:#fff">
                    <tr style="text-align:center" class="text-uppercase text-dark">
                        <th width="20%">Nombre de Usuario</th>
                        <th width="10%">Documento</th>
                        <th width="25%">E-mail</th>
                        <th width="25%">Roles</th>
                        <th width="20%">Opciones</th>

                    </tr>
                </thead>
                <tbody>
                    <tr style="text-align:center" onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->persona->documento }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            @if(!empty($user->getRoleNames()))
                            @foreach($user->getRoleNames() as $v)
                                <label style="font-size:90%" class="badge badge-success">{{ $v }}</label>
                            @endforeach
                            @endif
                        </td>
                        <td style="text-align: center" colspan="3">

                            <a data-keyboard="false" data-target="#modal-show-{{ $user->id }}" data-toggle="modal">
                                <button title="ver" class="btn btn-info btn-responsive">
                                    <i class="fa fa-eye"></i>
                                </button>

                            </a>
                            @include('user.modalshow')

                             <a href="{{URL::action('PerfilController@edit')}}">
                                <button title="editar" class="btn btn-primary btn-responsive">
                                    <i class="fa fa-edit"></i>
                                </button>
                            </a>
                            
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@push('scripts')
    <!--script src="{{asset('js/tablaDetalle.js')}}"></script-->
@endpush
@endsection

