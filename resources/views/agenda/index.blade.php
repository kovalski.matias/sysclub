@extends("layouts.admin")

@section('navegacion')
    <li class="breadcrumb-item"><a href="/">Menu Principal</a></li>
    @can('listar actividades')
    <li class="breadcrumb-item"><a href="/actividad">Actividades</a></li>
    @endcan
    <li class="breadcrumb-item active">Agenda</li>
    @can('listar membresias')
    <li class="breadcrumb-item"><a href="/membresia">Membresias</a></li>
    @endcan
    @can('listar cuotas')
    <li class="breadcrumb-item"><a href="/cuota">Cuotas</a></li>
    @endcan
@endsection



@section("content")


@if($errors->any())
<h4>{{$errors->first()}}</h4>
@endif
<div class="row">
    <div class="card col">
        <div id='calendar'></div>
    </div>
</div>

<!--MODAL VER-->
<div class="modal fade" id="exampleModal" data-keyboard="false"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Datos de la Actividad</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                </div>
            <div class="modal-body">
                
                <div class="d-none">
                    <label for="txtID">ID:</label>
                    <input type="text" class="form-control" name="id" id="txtID">
                    <label for="txtFecha">Fecha:</label>
                    <input type="date" class="form-control" name="txtFecha" id="txtFecha">
                </div>

                <div class="form row">
                    <div class="col-md-6">
                        <label for="txtTitulo">Título:</label>
                        <input class="form-control" type="text"  id="txtTitulo" disabled>
                    </div>
                    <div class="col-md-6">
                        <label for="txtHora">Hora Inicial:</label>
                        <input class="form-control" type="time"  id="txtHoraIn" disabled>        
                    </div>
                    <div class="col-md-6">
                        <label for="txtHora">Hora Final:</label>
                        <input class="form-control" type="time"  id="txtHoraFi" disabled>        
                    </div>

                    <div class="col-md-6">
                        <label for="espacio">Espacio:</label>
                        <input class="form-control" type="text"  id="espacio" disabled>     
                    </div>

                    <div class="col-md-6">
                        <label for="instructor">Instructor:</label>
                        <input class="form-control" type="text"  id="instructor" disabled>     
                    </div>

                    <div class="col-md-6">
                        <label for="precio">Precio:</label>
                        <input class="form-control" type="text" name="precio" id="precio" >     
                    </div>

                    <div class="col-md-6">
                        <label for="cupo">Cupo Disponible:</label>
                        <input class="form-control" type="text"  id="cupo" disabled>     
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <!--button id="btnAgregar" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</button-->
                <button id="btnModificar" class="btn btn-primary">Modificar</button>
                <!--button id="btnEliminar" class="btn btn-danger">Eliminar</button-->
                <button id="btnCancelar" data-dismiss="modal" class="btn btn-light">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<!--MODAL CREAR-->
<meta name="csrf-token" content="{{ csrf_token() }}" />

<div class="modal fade" id="modal-create" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Agendar Actividad</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Fecha Inicial</label>
                                <input type="date" class="form-control" id="txtFechaInicial" name="txtFechaInicial">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Fecha Final</label>
                                <input type="date" class="form-control" id="txtFechaFinal" name="txtFechaFinal">
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="">Hora inicial</label>
                                <input type="time" class="form-control" id="txtHoraInicial" name="txtHoraInicial">
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="">Hora Final</label>
                                <input type="time" class="form-control" id="txtHoraFinal" name="txtHoraFinal">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Color</label>
                                <select  name="color_id"id="color_id"class="color_id form-control"required>
                                    <option value="0"disabled="true"selected="true"title="Seleccione un color">
                                        -Seleccione un color-
                                    </option>
                                    @foreach ($colores as $color)
                                        <option
                                            value="{{$color->id }}">{{$color->nombre}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-6">
                            <div class="form-group">
                                <label>Días a Repetir</label>
                                <div class="custom-control custom-checkbox">
                                    <label class="checkbox-inline">
                                        <input type="hidden" id="domingonull" name=domingo value=NULL>
                                        <input class="domingo" type="checkbox" id="domingo" name=domingo value=0> Domingo
                                    </label>

                                    <label class="checkbox-inline">
                                        <input type="hidden" name=lunes value=NULL>
                                        <input class="lunes" type="checkbox" id="lunes" name=lunes value=1>Lunes
                                    </label>

                                    <label class="checkbox-inline">
                                        <input type="hidden" name=martes value=NULL>
                                        <input class="martes" type="checkbox" id="martes" name=martes value=2> Martes
                                    </label>

                                    <label class="checkbox-inline">
                                        <input type="hidden" name=miercoles value=NULL>
                                        <input class="miercoles" type="checkbox" id="miercoles" name=miercoles value=3>Miércoles
                                    </label>

                                    <label class="checkbox-inline">
                                        <input type="hidden" name=jueves value=NULL>
                                        <input class="jueves" type="checkbox" id="jueves" name=jueves value=4>Jueves
                                    </label>

                                    <label class="checkbox-inline">
                                        <input type="hidden" name=viernes value=NULL>
                                        <input class="viernes" type="checkbox" id="viernes" name=viernes value=5>Viernes
                                    </label>

                                    <label class="checkbox-inline">
                                        <input type="hidden" name=sabado value=NULL>
                                        <input class="sabado" type="checkbox" id="sabado" name=sabado value=6>Sábado
                                    </label>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="">Precio Mensual</label>
                                <input type="text" maxlength="5" title="Introduzca un precio" class="form-control" id="monto" name="monto" onkeypress="return soloNumeros(event)" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Disciplina</label>
                                <select  name="disciplina_id"id="disciplina_id"class="disciplina_id form-control"required>
                                    <option value="0"disabled="true"selected="true"title="Seleccione una disciplina">
                                        -Seleccione una disciplina-
                                    </option>
                                    @foreach ($disciplinas as $disciplina)
                                        <option
                                            value="{{$disciplina->id }}">{{$disciplina->nombre}}
                                        </option>
                                    @endforeach
                                </select>
                                <br>
                                <label>Espacio</label>
                                <select name="espacio_id"id="espacio_id"class="espacio_id form-control"required>
                                    <option value="0"disabled="true"selected="true"title="Seleccione un espacio">
                                        -Seleccione un espacio-
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Nombre</label>
                                <textarea class="form-control" name="txtDescripcion" id="txtDescripcion" cols="30"
                                    rows="1"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Instructor</label>       
                                <select  name="instructor_id"id="instructor_id"class="instructor_id form-control"required>
                                    <option value="0"disabled="true"selected="true"title="Seleccione un instructor">
                                        -Seleccione un instructor-
                                    </option>
                                    @foreach ($instructores as $instructor)
                                        @if($instructor->valoracion==5)
                                            <option
                                                value="{{$instructor->id }}">{{$instructor->persona->nombreCompleto()}}&#9733;&#9733;&#9733;&#9733;&#9733;

                                            </option>
                                        @elseif($instructor->valoracion==4)
                                            <option
                                                value="{{$instructor->id }}">{{$instructor->persona->nombreCompleto()}}&#9733;&#9733;&#9733;&#9733;
                                            </option>
                                        @elseif($instructor->valoracion==3)
                                            <option
                                                value="{{$instructor->id }}">{{$instructor->persona->nombreCompleto()}}&#9733;&#9733;&#9733;
                                            </option>
                                        @elseif($instructor->valoracion==2)
                                            <option
                                                value="{{$instructor->id }}">{{$instructor->persona->nombreCompleto()}}&#9733;&#9733;
                                            </option>
                                        @elseif($instructor->valoracion==1)
                                            <option
                                                value="{{$instructor->id }}">{{$instructor->persona->nombreCompleto()}}&#9733;
                                            </option>
                                        @elseif($instructor->valoracion==0)
                                            <option
                                                value="{{$instructor->id }}">{{$instructor->persona->nombreCompleto() . ' (nuevo)'}}
                                            </option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="button" onclick="guardar()" class="btn btn-primary">Guardar</button>
            </div>
            {{Form::Close()}}
        </div>
    </div>
</div>


@endsection

@section("style")
<link href='{{ asset("assets/dashboard/vendors/fullcalendar/core/main.css") }}' rel='stylesheet' />
<link href='{{ asset("assets/dashboard/vendors/fullcalendar/daygrid/main.css") }}' rel='stylesheet' />
<link href='{{ asset("assets/dashboard/vendors/fullcalendar/timegrid/main.css") }}' rel='stylesheet' />
<link href='{{ asset("assets/dashboard/vendors/fullcalendar/bootstrap/main.css") }}' rel='stylesheet' />
@endsection

@section("scripts")
<script src='{{ asset("assets/dashboard/vendors/fullcalendar/core/main.js") }}'></script>
<script src='{{ asset("assets/dashboard/vendors/fullcalendar/interaction/main.js") }}'></script>
<script src='{{ asset("assets/dashboard/vendors/fullcalendar/daygrid/main.js") }}'></script>
<script src='{{ asset("assets/dashboard/vendors/fullcalendar/timegrid/main.js") }}'></script>
<script src='{{ asset("assets/dashboard/vendors/fullcalendar/rrule/main.js") }}'></script>
<script src='{{ asset("assets/dashboard/vendors/fullcalendar/bootstrap/main.js") }}'></script>
<script src='{{ asset("assets/dashboard/vendors/fullcalendar/core/locales/es.js") }}'></script>
<script src='{{ asset("assets/dashboard/vendors/moment.min.js") }}'></script>
@push('scripts')
<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var calendar = null;
    $(function () {
        var calendarEl = document.getElementById('calendar');
        calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: ['interaction', 'dayGrid', 'timeGrid', 'bootstrap'],
            locale: 'es',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
            },
            navLinks: true, // can click day/week names to navigate views
            selectable: true,
            selectMirror: true,
            select: function (arg) {
                let fecha_inicial = moment(arg.start).format("YYYY-MM-DD")
                let fecha_final = moment(arg.end).format("YYYY-MM-DD")
                let hora_inicial = moment(arg.start).format("HH:mm:ss")
                let hora_final = moment(arg.end).format("HH:mm:ss")

                $("#txtFechaInicial").val(fecha_inicial);
                $("#txtFechaFinal").val(fecha_final);
                $("#txtHoraInicial").val(hora_inicial);
                $("#txtHoraFinal").val(hora_final);

                $("#modal-create").modal();
                calendar.unselect()
            },
            eventClick: function (info) {

                
                minutos = info.event.start.getMinutes();
                hora = info.event.start.getHours();
                minutos=(minutos<10)?"0"+minutos:minutos;
                hora=(hora<10)?"0"+hora:hora;
                horarioInicial = (hora+":"+minutos);

                minutes = info.event.end.getMinutes();
                hour = info.event.end.getHours();
                minutes=(minutes<10)?"0"+minutes:minutes;
                hour=(hour<10)?"0"+hour:hour;
                horarioFinal = (hour+":"+minutes);             

                

                $('#txtID').val(info.event.id);
                $('#txtTitulo').val(info.event.title);
                $('#txtHoraIn').val(horarioInicial);
                $('#txtHoraFi').val(horarioFinal);
                $('#espacio').val(info.event.extendedProps.espacio_id);
                $('#instructor').val(info.event.extendedProps.instructor_id);
                $('#precio').val(info.event.extendedProps.monto);
                $('#cupo').val(info.event.extendedProps.cupo);

                $('#exampleModal').modal('toggle'); 
            },
            editable: true,
            events: '/agenda/listar'
        });

        calendar.render();
    })

    $(document).ready(function(){

        var select5 = $("#color_id").select2({width:'100%'});
        select5.data('select2').$selection.css('height', '100%');
        var select6 = $("#disciplina_id").select2({width:'100%'});
        select6.data('select2').$selection.css('height', '100%');
        var select7 = $("#espacio_id").select2({width:'100%'});
        select7.data('select2').$selection.css('height', '100%');
        var select8 = $("#instructor_id").select2({width:'100%'});
        select8.data('select2').$selection.css('height', '100%');

        $(document).on('change','.disciplina_id',function(){
            var disciplina_id=$(this).val();
            var div=$(this).parent();
            var op=" ";

            $.ajax({
                type:'get',
                url:'{!!URL::to('agenda/create/encontrarEspacio')!!}',
                data:{'id':disciplina_id},
                success:function(data){
                    op+='<option value="0" selected disabled>-Seleccione un espacio-</option>';
                    for(var i=0;i<data.length;i++){
                        op+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                    }
                    div.find('.espacio_id').html(" ");
                    div.find('.espacio_id').append(op);
                },
                error:function(){
                }
            });
        });

    });

    function guardar(){

        var espacio_id = $("#espacio_id").val();
        var disciplina_id = $("#disciplina_id").val();
        var instructor_id = $("#instructor_id").val();
        var color_id = $("#color_id").val();
        var txtFechaInicial = $("#txtFechaInicial").val();
        var txtFechaFinal = $("#txtFechaFinal").val();
        var txtHoraInicial = $("#txtHoraInicial").val();
        var txtHoraFinal = $("#txtHoraFinal").val();
        var txtDescripcion = $("#txtDescripcion").val();
        var monto = $("#monto").val();

        if( $('.lunes').prop('checked') ) {
            console.log('lunes');
            var lunes = "1";
        }else{
            var lunes = "NULL";
        }

        if( $('.martes').prop('checked') ) {
            console.log('martes');
            var martes = "2";
        }else{
            var martes = "NULL";
        }

        if( $('.miercoles').prop('checked') ) {
            console.log('miercoles');
            var miercoles = "3";
        }else{
            var miercoles = "NULL";
        }

        if( $('.jueves').prop('checked') ) {
            console.log('jueves');
            var jueves = "4";
        }else{
            var jueves = "NULL";
        }

        if( $('.viernes').prop('checked') ) {
            console.log('viernes');
            var viernes = "5";
        }else{
            var viernes = "NULL";
        }

        if( $('.sabado').prop('checked') ) {
            console.log('sabado');
            var sabado = "6";
        }else{
            var sabado = "NULL";
        }

        if( $('.domingo').prop('checked') ) {
            console.log('domingo');
            var domingo = "0";
        }else{
            var domingo = "NULL";
        }


        $.ajax({
            type: "POST",
            url: "/agenda/guardar",
            data: {'color_id':color_id, 'espacio_id':espacio_id, 'disciplina_id':disciplina_id, 'instructor_id':instructor_id, 'txtFechaInicial':txtFechaInicial,'txtFechaFinal':txtFechaFinal, 'txtHoraInicial':txtHoraInicial, 'txtHoraFinal':txtHoraFinal, 'domingo':domingo, 'lunes':lunes, 'martes':martes, 'miercoles':miercoles, 'jueves':jueves, 'viernes':viernes, 'sabado':sabado, 'txtDescripcion':txtDescripcion, 'monto':monto},
            success: function (response) {
                
                if (monto=="") {
                    toastr.error('Ingrese un precio mensual para la actividad');
                } else if(txtDescripcion==""){
                    toastr.error('Ingrese un nombre para la actividad');
                }
                else if ((domingo=="NULL")&&(lunes=="NULL")&&(martes=="NULL")&&(miercoles=="NULL")&&(jueves=="NULL")&&(viernes=="NULL")&&(sabado=="NULL")) {
                    toastr.error('Aseguresé de marcar algún día de la semana');
                } else {
                   //estado=response['estado'];
                    //$('#modal-create').modal('toggle');
                    window.location.reload(true);
                    calendar.refetchEvents();
                    toastr.success('Agenda guardada exitosamente');
                    console.log(response); 
                }
                
            },
            error:function(){
                    toastr.error('Espacio o Instructor no disponibles');
            }

        });
        
    }

    $('#btnModificar').click(function () { 
        /*ObjEvento=recolectarDatosGui("PATCH");
        enviarInformacion('/'+$('#txtID').val(), ObjEvento);*/
        var precio = $("#precio").val()

        $.ajax({
            type: "PATCH",
            url: "/agenda/"+$('#txtID').val(),
            data: {'precio':precio},
            success: function (response) {
                toastr.success("Precio Actualizado");                
                window.location.reload(true);
            }
        });  

    });

    /*function recolectarDatosGui(method) {
        nuevoEvento={
            precio:$('#precio').val(),
            '_token':$("meta[name='csrf-token']").attr("content"),
            '_method':method
        }
        return nuevoEvento;
    }*/

    /*function limpiar() {
        //$("#agenda_modal").modal('hide');
        $("#txtFechaInicial").val("");
        $("#txtFechaFinal").val("");
        $("#txtHoraInicial").val("");
        $("#txtHoraFinal").val("");
        $("#txtHoraFinal").val("");
        $("#txtDescripcion").val("");
        $("#monto").val("");
        $("#espacio_id").empty();
        $("#disciplina_id").empty();
        $("#color_id").empty();
        $("#instructor_id").empty();
        $("#lunes").val("");
        $("#martes").val("");
        $("#miercoles").val("");
        $("#juenes").val("");
        $("#viernes").val("");
        $("#sabado").val("");
        $*/

/*
    function guardar() {
        var fd = new FormData(document.getElementById("formulario_agenda"));
        let fecha = $("#txtFecha").val();
        let hora = $("#txtHoraInicial").val();
        //let tiempo = $("#txtTiempo").val();
        let hora_inicial = $("#txtHoraInicial").val();

        let hora_final = $("#txtHoraFinal").val();
        fd.append("txtHoraInicial", hora_inicial);
        fd.append("txtHoraFinal", hora_final);
        console.log("no entre a ajax");
        console.log(fd);

        $.ajax({
            url: '{!!URL::to('/agenda/guardar')!!}',
            type: "POST",
            data: fd,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType

            success:function(data){
                console.log('success');
                console.log(data);
                console.log(data.length);
                calendar.refetchEvents();
            },
            error:function(){
                console.log("falloooo");
            }

        }).done(function (respuesta) {
            if (respuesta && respuesta.ok) {
                calendar.refetchEvents();
                alert("Se registro la agenda");
                limpiar();
            } else {
                alert("La agenda ya contiene la fecha seleccionada");
            }
        })
    }*/

</script>

<script>
    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
        especiales = [8, 37, 39, 46];
    
        tecla_especial = false
        for(var i in especiales) {
            if(key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
    
        if(letras.indexOf(tecla) == -1 && !tecla_especial)
            return false;
    }
  </script>

<script>
    function soloNumeros(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key);
        letras = " 1234567890";
        especiales = [8, 37, 39, 46];
    
        tecla_especial = false
        for(var i in especiales) {
            if(key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
    
        if(letras.indexOf(tecla) == -1 && !tecla_especial)
            return false;
    }
  </script>

@endpush
@endsection
