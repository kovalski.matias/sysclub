@extends('layouts.admin')
  <!-- Extiende de layout -->
@section('navegacion')
    <li class="breadcrumb-item"><a href="#">Indice de Vacaciones</a></li>
    <li class="breadcrumb-item active">Control de Asistencia</li>
@endsection
@section('titulo')
   <i class="fas fa-umbrella-beach"></i> Control de Asistencia
@endsection

@section('content')
<style>
    .select2-container .select2-selection {
	line-height: 1.6 !important;
	height: 2.375rem !important;
	border-radius: 3px !important;
    }

</style>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    

        @include('errors.request')
        {!!Form::open(array('url'=>'asistencia','method'=>'POST','autocomplete'=>'off','files' => true,))!!}
        {{Form::token()}}

        <div class="card card-outline card-secondary">
            <div class="card-header">
                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="actividad">Actividad</label>
                            <input type="string"value="{{ $cronograma->descripcion }}"class="form-control" disabled>
                            <input name="cronograma_id" type="hidden" value="{{ $cronograma->id }}" class="form-control">
                        </div>
                    </div>
                </div>


            </div>
            <div class="card-body">
            <label for="actividad">Listado de Alumnos</label>

            <div class="card-body" > <!--style="display: none;" -->
                <div class="row">
                    <table class="table table-hover table-condensed table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="30%"><p>Alumno </p></th>
                                <th><p>Asistencia</p></th>
                                <th colspan="2"><p>Observación</p></th>
                            </tr>
                        </thead>
                        <tbody>
                                @foreach ($membresias as $membresia)

                                        <?php

                                            $dia=\Carbon\Carbon::parse(now())->format('d-m');
                                            $fecha=\Carbon\Carbon::parse($membresia->alumno->persona->fecha_nacimiento)->format('d-m');

                                            
                                        ?>
                                    <tr class="text-uppercase" onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                                        <td width="30%">
                                            <input type="hidden" name="membresia_id[]" value="{{$membresia->id}}">
                                            <p> {{ $membresia->alumno->persona->nombreCompleto() }} @if($dia==$fecha)<i class="fa fa-birthday-cake fa-2x"style="color:orange;" aria-hidden="true"></i>@endif                   

                                            
                                                <?php
                                                $id_alumno=$membresia->alumno_id;
                                                $archivo=App\Archivo::whereAlumno_id($id_alumno)->get()->last();
                                                
                                                if ($archivo) {
                                                    $diagnostico=$archivo->descripcion;
                                                }else{
                                                    $diagnostico=null;
                                                }
                                                ?>
                                                @if($diagnostico!=null)
                                                    <label style="font-size: 70%" class="badge badge-success">{{$diagnostico}}</label>
                                                @else
                                                    <label style="font-size: 70%" class="badge badge-warning">No presento ficha</label>
                                                @endif
                                            </p>
                                           

                                        </td>
                                        <td style="text-align: center">

                                            <div class="form-group">
                                                <input type="hidden" name="presente[{{ $membresia->id }}]" value=0 class="custom-control-input" id="customSwitch4{{ $membresia->id }}">

                                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                                <input type="checkbox" name="presente[{{ $membresia->id }}]" value=1  class="custom-control-input" id="customSwitch3{{ $membresia->id }}">
                                                <label class="custom-control-label" for="customSwitch3{{ $membresia->id }}"></label>
                                                </div>
                                                
                                            </div>

                                        </td>

                                        <td colspan="2">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="form-group">
                                                    <!--label for="observacion_asistencia_id">observacion_asistencia</label-->
                                                    <select name="observacion_asistencia_id[]"id="observacion_asistencia_id"class="observacion_asistencia_id custom-select">
                                                        
                                                        @foreach ($observaciones as $observacion_asistencia)
                                                            <option
                                                                value="{{$observacion_asistencia->id }}">{{$observacion_asistencia->observacion}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <!--input type="text" name="observacion[]" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default"-->
                                            </div>


                                    </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>
            </div>




                            </div>
            <div class="card-footer">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group" style="text-align:center">
                        <label>

                        </label>
                        <br>
                        <a href="/asistencia">
                            <button title="Cancelar" class="btn btn-secondary" type="button"><i class="fas fa-arrow-left"></i> Cancelar</button>
                        </a>
                        <button title="Guardar" id="confirmar" class="btn btn-primary" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                    </div>
                </div>

            </div>
        </div>
    </div>


</div>

{!!Form::close()!!}

@push('scripts')
<script>

    $(document).ready(function(){

        
        $("select").select2({width:'100%'});

    });

</script>
@endpush

@endsection

