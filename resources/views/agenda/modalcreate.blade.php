{{--ventanita modal cuando se haga clic en eliminar--}}


<div class="modal fade" id="modal-create" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Agendar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{Form::Open(array(
                    'action'=>array('AgendaController@guardar'),
                    'method'=>'post'
                ))}}
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Fecha</label>
                                <input type="date" class="form-control" id="txtFecha" name="txtFecha">
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="">Hora inicial</label>
                                <input type="time" class="form-control" id="txtHoraInicial" name="txtHoraInicial">
                            </div>
                        </div>
                        <div class="col-3">
                        <div class="form-group">
                                <label for="">Hora Final</label>
                                <input type="time" class="form-control" id="txtHoraFinal" name="txtHoraFinal">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                               
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Descripción</label>
                                <textarea class="form-control" name="txtDescripcion" id="txtDescripcion" cols="30"
                                    rows="10"></textarea>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-success">Guardare</button>
            </div>
            {{Form::Close()}}
        </div>
    </div>
</div>

