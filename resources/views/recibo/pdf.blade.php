<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">


    <title>Recibo</title>
</head>
<body>

    <div id="content" class="container">
        <div id="header" style="text-align: right">
            <img src="{{asset('imagenes/logo/'.$empresa->first()->logo)}}" alt="logo" width="50px">
        </div>
        <h3 style="text-align:center">RECIBO     <p style="text-align:right">N° {{$recibo->codigo}}</p></h3>


        <!-- DATOS PERSONALES -->
        <!--h5 style="text-align: center; background-color: brown; color: #FFFFFF"><b>DATOS PERSONALES</b></h5-->
        <table class="table table-condensed table-hover" style="border:1px solid #FFFFFF; width:100%">
            <tr style="text-align: left" valign="middle">
                <th colspan="2">Recibi de: </th>
                <td colspan="5" style="text-align: left">{{$recibo->cobro->cuota->alumno}}</td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="2">La cantidad de: </th>
                <td colspan="5" style="text-align: left">${{$recibo->monto}}  ({{$importe}})</td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="2">En concepto de: </th>
                <td colspan="5" style="text-align: left">Pago de cuota mensual de {{$recibo->cobro->cuota->actividad}}</td>
            </tr>
        </table>

        <br>
        <b>Fecha: </b>{{Carbon\Carbon::parse($recibo->fecha)->format('d/m/Y') }}
        <br>


        
    </div>




    <script src="{{asset('js/jQuery-2.1.4.min.js')}}"></script>




 <!-- Bootstrap 3.3.5 -->
 <script src="{{ asset('js/bootstrap.min.js') }}"></script>




   <!-- AdminLTE App -->
 <script src="{{asset('js/app.min.js')}}"></script>

 <script type="text/php">
    if ( isset($pdf) ) {
        $pdf->page_script('
            $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
            $pdf->text(270, 800, "Pagina $PAGE_NUM de $PAGE_COUNT", $font, 10);
        ');
    }
</script>


</body>
</html>
