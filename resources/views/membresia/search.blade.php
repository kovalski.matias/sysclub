{!! Form::model(Request::only(
    ['desde','hasta', 'agenda_id', 'estado_membresia_id']),
    ['url' => 'membresia', 'method'=>'GET', 'autocomplete'=>'on', 'role'=>'search']

    )!!}

    <style>
        .select2-container .select2-selection {
        line-height: 1.6 !important;
        height: 2rem !important;
        border-radius: 3px !important;
        }

        .altura {
            height: 2rem !important;
        }

    </style>

<div class="row">
    <div class="col-2 ">
        <label for="desde">Fecha Desde</label>
        <input
            type="date"
            name="desde"
            id="desde"
            class="fecha form-control"
            value="{{$desde}}"
        >        
    </div>    


    <div class="col-2">
        <label for="hasta">Fecha Hasta</label>
        <input
            type="date"
            name="hasta"
            id="hasta"
            class="fecha form-control"
            value="{{$hasta}}"
        >       
    </div>

    <div class="col-3">
        <div class="form-group">
            <label for="agenda_id">Agenda</label>
            <select
                name="agenda_id"
                id="agenda_id"
                class="custom-select"
                >
                @foreach ($agendas as $agenda)
                    <option
                        value="{{$agenda->id}}"
                        @if($agenda_id!=null && $agenda_id==$agenda->id)
                            selected
                        @endif
                    >
                    {{$agenda->descripcion}}
                    </option>
                @endforeach
                <option
                    value="0" @if($agenda_id == null || $agenda_id==0) selected @endif>
                    -- Todas las actividades --
                </option>
            </select>
        </div>
    </div>

    <div class="col-3">
        <div class="form-group">
            <label for="estado_membresia_id">Estado</label>
            <select
                name="estado_membresia_id"
                id="estado_membresia_id"
                class="custom-select"
                >
                @foreach ($estadomembresias as $estado_membresia)
                    <option
                        value="{{$estado_membresia->id}}"
                        @if($estado_membresia_id!=null && $estado_membresia_id==$estado_membresia->id)
                            selected
                        @endif
                    >
                    {{$estado_membresia->nombre}}
                    </option>
                @endforeach
                <option
                    value="0" @if($estado_membresia_id == null || $estado_membresia_id==0) selected @endif>
                    -- Todas los estados --
                </option>
            </select>
        </div>
    </div>
    
    <div class="col-2">
        <label for="">&nbsp;</label>
        <div class="form-group">
            <span class="input-group-btn">
                <button
                    title="buscar"
                    type="submit"
                    id="bt_add"
                    name="filtrar"
                    class="btn btn-primary btn-sm">
                        <i class="fa fa-search"></i> Buscar
                </button>

                <a

                href= "{{ route('actividad.index') }}"
                class="btn btn-default btn-sm"
                >
                <i class="fas fa-eraser"></i>
                    ... Limpiar
            </a>

            </span>
        </div>
    </div>

</div>






{{Form::close()}}

@push('scripts')
<script type="text/javascript">
$(document).ready(function(){

    var select1 = $("#actividad_id").select2({width:'100%'});
    select1.data('select2').$selection.css('height', '34px');
    var select1 = $("#estado_membresia_id").select2({width:'100%'});
    select1.data('select2').$selection.css('height', '34px');



});







</script>
@endpush
