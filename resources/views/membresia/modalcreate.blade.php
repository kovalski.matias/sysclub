
<style>
    .select2-container .select2-selection {
	line-height: 1.6 !important;
	height: 2.375rem !important;
	border-radius: 3px !important;
    }

</style>

<div class="modal fade" id="modal-create-{{$agenda->id}}" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Iniciar una Membresia</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{Form::Open(array(
                    'action'=>array('MembresiaController@guardar'),
                    'method'=>'post'
                ))}}
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="actividad">Actividad</label>
                                <input type="string"value="{{ $agenda->descripcion }}"class="form-control" disabled>
                                <input name="agenda_id" type="hidden" value="{{ $agenda->id }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Socio</label>
                                <select  name="alumno_id"id="alumno_id"class="alumno_id form-control"required>
                                    <option value="0"disabled="true"selected="true"title="Seleccione un alumno">
                                        -Seleccione un socio-
                                    </option>
                                    @foreach ($alumnos as $alumno)
                                        <option
                                            value="{{$alumno->id }}">{{$alumno->persona->nombreCompleto()}} - {{$alumno->persona->documento}}
                                        </option>
                                    @endforeach
                                </select>
                                <br><br>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group" style="text-align:center">
                    <label>

                    </label>
                    <br>
                    <button title="Limpiar" class="btn btn-danger" type="reset"><i class="fa fa-eraser"></i> Limpiar</button>
                    <button title="Guardar" id="confirmar" class="btn btn-primary" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                </div>
            </div>
            {{Form::Close()}}
        </div>
    </div>
</div>

@push('scripts')
<script>

    $(document).ready(function(){

        
        $("select").select2({width:'100%'});

    });

</script>
@endpush