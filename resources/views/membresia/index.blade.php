@extends('layouts.admin')  <!-- Extiende de layout -->

@section('navegacion')
    <li class="breadcrumb-item"><a href="/">Menu Principal</a></li>
    @can('listar actividades')
    <li class="breadcrumb-item"><a href="/actividad">Actividades</a></li>
    @endcan
    @can('listar agenda')
    <li class="breadcrumb-item"><a href="/agenda">Agenda</a></li>
    @endcan
    <li class="breadcrumb-item active">Indice de Membresías</li>
    @can('listar cuotas')
    <li class="breadcrumb-item"><a href="/cuota">Cuotas</a></li>
    @endcan
@endsection

@section('content') <!-- Contenido -->

<div class="card ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        <div class="card-body">
        <p style="font-size:180%"> <i aria-hidden="true"></i> Indice de Membresias</p>
        <hr>
            <h4><i class="fa fa-filter" aria-hidden="true"></i> Filtrar</h4>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @include('membresia.search')
            <br>
            </div>
            <table id="tablaDetalle" style="width:100%" class="table table-striped table-hover">
                <thead style="background-color:#fff">
                    <tr style="text-align:center" class="text-uppercase text-dark">
                        <th width="20%">Fecha de Renovación</th>
                        <th width="15%">Fecha de Expirar</th>   
                        <th width="15%">Actividad</th>
                        <th width="20%">Socio</th>
                        <th width="10%">Precio</th>
                        <th width="20%">Opciones</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($membresias as $membresia)
                    
                    <tr style="text-align:center" onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                        <td>{{($membresia->updated_at)->format('d/m/Y') }}</td>
                        <td>{{Carbon\Carbon::parse($membresia->ends_at)->format('d/m/Y') }}</td>
                        <td>{{ $membresia->agenda->descripcion }}</td> 
                        <td>{{ $membresia->alumno->persona->nombreCompleto() }}</td>
                        <td>$ {{ $membresia->precio }}</td>                        
                        <td style="text-align: center" colspan="3">
                            
                            @if ($membresia->estado_membresia_id==1)
                            <label class="badge badge-success">{{$membresia->estadoMembresia->nombre }}</label>
                            <a data-backdrop="static" data-keyboard="false" data-target="#modal-delete-{{ $membresia->id }}" data-toggle="modal">
                                <button title="dar de baja" class="btn btn-danger btn-responsive">
                                    <i class="far fa-sad-tear"></i>
                                </button>
                            </a>@include('membresia.modaldelete')
                            @endif

                            @if($membresia->estado_membresia_id==2)
                                <label class="badge badge-secondary">{{$membresia->estadoMembresia->nombre }}</label>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@push('scripts')

    <script src="{{asset('js/tablaDetalle.js')}}"></script>



@endpush
@endsection

