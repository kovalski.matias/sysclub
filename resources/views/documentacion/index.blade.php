
@extends('layouts.admin')
@section('navegacion')
    <li class="breadcrumb-item"><a href="/">Menu Principal</a></li>
    <li class="breadcrumb-item active">Indice de Documentación</li>
@endsection



@section('content')
    <div class="card-body">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:#D2D6DE">
            @include('errors.request')
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" >
                        <i class="fas fa-book" aria-hidden="true"></i> Documentación
                    </h4>
                </div>
                <div class="card-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
                                <div class="">
                                    <!--div class="inner">
                                        <h3></h3>
                                        <p style="font-size:150%">Parametros de Cobro</p>
                                    </div-->
                                    <div class="icon">Cobrar Cuota en Efectivo
                                    <iframe style="border:none" width="360" height="215" src="{{ asset('videos/Cobrar Cuota en Efectivo.mp4')}}"  allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
                                <div class="">
                                    <!--div class="inner">
                                        <h3></h3>
                                        <p style="font-size:150%">Parametros de Cobro</p>
                                    </div-->
                                    <div class="icon">Registrar Asistencia
                                    <iframe style="border:none" width="360" height="215" src="{{ asset('videos/Registrar Asistencia.mp4')}}"  allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
                                <div class="">
                                    <!--div class="inner">
                                        <h3></h3>
                                        <p style="font-size:150%">Parametros de Cobro</p>
                                    </div-->
                                    <div class="icon">Registrar Membresía
                                    <iframe style="border:none" width="360" height="215" src="{{ asset('videos/Registrar Membresía.mp4')}}"  allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
                                <div class="">
                                    <!--div class="inner">
                                        <h3></h3>
                                        <p style="font-size:150%">Parametros de Cobro</p>
                                    </div-->
                                    <div class="icon">Registrar Personal
                                    <iframe style="border:none" width="360" height="215" src="{{ asset('videos/Registrar Personal.mp4')}}"  allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
                                <div class="">
                                    <!--div class="inner">
                                        <h3></h3>
                                        <p style="font-size:150%">Parametros de Cobro</p>
                                    </div-->
                                    <div class="icon">Registrar una Actividad
                                    <iframe style="border:none" width="360" height="215" src="{{ asset('videos/Registrar una Actividad.mp4')}}"  allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
                                <div class="">
                                    <!--div class="inner">
                                        <h3></h3>
                                        <p style="font-size:150%">Parametros de Cobro</p>
                                    </div-->
                                    <div class="icon">Ver Registros de Auditoría
                                    <iframe style="border:none" width="360" height="215" src="{{ asset('videos/Ver Registros de Auditoria.mp4')}}"  allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
                                <div class="">
                                    <!--div class="inner">
                                        <h3></h3>
                                        <p style="font-size:150%">Parametros de Cobro</p>
                                    </div-->
                                    <div class="icon">Pagar con MercadoPago
                                    <iframe style="border:none" width="360" height="215" src="{{ asset('videos/Pagar con MercadoPago.mp4')}}"  allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
                                <div class="">
                                    <!--div class="inner">
                                        <h3></h3>
                                        <p style="font-size:150%">Parametros de Cobro</p>
                                    </div-->
                                    <div class="icon">Registrar Alumno
                                    <iframe style="border:none" width="360" height="215" src="{{ asset('videos/Registrar Alumno.mp4')}}"  allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

    @push('scripts')
    <script src="{{asset('js/tablaDetalle.js')}}"></script>


    @endpush
    @endsection