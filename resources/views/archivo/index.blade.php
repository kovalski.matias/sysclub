@extends('layouts.admin')  <!-- Extiende de layout -->

@section('navegacion')
    <li class="breadcrumb-item"><a href="/">Menu Principal</a></li>
    <li class="breadcrumb-item"><a href="/socio">Socios</a></li>
    <li class="breadcrumb-item active">Indice de Archivos</li>
@endsection


@section('content') <!-- Contenido -->

<div class="card ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        <div class="card-header">
            <div class="card-title">
                <p style="font-size:130%"> <i aria-hidden="true"></i> Indice de Archivos</p>
            </div>
            <div class="card-tools">
                <a data-keyboard="false" data-target="#modal-create-{{ $alumno->id }}" data-toggle="modal">
                    <button title="" class="btn btn-primary btn-responsive">
                    <i class="fas fa-folder-plus"> Nuevo</i>
                    </button>
                </a>
                @include('archivo.modalcreate')
            </div>
        </div>
        <div class="card-body">
            

            <!--div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <p>
                    <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fa fa-filter" aria-hidden="true"></i> Filtrar
                    </a>
                </p>
                <div class="collapse" id="collapseExample">
                    <div class="card card-body">

                    
                        
                    </div>
                </div>
            </div-->
            <table id="tablaDetalle" style="width:100%" class="table table-striped table-hover">
                <thead style="background-color:#fff">
                    <tr style="text-align:center" class="text-uppercase text-dark">
                        <th width="20%" >fecha</th>   
                        <th width="30%" >Apellido y Nombre</th>
                        <th width="30%" >descripcion</th>
                        <th width="20%" >Opciones</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($archivos as $archivo)
                    
                    <tr style="text-align:center" onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                        <td>{{($archivo->created_at)->format('d/m/Y') }}</td>
                        <td>{{ $archivo->alumno->persona->nombreCompleto() }}</td>
                        <td>{{ $archivo->descripcion }}</td>
                        
                        <td style="text-align: center" colspan="3">
                            <a href="{{ route('archivo.descargar',$archivo->documento) }}" class="btn-descargar" target="_blank">
                                <i class="fas fa-download"></i>
                            </a>
                            
                            
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@push('scripts')

    <script src="{{asset('js/tablaDetalle.js')}}"></script>



@endpush
@endsection

