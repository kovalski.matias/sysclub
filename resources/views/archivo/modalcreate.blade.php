<style>
    .select2-container .select2-selection {
	line-height: 1.6 !important;
	height: 2.375rem !important;
	border-radius: 3px !important;
    }

</style>

<div class="modal fade" id="modal-create-{{$alumno->id}}" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Registrar una Interrogante</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{Form::Open(array(
                    'route'=>['archivo.guardar'], 'files' => true,
                    'method'=>'post'
                ))}}
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group" style="text-align:center">
                                <label for="alumno" >Alumno</label>
                                <input type="string"value="{{ $alumno->persona->nombreCompleto() }}"class="form-control" disabled>
                                <input name="alumno_id" type="hidden" value="{{ $alumno->id }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="form-group" style="text-align:center">
                                <label for="descripcion"> Descripción</label>
                                <input type="text"name="descripcion"value="{{old('descripcion')}}"class="form-control"
                                    placeholder="Ingrese la descripcion..."title="Introduzca la descripcion" required>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="documento">Documento</label>
                                {!! Form::file('documento')!!}
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group" style="text-align:center">
                    <label>

                    </label>
                    <br>
                    <button title="Limpiar" class="btn btn-danger" type="reset"><i class="fa fa-eraser"></i> Limpiar</button>
                    <button title="Guardar" id="confirmar" class="btn btn-primary" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                </div>
            </div>
            {{csrf_field()}}
            {{Form::Close()}}
        </div>
    </div>
</div>

@push('scripts')

@endpush