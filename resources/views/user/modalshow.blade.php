{{--ventanita modal cuando se haga clic en eliminar--}}


<div class="modal fade modal-slide-in-right"
    aria-hidden="true"
    role="dialog"
    tabindex="-1"
    id="modal-show-{{$user->id}}">



    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="">
                <h3 class="modal-title" style="color: black"><i style="color: black" class="" aria-hidden="true"></i> Detalle del Usuario {{ $user->name }}</h3>
                <div class="modal-body">
                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>Apellido</th>
                                <td>{{ $user->persona->apellidos }}</td>
                            </tr>
                            <tr>
                                <th>Nombres</th>
                                <td>{{ $user->persona->nombres }}</td>
                            </tr>
                            <tr>
                                <th>Documento</th>
                                <td>{{ $user->persona->documento }}</td>
                            </tr>
                            <tr>
                                <th>E-mail</th>
                                <td>{{ $user->email }}</td>

                            </tr>
                            <tr>
                                <th>Fecha de nacimiento</th>
                                <td>{{Carbon\Carbon::parse($user->persona->fecha_nacimiento)->format('d/m/Y') }}</td>

                            </tr>                           

                        </thead>
                        
                    </table>
                </div>
            </div>
        </div>

    </div>


</div>

