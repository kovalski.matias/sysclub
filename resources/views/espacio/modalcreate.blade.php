<style>
    .select2-container .select2-selection {
	line-height: 1.6 !important;
	height: 2.375rem !important;
	border-radius: 3px !important;
    }

</style>

<div class="modal fade" id="modal-create" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Registrar un Espacio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{Form::Open(array(
                    'action'=>array('EspacioController@guardar'),
                    'method'=>'post'
                ))}}
                    <div class="row">
                        <div class="col-6">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="nomrazon_socialbres"> Nombre</label>
                                    <input type="string"name="nombre"value="{{old('nombre')}}"class="form-control"
                                        placeholder="Ingrese el nombre..."title="Introduzca el nombre" onkeypress="return soloLetras(event)">
                                </div>
                            </div><div class="col-12">
                                <div class="form-group">
                                    <label for="cuit">Descripcion</label>
                                    <input type="string"name="descripcion"value="{{old('descripcion')}}"class="form-control"
                                        placeholder="Ingrese la descripcion..."title="Introduzca la descripcion" onkeypress="return soloLetras(event)">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="cuit">Capacidad</label>
                                    <input type="number"name="cupo_espacio"value="{{old('cupo_espacio')}}"class="form-control"
                                        placeholder="Ingrese la capacidad..."title="Introduzca la capacidad" onkeypress="return soloNumeros(event)">
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-6">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <label>Lista de Disciplinas</label>
                                <div class="form-group">
                                    <ul class="list-unstyled">
                                        @foreach ($disciplinas as $disciplina)
                                            <li>
                                                <label>
                                                    {{ Form::checkbox('disciplinas[]', $disciplina->id, null) }}
                                                    {{ $disciplina->nombre }}
                                                </label>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group" style="text-align:center">
                    <label>

                    </label>
                    <br>
                    <button title="Limpiar" class="btn btn-danger" type="reset"><i class="fa fa-eraser"></i> Limpiar</button>
                    <button title="Guardar" id="confirmar" class="btn btn-primary" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                </div>
            </div>
            {{Form::Close()}}
        </div>
    </div>
</div>

@push('scripts')

@endpush