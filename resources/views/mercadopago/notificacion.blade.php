@extends('layouts.admin')
@section('content')
    {!!Form::model($cuota, ['method'=>'PATCH','route'=>['mercadopago.notificacion',$cuota->id]])!!}
    {{Form::token()}}
    <!--h2>pago exitoso</h2>
    <button title="Guardar" id="confirmar" class="btn btn-primary" type="submit"> <i class="fa fa-check"></i> Guardar</button-->
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><strong>Pago Exitoso</strong></h4>
                </div>
                <div class="modal-body">
                    <p>¡Muchas gracias por haber abonado su cuota!</p>
                </div>
                <div class="modal-footer">
                        <button type="submit" class="btn btn-success" data-dismiss="modal">
                                Aceptar
                        </button>
                </div>
            </div>
        </div> 
    {!!Form::close()!!}

@push('scripts')

<script type="text/javascript">





</script>

@endpush
@endsection

