@extends('layouts.admin')  <!-- Extiende de layout -->

@section('navegacion')
    <li class="breadcrumb-item"><a href="configuracion">Menu Configuraciones</a></li>
    <li class="breadcrumb-item active">Indice de Preguntas</li>
@endsection


@section('content') <!-- Contenido -->

<div class="card ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        <div class="card-header">
            <div class="card-title">
                <p style="font-size:130%"> <i aria-hidden="true"></i> Indice de Preguntas</p>
            </div>
            <div class="card-tools">
                <a data-keyboard="false" data-target="#modal-create" data-toggle="modal">
                    <button title="" class="btn btn-primary btn-responsive">
                        <i class="">Nueva</i>
                    </button>
                </a>
                @include('pregunta.modalcreate')
            </div>
        </div>
        <div class="card-body">
            

            
            <table id="tablaDetalle" style="width:100%" class="table table-striped table-hover">
                <thead style="background-color:#fff">
                    <tr style="text-align:center" class="text-uppercase text-dark">
                        <th width="80%" >interrogante</th>
                        <th width="20%" >Opciones</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($preguntas as $pregunta)
                    
                    <tr style="text-align:center"onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                        <td>{{ $pregunta->interrogante }}</td>
                        
                        <td style="text-align: center" colspan="3">
                            <a data-backdrop="static" data-keyboard="false" data-target="#modal-edit-{{ $pregunta->id }}" data-toggle="modal">
                                <button title="editar" class="btn btn-primary btn-responsive">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </a>@include('pregunta.modaledit')                         
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@push('scripts')

    <script src="{{asset('js/tablaDetalle.js')}}"></script>



@endpush
@endsection

