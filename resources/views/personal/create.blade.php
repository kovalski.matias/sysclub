@extends('layouts.admin')
  <!-- Extiende de layout -->

@section('content')

<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h3>Registrar Personal</h3>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/personal">Indice de Personal</a></li>
            <li class="breadcrumb-item active">Registrar Personal</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
    
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        @include('errors.request')
        @include('personal.mensaje')
        {!!Form::open(array('url'=>'personal','method'=>'POST','autocomplete'=>'off','files' => true,))!!}
        {{Form::token()}}

        <div class="card card-dark">
            <div class="card-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="nombres"> Nombres</label>
                        <input type="string"name="nombres"maxlength="30"value="{{old('nombres')}}"class="form-control"
                            placeholder="Ingrese el nombre..."title="Introduzca un nombre" onkeypress="return soloLetras(event)">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="apellidos">Apellidos</label>
                        <input type="string"name="apellidos"maxlength="30"value="{{old('apellidos')}}"class="form-control"
                            placeholder="Ingrese el apellido..."title="Introduzca el apellido" onkeypress="return soloLetras(event)">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="tipo_documento_id">Tipo de Documento</label>
                        <select name="tipo_documento_id"id="tipo_documento_id"class="tipo_documento_id custom-select"required>
                            <option value="0"disabled="true"selected="true"title="-Seleccione un tipo de documento-">
                                -Seleccione un tipo de documento-
                            </option>
                            @foreach ($tipo_documentos as $tipo_documento)
                                <option
                                    value="{{$tipo_documento->id }}">{{$tipo_documento->definicion}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="documento">Documento</label>
                        <input type="number"name="documento"value="{{old('documento')}}"class="form-control"
                            placeholder="33.222.111"title="Introduzca el documento" onkeypress="return soloNumeros(event)">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="fecha_nacimiento">Fecha de Nacimiento</label>
                        <input  type="date"name="fecha_nacimiento"value="{{old('fecha_nacimiento')}}"class="form-control"
                            placeholder="dia/mes/año"title="Introduzca la fecha de nacimiento">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="sexo_id">Sexo</label>
                        <select name="sexo_id"id="sexo_id"class="sexo_id custom-select"required>
                            <option value="0"disabled="true" selected="true"title="-Seleccione un tipo de sexo-">
                                -Seleccione un sexo-
                            </option>
                            @foreach ($sexos as $sexo)
                                <option
                                    value="{{$sexo->id }}">{{$sexo->definicion}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label>Lista de Disciplinas</label>
                        <div class="form-group">
                            <ul class="list-unstyled">
                                @foreach ($disciplinas as $disciplina)
                                    <li>
                                        <label>
                                            {{ Form::checkbox('disciplinas[]', $disciplina->id, null) }}
                                            {{ $disciplina->nombre }}
                                        </label>
                                    </li>
                                @endforeach
                            </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="card card-dark">
            <div class="card-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="email">
                        E-mail
                    </label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                        </div>
                        <input
                            type="email"
                            name="email"
                            value="{{old('email')}}"
                            class="input-group form-control"
                            placeholder="uncorreo@mail.com..."
                            title="Introduzca un número de telefono">
                    </div>
                </div><br>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="telefono">Telefono</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-mobile-alt"></i></span>
                        </div>
                        <input
                            type="tel"name="telefono"id="telefono"value="{{old('telefono')}}"class="input-group form-control"
                            title="Introduzca un correo electrónico">
                    </div>
                </div><br>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label>Pais</label>
                        <select  name="pais_id"id="pais_id"class="pais_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione un pais">
                                -Seleccione un pais-
                            </option>
                            @foreach ($paises as $pais)
                                <option
                                    value="{{$pais->id }}">{{$pais->nombre}}
                                </option>
                            @endforeach
                        </select>
                        <br>
                        <label>Provincia</label>
                        <select name="provincia_id"id="provincia_id"class="provincia_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione una provincia">
                                -Seleccione una provincia-
                            </option>
                        </select>
                        <br>
                        <label>Ciudad</label>
                        <select name="ciudad_id"id="ciudad_id"class="ciudad_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione una ciudad">
                                -Seleccione una ciudad-
                            </option>
                        </select>
                        <br>
                        <!--label>Barrio</label>
                        <select name="barrio_id"id="barrio_id"class="barrio_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione un barrio">
                                -Seleccione un barrio-
                            </option>
                        </select>
                        <br>
                        <label>Calle</label>
                        <select name="calle_id"id="calle_id"class="calle_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione una calle">
                                -Seleccione una calle-
                            </option>
                        </select>
                        <br-->
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="direccion">
                                        Dirección
                                    </label>
                                    <input
                                        type="string"
                                        name="direccion"
                                        value="{{old('direccion')}}"
                                        class="form-control"
                                        placeholder="Introdusca la direccion"
                                        title="Introduzca la direccion">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="piso">
                                        Piso
                                    </label>
                                    <input
                                        type="string"
                                        name="piso"
                                        value="{{old('piso')}}"
                                        class="form-control"
                                        placeholder="Introdusca el piso"
                                        title="Introduzca el piso">
                                </div>
                            </div>
                            
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="departamento">
                                        Departamento
                                    </label>
                                    <input
                                        type="string"
                                        name="departamento"
                                        value="{{old('departamento')}}"
                                        class="form-control"
                                        placeholder="Introdusca el departamento"
                                        title="Introduzca la departamento">
                                </div>
                            </div>
                            
                            
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group" style="text-align:center">
                                    <label>

                                    </label>
                                    <br>
                                    <button title="Limpiar" class="btn btn-danger" type="reset"><i class="fa fa-eraser"></i> Limpiar</button>
                                    <button title="Guardar" id="confirmar" class="btn btn-primary" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                                </div>
                            </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
    
</div>

{!!Form::close()!!}

    @push('scripts')
    <script type="text/javascript">

        $(document).ready(function(){


            var select0 = $("#tipo_documento_id").select2({width:'100%'});
            select0.data('select2').$selection.css('height', '100%');

            var select1 = $("#sexo_id").select2({width:'100%'});
            select1.data('select2').$selection.css('height', '100%');

            var select6 = $("#pais_id").select2({width:'100%'});
            select6.data('select2').$selection.css('height', '100%');
            var select7 = $("#provincia_id").select2({width:'100%'});
            select7.data('select2').$selection.css('height', '100%');
            var select8 = $("#ciudad_id").select2({width:'100%'});
            select8.data('select2').$selection.css('height', '100%');



            $(document).on('change','.pais_id',function(){
                var pais_id=$(this).val();
                var div=$(this).parent();
                var op=" ";

                $.ajax({
                    type:'get',
                    url:'{!!URL::to('personal/create/encontrarProvincia')!!}',
                    data:{'id':pais_id},
                    success:function(data){
                        op+='<option value="0" selected disabled>-Seleccione una provincia-</option>';
                        for(var i=0;i<data.length;i++){
                            op+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                        }
                        div.find('.provincia_id').html(" ");
                        div.find('.provincia_id').append(op);
                    },
                    error:function(){
                    }
                });
            });


            $(document).on('change','.provincia_id',function(){
                var provincia_id=$(this).val();
                var div=$(this).parent();
                var op=" ";



                $.ajax({
                    type:'get',
                    url:'{!!URL::to('personal/create/encontrarCiudad')!!}',
                    data:{'id':provincia_id},
                    success:function(data){
                        op+='<option value="0" selected disabled>-Seleccione una ciudad-</option>';
                        for(var i=0;i<data.length;i++){
                            op+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                        }
                        div.find('.ciudad_id').html(" ");
                        div.find('.ciudad_id').append(op);
                    },
                    error:function(){
                    }
                });
            });


            $('#telefono').mask('(0000) 00-0000');
        });



</script>

<script>
    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
        especiales = [8, 37, 39, 46];
    
        tecla_especial = false
        for(var i in especiales) {
            if(key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
    
        if(letras.indexOf(tecla) == -1 && !tecla_especial)
            return false;
    }
  </script>

<script>
    function soloNumeros(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key);
        letras = " 1234567890";
        especiales = [8, 37, 39, 46];
    
        tecla_especial = false
        for(var i in especiales) {
            if(key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
    
        if(letras.indexOf(tecla) == -1 && !tecla_especial)
            return false;
    }
  </script>
@endpush

@endsection

