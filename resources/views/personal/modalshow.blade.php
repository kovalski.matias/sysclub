{{--ventanita modal cuando se haga clic en eliminar--}}


<div class="modal fade modal-slide-in-right"
    aria-hidden="true"
    role="dialog"
    tabindex="-1"
    id="modal-show-{{$personal->id}}">



    <<div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="">
                <h3 class="modal-title" style="color: black"><i style="color: white" class="" aria-hidden="true"></i> Detalle del Personal</h3>
                
                 @if($personal->persona->user->foto == null)
                    <img src="{{ asset('imagenes/perfil/default.png')}}" width="150px" class="img-circle elevation-2" alt="User Image">
                @else
                    <img src="{{ asset('imagenes/perfil/'.$personal->persona->user->foto)}}" width="150px" class="img-circle elevation-2" alt="User Image">
                @endif</h3>
                <div class="modal-body">
                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>Apellido</th>
                                <td>{{ $personal->persona->apellidos }}</td>
                            </tr>
                            <tr>
                                <th>Nombres</th>
                                <td>{{ $personal->persona->nombres }}</td>
                            </tr>
                                <th>Documento</th>
                                <td>{{ $personal->persona->documento }}</td>
                            </tr>
                            <tr>
                                <th>Sexo</th>
                                <td>{{ $personal->persona->sexo->definicion }}</td>
                            </tr>
                            <tr>
                                <th>Domicilio</th>
                                <td>{{ $personal->persona->direccion() }}</td>
                            </tr>
                            <tr>
                                <th>Fecha de nacimiento</th>
                                <td>{{Carbon\Carbon::parse($personal->persona->fecha_nacimiento)->format('d/m/Y') }} ({{Carbon\Carbon::parse($personal->persona->fecha_nacimiento)->age }} años)</td>
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>

    </div>


</div>

