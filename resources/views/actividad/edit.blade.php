@extends('layouts.admin')
  <!-- Extiende de layout -->

@section('content')

<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h3>Editar Datos del Alumno</h3>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/alumno">Indice de Alumno</a></li>
            <li class="breadcrumb-item active">Editar Alumno</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
    
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        @include('errors.request')
        @include('alumno.mensaje')
        {!!Form::model($alumno, ['method'=>'PATCH','route'=>['alumno.update',$alumno->id]])!!}
        {{Form::token()}}

        <div class="card card-dark">
            <div class="card-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                            <label for="nombres">Nombres</label>
                            <input type="string"name="nombres"value="{{ $alumno->persona->nombres}}"class="form-control"
                                title="nombre de la persona">
                        </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                            <label for="apellidos"> Apellido </label>
                            <input type="string"name="apellidos" value="{{ $alumno->persona->apellidos }}" class="form-control"
                                title="apellido de la persona">
                        </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="documento">Documento</label>
                        <input type="integer"name="documento"value="{{ $alumno->persona->documento }}" class="form-control"
                            title="documento de la persona">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="fecha_nacimiento">Fecha de nacimiento</label>
                        <input type="date"name="fecha_nacimiento"value="{{ $alumno->persona->fecha_nacimiento }}"class="form-control"
                            title="fecha de nacimiento de la persona">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label>
                            Sexo
                        </label>
                        <select
                            id="sexo_id"
                            name="sexo_id"
                            class="form-control">
                                @foreach ($sexos as $sexo)
                                    @if ($sexo->id==$alumno->persona->sexo_id)
                                        <option value="{{$sexo->id}}" selected>{{$sexo->definicion}}</option> 
                                    @else
                                            <option value="{{$sexo->id}}">{{$sexo->definicion}}</option>                                                
                                    @endif
                                @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label>

                        </label>
                        <br>
                        <button title="Guardar" class="btn btn-primary btn-responsive" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                        <button title="Limpiar" class="btn btn-danger btn-responsive" type="reset"><i class="fa fa-remove">X</i> Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="card card-dark">
            <div class="card-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label>Pais</label>
                        <select  name="pais_id"id="pais_id"class="pais_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione un pais">
                                -Seleccione un pais-
                            </option>
                            @foreach ($paises as $pais)
                                <option
                                    value="{{$pais->id }}">{{$pais->nombre}}
                                </option>
                            @endforeach
                        </select>
                        <br>
                        <label>Provincia</label>
                        <select name="provincia_id"id="provincia_id"class="provincia_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione una provincia">
                                -Seleccione una provincia-
                            </option>
                        </select>
                        <br>
                        <label>Ciudad</label>
                        <select name="ciudad_id"id="ciudad_id"class="ciudad_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione una ciudad">
                                -Seleccione una ciudad-
                            </option>
                        </select>
                        <br>
                        <label>Barrio</label>
                        <select name="barrio_id"id="barrio_id"class="barrio_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione un barrio">
                                -Seleccione un barrio-
                            </option>
                        </select>
                        <br>
                        <label>Calle</label>
                        <select name="calle_id"id="calle_id"class="calle_id form-control"required>
                            <option value="0"disabled="true"selected="true"title="Seleccione una calle">
                                -Seleccione una calle-
                            </option>
                        </select>
                        <br>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="piso">
                                        Piso
                                    </label>
                                    <input
                                        type="string"
                                        name="piso"
                                        value="{{old('piso')}}"
                                        class="form-control"
                                        placeholder="Introdusca el piso"
                                        title="Introduzca el piso">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="nro_casa">
                                        Número
                                    </label>
                                    <input
                                        type="string"
                                        name="nro_casa"
                                        value="{{old('nro_casa')}}"
                                        class="form-control"
                                        placeholder="Introdusca el número"
                                        title="Introduzca la número">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="foto de perfil">
                                        Foto de Perfil
                                    </label>
                                    <input
                                        type="file"
                                        name="imagen"
                                        value="{{old('imagen')}}"
                                        class="form-control"
                                        >
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label>

                                    </label>
                                    <br>
                                    <button title="Guardar" class="btn btn-primary btn-responsive" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                                    <button title="Limpiar" class="btn btn-danger btn-responsive" type="reset"><i class="fa fa-remove">X</i> Cancelar</button>
                                </div>
                            </div>
                    </div>
                </div>

            </div>
        </div>
    </div-->
</div>
    
</div>

{!!Form::close()!!}

    @push('scripts')
    <script type="text/javascript">    
        $(document).ready(function(){
            var select1 = $("#sexo_id").select2({width:'100%'});
            select1.data('select2').$selection.css('height', '100%');
        });
    </script>
    <!--script type="text/javascript">

        $(document).ready(function(){


            var select0 = $("#tipo_documento_id").select2({width:'100%'});
            select0.data('select2').$selection.css('height', '100%');

            var select1 = $("#sexo_id").select2({width:'100%'});
            select1.data('select2').$selection.css('height', '100%');

            var select6 = $("#pais_id").select2({width:'100%'});
            select6.data('select2').$selection.css('height', '100%');
            var select7 = $("#provincia_id").select2({width:'100%'});
            select7.data('select2').$selection.css('height', '100%');
            var select8 = $("#ciudad_id").select2({width:'100%'});
            select8.data('select2').$selection.css('height', '100%');
            var select9 = $("#barrio_id").select2({width:'100%'});
            select9.data('select2').$selection.css('height', '100%');
            var select11 = $("#calle_id").select2({width:'100%'});
            select11.data('select2').$selection.css('height', '100%');



            $(document).on('change','.pais_id',function(){
                var pais_id=$(this).val();
                var div=$(this).parent();
                var op=" ";

                $.ajax({
                    type:'get',
                    url:'{!!URL::to('alumno/create/encontrarProvincia')!!}',
                    data:{'id':pais_id},
                    success:function(data){
                        op+='<option value="0" selected disabled>-Seleccione una provincia-</option>';
                        for(var i=0;i<data.length;i++){
                            op+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                        }
                        div.find('.provincia_id').html(" ");
                        div.find('.provincia_id').append(op);
                    },
                    error:function(){
                    }
                });
            });


            $(document).on('change','.provincia_id',function(){
                var provincia_id=$(this).val();
                var div=$(this).parent();
                var op=" ";



                $.ajax({
                    type:'get',
                    url:'{!!URL::to('alumno/create/encontrarCiudad')!!}',
                    data:{'id':provincia_id},
                    success:function(data){
                        op+='<option value="0" selected disabled>-Seleccione una ciudad-</option>';
                        for(var i=0;i<data.length;i++){
                            op+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                        }
                        div.find('.ciudad_id').html(" ");
                        div.find('.ciudad_id').append(op);
                    },
                    error:function(){
                    }
                });
            });

            $(document).on('change','.ciudad_id',function(){
                var ciudad_id=$(this).val();
                var div=$(this).parent();
                var op=" ";



                $.ajax({
                    type:'get',
                    url:'{!!URL::to('alumno/create/encontrarBarrio')!!}',
                    data:{'id':ciudad_id},
                    success:function(data){
                        console.log('success');
                        console.log(data);
                        console.log(data.length);
                        op+='<option value="0" selected disabled>-Seleccione un barrio-</option>';
                        for(var i=0;i<data.length;i++){
                            op+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                        }
                        div.find('.barrio_id').html(" ");
                        div.find('.barrio_id').append(op);
                    },
                    error:function(){
                    }
                });
            });

            $(document).on('change','.barrio_id',function(){
                var barrio_id=$(this).val();
                var div=$(this).parent();
                var op=" ";



                $.ajax({
                    type:'get',
                    url:'{!!URL::to('alumno/create/encontrarCalle')!!}',
                    data:{'id':barrio_id},
                    success:function(data){
                        op+='<option value="0" selected disabled>-Seleccione una calle-</option>';
                        for(var i=0;i<data.length;i++)
                        {

                            op+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                        }
                        div.find('.calle_id').html(" ");
                        div.find('.calle_id').append(op);
                    },
                    error:function(){
                    }
                });


            });
        });



</script-->
@endpush

@endsection

