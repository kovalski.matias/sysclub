{{--ventanita modal cuando se haga clic en eliminar--}}


<div class="modal fade modal-slide-in-right"
    aria-hidden="true"
    role="dialog"
    tabindex="-1"
    id="modal-show-{{$agenda->id}}">

    <div class="modal-dialog">
            <!--contenido del modal-->
            <div class="modal-content">
                
                <!-- cabecera del modal -->
                <div  style="background-color:{{$agenda->color->valor}}">
                    <h3  style="color:{{$agenda->color->texto}}"><b>{{$agenda->descripcion}}</b></h3>
                </div>

                <!--cuerpo del modal-->
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <!--div class="col-12"-->
                                <div class="col-6">
                                    <label for="instructor">Instructor: &nbsp;</label><b>{{$agenda->personal->persona->nombreCompleto()}}</b>
                                </div>
                                <div class="col-6">
                                    <label for="salon">Salón: &nbsp;</label><b>{{$agenda->espacio->nombre}}</b>
                                </div>
                                <div class="col-12">
                                    <label for="dias">Días: &nbsp;</label>
                                    @if($agenda->lunes!="NULL")
                                    <label style="font-size: 90%" class="badge badge-success">Lunes</label>
                                    @endif
                                    @if($agenda->martes!="NULL")
                                        <label style="font-size: 90%" class="badge badge-success">Martes</label>
                                    @endif
                                    @if($agenda->miercoles!="NULL")
                                        <label style="font-size: 90%" class="badge badge-success">Miercoles</label>
                                    @endif
                                    @if($agenda->jueves!="NULL")
                                        <label style="font-size: 90%" class="badge badge-success">Jueves</label>
                                    @endif
                                    @if($agenda->viernes!="NULL")
                                        <label style="font-size: 90%" class="badge badge-success">Viernes</label>
                                    @endif
                                    @if($agenda->sabado!="NULL")
                                        <label style="font-size: 90%" class="badge badge-success">Sábado</label>
                                    @endif
                                </div>
                                <div class="col-6">
                                    <label for="hora_inicial">Hora Inicial: &nbsp;</label><b>{{$agenda->hora_inicial}}</b>
                                </div>
                                <div class="col-6">
                                    <label for="hora_final">Hora Final: &nbsp;</label><b>{{$agenda->hora_final}}</b>
                                </div>
                                <div class="col-6">
                                    <label for="precio_mensual">Precio Mensual: &nbsp;</label><b>{{$agenda->monto}}</b>
                                </div>
                                <div class="col-6">
                                    <label for="cupo">Cupo Disponible: &nbsp;</label><b>{{$agenda->cupo_actividad}}</b>
                                </div>
                                
                            <!--/div-->

                            <div class="col-12"style="width:100%; height:400px; overflow: scroll;">
                                <br>
                                <h5><b>Alumnos Registrados</b></h5>
                                <table style="width:100%; height:400px; overflow: scroll;" class="table table-bordered table-condensed table-hover">
                                    <thead style="background-color:#fff">
                                        <tr style="text-align:center" class="text-uppercase text-dark">
                                            <th width="40%" >Apellido y Nombre</th>
                                            <th width="20%" >Documento</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($agenda->membresias as $membresia)
                                        @if($membresia->estado_membresia_id==1)
                                        <tr>
                                            <td>{{$membresia->alumno->persona->nombreCompleto()}}</td>
                                            <td>{{ $membresia->alumno->persona->documento }}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>                     
                    </div>
                </div>            
            </div>    
        </div>


</div>

