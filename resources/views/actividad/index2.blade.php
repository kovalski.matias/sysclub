@extends('layouts.admin')  <!-- Extiende de layout -->

@section('navegacion')
    <li class="breadcrumb-item"><a href="/">Menu Principal</a></li>
    <li class="breadcrumb-item active">Indice de Actividad</li>
    @can('listar agenda')
    <li class="breadcrumb-item"><a href="/agenda">Agenda</a></li>
    @endcan
    @can('listar membresias')
    <li class="breadcrumb-item"><a href="/membresia">Membresias</a></li>
    @can('listar cuotas')
    <li class="breadcrumb-item"><a href="/cuota">Cuotas</a></li>
    @endcan
@endsection


@section('content') <!-- Contenido -->

<div class="card ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        <div class="card-header">
            <div class="card-title">
                <p style="font-size:130%"> <i aria-hidden="true"></i> Indice de actividad</p>
            </div>
            <div class="card-tools">
                @can('listar agenda')
                <a href= {{ route('agenda.index')}}>
                    <button class="btn btn-primary">
                        <i class="fas fa-plus"></i> Nueva
                    </button>
                </a>
                @endcan
            </div>
        </div>
        <div class="card-body">
            

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <p>
                    <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fa fa-filter" aria-hidden="true"></i> Filtrar
                    </a>
                </p>
                <div class="collapse" id="collapseExample">
                    <div class="card card-body">

                        <!-- aca colocar el include-->
                        
                    </div>
                </div>
            </div>
            <table id="tablaDetalle" style="width:100%" class="table table-striped table-hover">
                <thead style="background-color:#fff">
                    <tr class="text-uppercase text-dark">
                        <th width="30%">Actividad</th>   
                        <th width="25%">Instructor</th>
                        <th width="10%">Cupo</th>
                        <th width="15%">Sala</th>
                        <th width="20%">Opciones</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($actividades as $actividad)
                    
                    <tr onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                        <td> {{$actividad->nombre}} </td>
                        <td>{{ $actividad->agenda->personal->persona->nombreCompleto() }}</td>
                        <td> {{$actividad->cupo_actividad}} </td>
                        <td> {{$actividad->agenda->espacio->nombre}} </td>
                        <td style="text-align: center" colspan="3">
                            
                            <a data-keyboard="false" data-target="#modal-create-{{ $actividad->id}}" data-toggle="modal">
                                <button title="Agregar Alumno" class="btn btn-primary btn-responsive">
                                    <i class="fas fa-plus"></i>
                                </button>
                            </a>
                            @include('membresia.modalcreate')
                        </td>
                        
                    </tr>
                    
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>
</div>
@push('scripts')

    <script src="{{asset('js/tablaDetalle.js')}}"> </script>

    

@endpush
@endsection

