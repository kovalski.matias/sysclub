@extends('layouts.admin')  <!-- Extiende de layout -->

@section('navegacion')
    <li class="breadcrumb-item"><a href="/">Menu Principal</a></li>
    <li class="breadcrumb-item active">Indice de Actividades</li>
    @can('listar agenda')
    <li class="breadcrumb-item"><a href="/agenda">Agenda</a></li>
    @endcan
    @can('listar membresias')
    <li class="breadcrumb-item"><a href="/membresia">Membresias</a></li>
    @endcan
    @can('listar cuotas')
    <li class="breadcrumb-item"><a href="/cuota">Cuotas</a></li>
    @endcan
@endsection

@section('content') <!-- Contenido -->


<div class="card ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        <div class="card-body">
            <div class="row">
                <div class="col-10">
                    <p style="font-size:180%"> <i aria-hidden="true"></i> Indice de Actividades</p>
                    
                </div>
                <div class="col-2">
                @can('listar agenda')
                    <a href= {{ route('agenda.index')}}>
                        <button class="btn btn-primary">
                            <i class="fas fa-plus"></i> Nueva
                        </button>
                    </a>
                    @endcan
                </div>
            </div>
            
        <hr>
            <h4><i class="fa fa-filter" aria-hidden="true"></i> Filtrar</h4>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @include('actividad.search')
            <br>
            </div>
            <table id="tablaDetalle" style="width:100%" class="table table-striped table-hover">
                <thead style="background-color:#fff">
                    <tr style="text-align:center" class="text-uppercase text-dark">
                        <th width="15%">Actividad</th>   
                        <th width="15%">Instructor</th>   
                        <th width="25%">Días</th>
                        <th width="10%">Cupo</th>
                        <th width="15%">Espacio</th>
                        <th width="20%">Opciones</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($agendas as $agenda)
                    
                    <tr style="text-align:center" onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                        <td> {{$agenda->descripcion}} </td>
                        <td>{{ $agenda->personal->persona->nombreCompleto() }}</td>
                        <td>
                            @if($agenda->lunes!="NULL")
                                <label style="font-size: 80%" class="badge badge-success">Lunes</label>
                            @endif
                            @if($agenda->martes!="NULL")
                                <label style="font-size: 80%" class="badge badge-success">Martes</label>
                            @endif
                            @if($agenda->miercoles!="NULL")
                                <label style="font-size: 80%" class="badge badge-success">Miercoles</label>
                            @endif
                            @if($agenda->jueves!="NULL")
                                <label style="font-size: 80%" class="badge badge-success">Jueves</label>
                            @endif
                            @if($agenda->viernes!="NULL")
                                <label style="font-size: 80%" class="badge badge-success">Viernes</label>
                            @endif
                            @if($agenda->sabado!="NULL")
                                <label style="font-size: 80%" class="badge badge-success">Sábado</label>
                            @endif
                        </td>
                        <td> @if($agenda->cupo_actividad>0)
                                {{ $agenda->cupo_actividad}}
                            @elseif($agenda->cupo_actividad==0)
                                <label style="font-size: 90%" class="badge badge-danger">Sin Cupo</label>
                            @endif </td>
                        <td> {{$agenda->espacio->nombre}} </td>
                        <td style="text-align: center" colspan="3">
                            @if($agenda->cupo_actividad>0)
                            <a data-keyboard="false" data-target="#modal-create-{{ $agenda->id}}" data-toggle="modal">
                                <button title="Agregar Alumno" class="btn btn-primary btn-responsive">
                                    <i class="fas fa-plus"></i>
                                </button>
                            </a>
                            @endif
                            @include('membresia.modalcreate')
                            <a data-keyboard="false" data-target="#modal-show-{{ $agenda->id }}" data-toggle="modal">
                                <button title="ver" class="btn btn-info btn-responsive">
                                    <i class="fa fa-eye"></i>
                                </button>

                            </a>
                            @include('actividad.modalshow')
                        </td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
            
            
        </div>
    </div>
</div>

@push('scripts')
    <script src="{{asset('js/tablaDetalle.js')}}"></script>



@endpush
@endsection

