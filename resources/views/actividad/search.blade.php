{!! Form::model(Request::only(
    ['desde','hasta', 'disciplina_id']),
    ['url' => 'actividad', 'method'=>'GET', 'autocomplete'=>'on', 'role'=>'search']

    )!!}

    <style>
        .select2-container .select2-selection {
        line-height: 1.6 !important;
        height: 2rem !important;
        border-radius: 3px !important;
        }

        .altura {
            height: 2rem !important;
        }

    </style>

<div class="row">
    <div class="col-3 ">
        <label for="desde">Fecha Desde</label>
        <input
            type="date"
            name="desde"
            id="desde"
            class="fecha form-control"
            value="{{$desde}}"
        >        
    </div>    


    <div class="col-3">
        <label for="hasta">Fecha Hasta</label>
        <input
            type="date"
            name="hasta"
            id="hasta"
            class="fecha form-control"
            value="{{$hasta}}"
        >       
    </div>

    <div class="col-3">
        <div class="form-group">
            <label for="disciplina_id">Disciplina</label>
            <select
                name="disciplina_id"
                id="disciplina_id"
                class="custom-select"
                >
                @foreach ($disciplinas as $disciplina)
                    <option
                        value="{{$disciplina->id}}"
                        @if($disciplina_id!=null && $disciplina_id==$disciplina->id)
                            selected
                        @endif
                    >
                    {{$disciplina->nombre}}
                    </option>
                @endforeach
                <option
                    value="0" @if($disciplina_id == null || $disciplina_id==0) selected @endif>
                    -- Todas las disciplinas --
                </option>
            </select>
        </div>
    </div>
    
    <div class="col-3">
        <label for="">&nbsp;</label>
        <div class="form-group">
            <span class="input-group-btn">
                <button
                    title="buscar"
                    type="submit"
                    id="bt_add"
                    name="filtrar"
                    class="btn btn-primary btn-sm">
                        <i class="fa fa-search"></i> Buscar
                </button>

                <a

                href= "{{ route('actividad.index') }}"
                class="btn btn-default btn-sm"
                >
                <i class="fas fa-eraser"></i>
                    ... Limpiar
            </a>

            </span>
        </div>
    </div>

</div>






{{Form::close()}}

@push('scripts')
<script type="text/javascript">
$(document).ready(function(){

    var select1 = $("#disciplina_id").select2({width:'100%'});
    select1.data('select2').$selection.css('height', '34px');



});







</script>
@endpush
