@extends('layouts.admin')
  <!-- Extiende de layout -->

@section('content')

<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h3>Registrar actividad</h3>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/actividad">Indice de Actividad</a></li>
            <li class="breadcrumb-item active">Registrar Actividad</li>
        </ol>
        </div>
    </div>
</div><!-- /.container-fluid -->
    
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        @include('errors.request')
        @include('actividad.mensaje')
        {!!Form::open(array('url'=>'actividad','method'=>'POST','autocomplete'=>'off','files' => true,))!!}
        {{Form::token()}}

        <div class="card card-dark">
            <div class="card-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="nombre"> Nombre</label>
                        <input type="string"name="nombre"maxlength="30"value="{{old('nombre')}}"class="form-control"
                            placeholder="Ingrese el nombre..."title="Introduzca un nombre">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="cupo">Cupo</label>
                        <input type="number"name="cupo"maxlength="30"value="{{old('cupo')}}"class="form-control"
                            placeholder="Ingrese el cupo..."title="Introduzca el cupo">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="monto">Monto</label>
                        <input type="number"name="monto"maxlength="30"value="{{old('monto')}}"class="form-control"
                            placeholder="Ingrese el monto..."title="Introduzca el monto">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="fecha_nacimiento">Fecha de Inicio</label>
                        <input  type="date"name="fecha_inicio"value="{{old('fecha_inicio')}}"class="form-control"
                            placeholder="dia/mes/año"title="Introduzca la fecha de inicio">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="fecha_cierre">Fecha de Cierre</label>
                        <input  type="date"name="fecha_cierre"value="{{old('fecha_cierre')}}"class="form-control"
                            placeholder="dia/mes/año"title="Introduzca la fecha de cierre">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="card card-dark">
            <div class="card-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="disciplina_id">Disciplina</label>
                                    <select name="disciplina_id"id="disciplina_id"class="disciplina_id custom-select"required>
                                        <option value="0"disabled="true"selected="true"title="-Seleccione una disciplina-">
                                            -Seleccione una Disciplina-
                                        </option>
                                        @foreach ($disciplinas as $disciplina)
                                            <option
                                                value="{{$disciplina->id }}">{{$disciplina->nombre}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="personal_id">Instructor</label>
                                    <select name="personal_id"id="personal_id"class="personal_id custom-select"required>
                                        <option value="0"disabled="true"selected="true"title="-Seleccione un instructor-">
                                            -Seleccione un Instructor-
                                        </option>
                                        @foreach ($personales as $personal)
                                            <option
                                                value="{{$personal->id }}">{{$personal->persona->nombreCompleto}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!--div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="plan_id">PLan</label>
                                    <select name="plan_id"id="plan_id"class="plan_id custom-select"required>
                                        <option value="0"disabled="true"selected="true"title="-Seleccione un plan-">
                                            -Seleccione un plan-
                                        </option>
                                        @foreach ($planes as $plan)
                                            <option
                                                value="{{$plan->id }}">{{$plan->nombre}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="plan_id">Espacio</label>
                                    <select name="plan_id"id="plan_id"class="plan_id custom-select"required>
                                        <option value="0"disabled="true"selected="true"title="-Seleccione un plan-">
                                            -Seleccione un plan-
                                        </option>
                                        @foreach ($planes as $plan)
                                            <option
                                                value="{{$plan->id }}">{{$plan->nombre}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div-->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label>

                                    </label>
                                    <br>
                                    <button title="Guardar" class="btn btn-primary btn-responsive" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                                    <button title="Limpiar" class="btn btn-danger btn-responsive" type="reset"><i class="fa fa-remove">X</i> Cancelar</button>
                                </div>
                            </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
    
</div>

{!!Form::close()!!}

    @push('scripts')
    <script type="text/javascript">

        $(document).ready(function(){


            var select0 = $("#tipo_documento_id").select2({width:'100%'});
            select0.data('select2').$selection.css('height', '100%');

            var select1 = $("#sexo_id").select2({width:'100%'});
            select1.data('select2').$selection.css('height', '100%');

            var select6 = $("#pais_id").select2({width:'100%'});
            select6.data('select2').$selection.css('height', '100%');
            var select7 = $("#provincia_id").select2({width:'100%'});
            select7.data('select2').$selection.css('height', '100%');
            var select8 = $("#ciudad_id").select2({width:'100%'});
            select8.data('select2').$selection.css('height', '100%');
            var select9 = $("#barrio_id").select2({width:'100%'});
            select9.data('select2').$selection.css('height', '100%');
            var select11 = $("#calle_id").select2({width:'100%'});
            select11.data('select2').$selection.css('height', '100%');



            $(document).on('change','.pais_id',function(){
                var pais_id=$(this).val();
                var div=$(this).parent();
                var op=" ";

                $.ajax({
                    type:'get',
                    url:'{!!URL::to('actividad/create/encontrarProvincia')!!}',
                    data:{'id':pais_id},
                    success:function(data){
                        op+='<option value="0" selected disabled>-Seleccione una provincia-</option>';
                        for(var i=0;i<data.length;i++){
                            op+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                        }
                        div.find('.provincia_id').html(" ");
                        div.find('.provincia_id').append(op);
                    },
                    error:function(){
                    }
                });
            });


            $(document).on('change','.provincia_id',function(){
                var provincia_id=$(this).val();
                var div=$(this).parent();
                var op=" ";



                $.ajax({
                    type:'get',
                    url:'{!!URL::to('actividad/create/encontrarCiudad')!!}',
                    data:{'id':provincia_id},
                    success:function(data){
                        op+='<option value="0" selected disabled>-Seleccione una ciudad-</option>';
                        for(var i=0;i<data.length;i++){
                            op+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                        }
                        div.find('.ciudad_id').html(" ");
                        div.find('.ciudad_id').append(op);
                    },
                    error:function(){
                    }
                });
            });

            $(document).on('change','.ciudad_id',function(){
                var ciudad_id=$(this).val();
                var div=$(this).parent();
                var op=" ";



                $.ajax({
                    type:'get',
                    url:'{!!URL::to('actividad/create/encontrarBarrio')!!}',
                    data:{'id':ciudad_id},
                    success:function(data){
                        console.log('success');
                        console.log(data);
                        console.log(data.length);
                        op+='<option value="0" selected disabled>-Seleccione un barrio-</option>';
                        for(var i=0;i<data.length;i++){
                            op+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                        }
                        div.find('.barrio_id').html(" ");
                        div.find('.barrio_id').append(op);
                    },
                    error:function(){
                    }
                });
            });

            $(document).on('change','.barrio_id',function(){
                var barrio_id=$(this).val();
                var div=$(this).parent();
                var op=" ";



                $.ajax({
                    type:'get',
                    url:'{!!URL::to('actividad/create/encontrarCalle')!!}',
                    data:{'id':barrio_id},
                    success:function(data){
                        op+='<option value="0" selected disabled>-Seleccione una calle-</option>';
                        for(var i=0;i<data.length;i++)
                        {

                            op+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                        }
                        div.find('.calle_id').html(" ");
                        div.find('.calle_id').append(op);
                    },
                    error:function(){
                    }
                });


            });
        });



</script>
@endpush

@endsection

