@extends('layouts.admin')

@section('content')
<div class="card ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        <div class="card-header">
            <div class="card-title">
                <p style="font-size:130%"> <i aria-hidden="true"></i> Recaudaciones</p>
            </div>
        </div>
        <div class="card-body">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div>
                    <div class="card-body">
                    @include('estadistica.searchrecaudacion')
                        
                    </div>
                </div>

                <div class="col-9">
                    <div id="chart_div"></div>
                </div>
                
            </div>
        </div>
    </div>
</div>


@push('scripts')
    <script type="text/javascript">
        google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

    

    var data = google.visualization.arrayToDataTable([
        ["Nombre", "Recaudacion"],
            @foreach ($disciplinas as $d)
            ['{{ $d->nombre }}', {{$d->recaudacion}}],
            @endforeach                          
    ]);

    var options = {
        title: "Recaudación por tipo de actividad",
        width: 800, //400
        height: 600, //600
        animation: {
            duration: "2000",
            startup:  "true"
        },
        bar: {groupWidth: "95%"},
        legend: false,
        colors: ["#3C8DBC"],
        vAxis: {title : 'Recaudación'},
        hAxis: {title : 'Actividades'},
    };

    var chart = new google.visualization.ColumnChart(
        document.getElementById('chart_div'));

    chart.draw(data, options);
    }
    </script>
@endpush

@endsection