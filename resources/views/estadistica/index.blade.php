
@extends('layouts.admin')
@section('navegacion')
    <li class="breadcrumb-item"><a href="/">Menu Principal</a></li>
    <li class="breadcrumb-item active">Indice de Estadísticas</li>
@endsection



@section('content')
    <div class="card-body">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color:#D2D6DE">
            @include('errors.request')
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" >
                        <i class="fas fa-chart-pie" aria-hidden="true"></i>Estadísticas
                    </h4>
                </div>
                <div class="card-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                <div class="small-box bg-info">
                                    <div class="inner">
                                        <h3></h3>
                                        <p style="font-size:150%">Recaudación por Disciplina</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fas fa-coins"></i>
                                    </div>
                                    <a href="estadistica/recaudacion" class="small-box-footer">
                                        Más Información <i class="fas fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                                <div class="small-box bg-info">
                                    <div class="inner">
                                        <h3></h3>
                                        <p style="font-size:150%">Horarios más Demandados</p>
                                    </div>
                                    <div class="icon">
                                        <i class="far fa-clock"></i>
                                    </div>
                                    <a href="estadistica/horarios" class="small-box-footer">
                                        Más Información <i class="fas fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
    <script src="{{asset('js/tablaDetalle.js')}}"></script>


    @endpush
    @endsection

