@extends('layouts.admin')

@section('content')
<div class="card ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        <div class="card-header">
            <div class="card-title">
                <p style="font-size:130%"> <i aria-hidden="true"></i> Franja horaria más demandados</p>
                <br>
                <p style="font-size:110%"> <i aria-hidden="true"></i> Filtrado por el último mes</p>
            </div>
        </div>
        <div class="card-body">
            

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                

                <div class="col-9">
                <div id="columnchart_material" style="width: 900px; height: 300px;"></div>
                </div>
                
            </div>
        </div>
    </div>
</div>


@push('scripts')
<script type="text/javascript">
            google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Días de la Semana', 'Mañana', 'Tarde', 'Noche'],
          ['Lunes', {{$lunesmañana}}, {{$lunestarde}}, {{$lunesnoche}}],
          ['Martes', {{$martesmañana}}, {{$martestarde}}, {{$martesnoche}}],
          ['Miércoles', {{$miercolesmañana}}, {{$miercolestarde}}, {{$miercolesnoche}}],
          ['Jueves', {{$juevesmañana}}, {{$juevestarde}}, {{$juevesnoche}}],
          ['Viernes', {{$viernesmañana}}, {{$viernestarde}}, {{$viernesnoche}}],
          ['Sábado', {{$sabadomañana}}, {{$sabadotarde}}, {{$sabadonoche}}]
        ]);    


        var options = {
          chart: {
            title: 'Demanda Horaria',
          },
          bar: {groupWidth: "75%"},
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
 
        </script>
@endpush

@endsection