<style>
    .select2-container .select2-selection {
	line-height: 1.6 !important;
	height: 2.375rem !important;
	border-radius: 3px !important;
    }

</style>

<div class="modal fade" id="modal-create" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Registrar un Tipo de Documento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{Form::Open(array(
                    'action'=>array('TipoDocumentoController@guardar'),
                    'method'=>'post'
                ))}}
                    <div class="row">
                        <div class="col-3">
                            <div class="form-group">
                                <label for="cuit">Abreviatura</label>
                                <input type="string"name="abreviatura"maxlength="30"value="{{old('abreviatura')}}"class="form-control"
                                    placeholder="Ingrese la abreviatura..."title="Introduzca la abreviatura" onkeypress="return soloLetras(event)">
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="form-group">
                                <label for="nomrazon_socialbres"> Definicion</label>
                                <input type="string"name="definicion"maxlength="30"value="{{old('definicion')}}"class="form-control"
                                    placeholder="Ingrese la definicion..."title="Introduzca la Definicion" onkeypress="return soloLetras(event)">
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group" style="text-align:center">
                    <label>

                    </label>
                    <br>
                    <button title="Limpiar" class="btn btn-danger" type="reset"><i class="fa fa-eraser"></i> Limpiar</button>
                    <button title="Guardar" id="confirmar" class="btn btn-primary" type="submit"> <i class="fa fa-check"></i> Guardar</button>
                </div>
            </div>
            {{Form::Close()}}
        </div>
    </div>
</div>

@push('scripts')

@endpush