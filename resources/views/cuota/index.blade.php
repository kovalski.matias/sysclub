@extends('layouts.admin')  <!-- Extiende de layout -->

@section('navegacion')
    <li class="breadcrumb-item"><a href="/">Menu Principal</a></li>
    <!--@can('listar actividades')
    <li class="breadcrumb-item"><a href="/actividad">Actividades</a></li>
    @endcan
    @can('listar agenda')
    <li class="breadcrumb-item"><a href="/agenda">Agenda</a></li>
    @endcan-->
    @can('listar membresias')
    <li class="breadcrumb-item"><a href="/membresia">Membresias</a></li>
    @endcan
    <li class="breadcrumb-item active">Indice de Cuotas</li>
    @can('listar movimientos')
    <li class="breadcrumb-item"><a href="{{ route('movimiento.index') }}">Movimientos</a></li>
    @endcan
@endsection

@section('content') <!-- Contenido -->



<!--
    "id": 123456,
    "nickname": "TT123456",
    "password": "qatest123456",
    "site_status": "active",
    "email": "test_user_123456@testuser.com"
-->

<div class="card ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        <div class="card-body">
        <p style="font-size:180%"> <i aria-hidden="true"></i> Indice de Cuotas</p>
        <hr>
            <h4><i class="fa fa-filter" aria-hidden="true"></i> Filtrar</h4>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @include('cuota.search')
            <br>
            </div>
            <table id="tablaDetalle" style="width:100%" class="table table-striped table-hover">
                <thead style="background-color:#fff">
                    <tr style="text-align:center" class="text-uppercase text-dark">
                        <th width="10%">Fecha Vencimiento</th>   
                        <th width="15%">Actividades</th>
                        <th width="20%">Alumno</th>
                        <th width="15%">Precio</th>
                        <th width="20%">Estado</th>
                        <th width="20%">Opciones</th>

                    </tr>
                </thead>
                <tbody>
                <?php $total=0;?>
                    @foreach ($cuotas as $cuota)
                    
                    <tr style="text-align:center" onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                        
                        <td width="10%">{{ $cuota->fecha_casteada() }}</td>
                        <td width="15%">{{ $cuota->actividad }}</td> 
                        <td width="20%">{{ $cuota->alumno }}</td>
                        <td width="15%" style="text-align:right">$ {{ $cuota->monto }}</td> 
                        <td width="20%">@if ($cuota->estadoCuota->nombre  == "Generada")
                                <label class="badge badge-default">{{$cuota->estadoCuota->nombre }}</label>
                            @endif
                            @if ($cuota->estadoCuota->nombre  == "Pagada")
                                <label class="badge badge-primary">{{$cuota->estadoCuota->nombre }}</label>
                                    <!--img src="{{ asset('imagenes/cobro/'.$cuota->cobro->formaPago->logo)}}" class="img-circle elevation-2" width="30px" alt="Cobro Image"-->
                                @if($cuota->cobro->forma_pago_id==1)
                                    <label class="badge badge-success">{{$cuota->cobro->formaPago->nombre }}</label>
                                    <?php
                                    $total=$total+$cuota->cobro->monto;
                                    //dd($cuota->cobro->monto);
                                    ?> 
                                @elseif($cuota->cobro->forma_pago_id==2)
                                    <label class="badge badge-info">{{$cuota->cobro->formaPago->nombre }}</label> 
                                    <?php
                                    $total=$total+$cuota->cobro->monto;
                                    //dd($cuota->cobro->monto);
                                    ?>
                                @endif
                                
                            
                            @endif
                            @if ($cuota->estadoCuota->nombre  == "Vencida")
                                <label class="badge badge-danger">{{$cuota->estadoCuota->nombre }}</label>
                            @endif
                            @if ($cuota->estadoCuota->nombre  == "Cancelada")
                                <label class="badge badge-secondary">{{$cuota->estadoCuota->nombre }}</label>
                            @endif
                        </td>                       
                        <td width="20%" style="text-align: center">
                            @can('cobrar cuota')
                            @if($cuota->estadoCuota->nombre!="Pagada")   
                                <a data-keyboard="false" data-target="#modal-cobrar-{{ $cuota->id}}" data-toggle="modal">
                                    <button title="Cobrar cuota" class="btn btn-primary btn-responsive">
                                        <i class="">Cobrar</i>
                                    </button>
                                </a>
                                @include('cuota.modalcobrar')
                            @endif
                            @endcan

                            @if(isset($cuota->cobro->recibo)&&($cuota->estadoCuota->nombre!="Cancelada"))
                                <a href="{{ route('recibo.pdf',$cuota->cobro->recibo->id) }}">
                                    <button title="generar recibo" class="btn btn-danger btn-responsive">
                                        <i class="fas fa-file-pdf"></i>
                                    </button>
                                </a>
                            @endif
                            
                            @can('anular cuota')
                            @if(isset($cuota->cobro)&&($cuota->cobro->estado_cobro_id==1))
                                <a data-keyboard="false" data-target="#modal-cancelar-{{ $cuota->id}}" data-toggle="modal">
                                    <button title="Cobrar cuota" class="btn btn-warning btn-responsive">
                                        <i class="">Anular Cobro</i>
                                    </button>
                                </a>
                                @include('cuota.modalcancelar')
                            @endif
                            @endcan

                            
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <!--tfoot>
                    <tr style="text-align:center" class="text-uppercase text-dark">
                        <td style="font-size:110%" width="20%"><b>Total Cuotas Cobradas</b></td>
                        <td width="10%"></td>
                        <td width="20%"></td>
                        <td style="text-align:right; font-size:110%" width="10%"><b>$ {{ $total }}</b></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                    </tr>
                </tfoot-->
            </table>
            
            
        </div>
    </div>
</div>

@push('scripts')
    <script src="{{asset('js/tablaDetalle.js')}}"></script>



@endpush
@endsection

