@extends('layouts.admin')  <!-- Extiende de layout -->

@section('navegacion')
    <li class="breadcrumb-item"><a href="/">Menu Principal</a></li>
    <li class="breadcrumb-item active">Indice de Cuotas</li>
    <li class="breadcrumb-item"><a href="{{URL::action('ArchivoController@index',$alumno->id)}}">Archivos</a></li>
    
@endsection

@section('content') <!-- Contenido -->

<div class="card ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @include('errors.request')
        <div class="card-body">
        <p style="font-size:180%"> <i aria-hidden="true"></i> Indice de Cuotas</p>
        <hr>
            <h4><i class="fa fa-filter" aria-hidden="true"></i> Filtrar</h4>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            
            <br>
            </div>
            <table id="tablaDetalle" style="width:100%" class="table table-striped table-hover">
                <thead style="background-color:#fff">
                    <tr style="text-align:center" class="text-uppercase text-dark">
                        <th width="20%">Fecha a Vencer</th>
                        <th width="15%">Actividad</th>   
                        <th width="15%">Socio</th>
                        <th width="20%">Precio</th>
                        <th width="10%">Estado</th>
                        <th width="20%">Opciones</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($membresias as $membresia)

                        @foreach ($membresia->cuotas as $cuota)
                    
                            <tr style="text-align:center" onmouseover="cambiar_color_over(this)" onmouseout="cambiar_color_out(this)">
                            <td width="10%">{{ $cuota->fecha_casteada() }}</td>
                        <td width="15%">{{ $cuota->actividad }}</td> 
                        <td width="20%">{{ $cuota->alumno }}</td>
                        <td width="15%" style="text-align:right">$ {{ $cuota->monto }}</td> 
                        <td width="20%">@if ($cuota->estadoCuota->nombre  == "Generada")
                                <label class="badge badge-default">{{$cuota->estadoCuota->nombre }}</label>
                            @endif
                            @if ($cuota->estadoCuota->nombre  == "Pagada")
                                <label class="badge badge-primary">{{$cuota->estadoCuota->nombre }}</label>
                                    <!--img src="{{ asset('imagenes/cobro/'.$cuota->cobro->formaPago->logo)}}" class="img-circle elevation-2" width="30px" alt="Cobro Image"-->
                                @if($cuota->cobro->forma_pago_id==1)
                                    <label class="badge badge-success">{{$cuota->cobro->formaPago->nombre }}</label>
                                    
                                @elseif($cuota->cobro->forma_pago_id==2)
                                    <label class="badge badge-info">{{$cuota->cobro->formaPago->nombre }}</label> 
                                    
                                @endif
                                
                            
                            @endif
                            @if ($cuota->estadoCuota->nombre  == "Vencida")
                                <label class="badge badge-danger">{{$cuota->estadoCuota->nombre }}</label>
                            @endif
                            @if ($cuota->estadoCuota->nombre  == "Cancelada")
                                <label class="badge badge-secondary">{{$cuota->estadoCuota->nombre }}</label>
                            @endif
                        </td>                       
                        <td width="20%" style="text-align: center">
                            @can('cobrar cuota')
                            @if($cuota->estadoCuota->nombre!="Pagada")   
                                <a data-keyboard="false" data-target="#modal-cobrar-{{ $cuota->id}}" data-toggle="modal">
                                    <button title="Cobrar cuota" class="btn btn-primary btn-responsive">
                                        <i class="">Cobrar</i>
                                    </button>
                                </a>
                                @include('cuota.modalcobrar')
                            @endif
                            @endcan

                            @if(isset($cuota->cobro->recibo)&&($cuota->estadoCuota->nombre!="Cancelada"))
                                <a href="{{ route('recibo.pdf',$cuota->cobro->recibo->id) }}">
                                    <button title="generar recibo" class="btn btn-danger btn-responsive">
                                        <i class="fas fa-file-pdf"></i>
                                    </button>
                                </a>
                            @endif
                            
                            @can('anular cuota')
                            @if(isset($cuota->cobro)&&($cuota->cobro->estado_cobro_id==1))
                                <a data-keyboard="false" data-target="#modal-cancelar-{{ $cuota->id}}" data-toggle="modal">
                                    <button title="Cobrar cuota" class="btn btn-warning btn-responsive">
                                        <i class="">Anular Cobro</i>
                                    </button>
                                </a>
                                @include('cuota.modalcancelar')
                            @endif
                            @endcan

                            
                        </td>
                            </tr>
                        @endforeach
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@push('scripts')

    <script>
    
    $(document).ready(function() {
    $('#tablaDetalle').DataTable({
        "order":[[ 5, "desc" ]],
        "language":{
            "info":"_TOTAL_ registros",
            "search": "Buscar",
            "paginate": {
                "next":"Siguiente",
                "previous":"Anterior"
            },
            "lengthMenu":'Mostrar <select>'+
                '<option value="5">5</option>'+
                '<option value="10">10</option>'+
                '<select> registros',
            "loadingRecords":"Cargando...",
            "processing":"Procesando...",
            "emptyTable":"No hay datos",
            "zeroRecords":"No hay coincidencias",
            "infoEmpty":"",
            "infoFiltered":""

        },
        "pageLength" : 5,
        "lengthMenu": "[[5, 10], [5, 10]]"
    });
    cambiar_color_over(celda);
} );

function cambiar_color_over(celda){
celda.style.backgroundColor="#A7A7A7"
}
function cambiar_color_out(celda){
celda.style.backgroundColor="#FFFFFF"


}

    
    </script>



@endpush
@endsection

