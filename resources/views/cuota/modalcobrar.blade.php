
<style>
    .select2-container .select2-selection {
	line-height: 1.6 !important;
	height: 2.375rem !important;
	border-radius: 3px !important;
    }

</style>

<div class="modal fade" id="modal-cobrar-{{$cuota->id}}"  tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cobrar Cuota</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{Form::Open(array(
                    'action'=>array('CuotaController@cobrar'),
                    'method'=>'post'
                ))}}
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="actividad">Actividad</label>
                                <input type="string"value="{{ $cuota->actividad }}"class="form-control" disabled>
                                <input name="cuota_id" type="hidden" value="{{ $cuota->id }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="alumno">Alumno</label>
                                    <input type="string"value="{{ $cuota->alumno }}"class="form-control" disabled>
                                    <input name="alumno" type="hidden" value="{{ $cuota->alumno }}" class="form-control">
                                </div>
                                <br>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="estado">Estado</label>
                                    <input type="string"value="{{ $cuota->estadoCuota->nombre }}"class="form-control" disabled>
                                    <input name="estado" type="hidden" value="{{ $cuota->estadoCuota->id }}" class="form-control">
                                </div>
                                <br>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="monto">Monto</label>
                                    <input type="string"value="${{ $cuota->membresia->agenda->monto }}"class="form-control" disabled>
                                    <input name="monto" type="hidden" value="{{ $cuota->monto }}" class="form-control">
                                </div>
                                <br>
                            </div>
                        </div>
                        @if($cuota->estado_cuota_id==3)
                        <div class="col-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="estado">% Recargo por vencimiento</label>
                                    <input type="string"value="{{ $recargo->incremento }}"class="form-control" disabled>
                                    <!--input name="estado" type="hidden" value="{{ $cuota->estadoCuota->id }}" class="form-control"-->
                                </div>
                                <br>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="monto">Monto Total</label>
                                    <?php

                                        $total= $cuota->membresia->agenda->monto + ($cuota->membresia->actividad->monto*$recargo->incremento)/100;
                                        
                                    ?>
                                    <input type="string"value="${{ $total }}"class="form-control" disabled>
                                    
                                </div>
                                <br>
                            </div>
                        </div>
                        @endif
                    </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group" style="text-align:center">
                    <label>

                    </label>
                    <br>
                    <!--button title="Limpiar" class="btn btn-danger" type="reset"><i class="fa fa-eraser"></i> Limpiar</button-->
                    <button title="Cobrar" id="confirmar" class="btn btn-success btn-lg" type="submit"> <i class="fas fa-dollar-sign"></i> Cobrar</button>
                </div>
            </div>
            {{Form::Close()}}
        </div>
    </div>
</div>

@push('scripts')
<script>

    $(document).ready(function(){

        
        $("select").select2({width:'100%'});

    });

</script>
@endpush