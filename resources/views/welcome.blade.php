@extends('layouts.admin') <!-- Extiende de layout -->

@section('titulo') <!-- Titulo -->
<div class="box-header" style="text-align: center">
    <h4 class = "box-title" style="font-size:120%">Bienvenido/a: <b style="font-size:120%"> {{ Auth::user()->name }}</b></h4>
        @if($dia==$fecha)
            <h3 class = "box-title" style="font-size:120%">!Feliz Cumple!<i class="fa fa-birthday-cake" aria-hidden="true" style="color:red; width:6; height:6;"></i> {{$empresa->razon_social}} te saluda</h3>
        @endif
    <div class="box-tools pull-right"> </div>
</div>
@endsection

@section('content') <!-- Contenido -->
<div class="card-body">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="row">
            
            @can('listar usuarios')
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="small-box bg-primary">
                    <div class="inner">
                        <h3>{{$cantUser}}</h3>
                        <p style="font-size:150%">Personas registradas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-id-card" aria-hidden="true"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        Más Información <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            @endcan
            @role('Administrador')
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{now()->translatedFormat('l d')}}</h3>
                        <!--$diaActual = Carbon::now()->translatedFormat('l d \d\e F \d\e\l Y');
                        // jueves 04 de junio del 2020-->
                        <p style="font-size:150%">Asistencias</p>
                    </div>
                <div class="icon">
                    <i class="far fa-calendar-alt" aria-hidden="true"></i>
                </div>
                <a href="asistencia" class="small-box-footer">
                    Registrar Asistencia <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
            @endrole

            @role('Alumno')
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>{{now()->translatedFormat('l d')}}</h3>
                        <!--$diaActual = Carbon::now()->translatedFormat('l d \d\e F \d\e\l Y');
                        // jueves 04 de junio del 2020-->
                        <p style="font-size:150%">Asistencia</p>
                    </div>
                <div class="icon">
                    <i class="far fa-calendar-alt" aria-hidden="true"></i>
                </div>
                <a href="confirmar" class="small-box-footer">
                    Confirmar Asistencia <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
            @endrole

        </div>
    </div>
</div>
@endsection
