<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">


    <title>Ficha Médica</title>
</head>
<body>

    <div id="content" class="container">
        
        <h3 style="text-align: center">FICHA MÉDICA</h3>


        <!-- DATOS PERSONALES -->
        <h5 style="text-align: center; background-color: brown; color: #FFFFFF"><b>DATOS PERSONALES</b></h5>
        <table class="table table-condensed table-hover" style="border:1px solid #FFFFFF; width:100%">
            <tr style="text-align: left" valign="middle">
              <th colspan="4">Apellidos y Nombres Completos: </th>
              <td colspan="3" style="text-align: left"></td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th width="5%" >Sexo: </th>
                <td width="5%" style="text-align: left"></td>
                <th width="35%">Fecha de Nacimiento: </th>
                <td width="10%" style="text-align: left"></td>
                <th width="5%">Edad: </th>
                <td colspan="2" width="40%" style="text-align: left"></td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="3" >En caso de emergencia llamar al telefono: </th>
                <td colspan="1" style="text-align: left"></td>
                <th colspan="2">Nombre: </th>
                <td style="text-align: left"></td>
            </tr>
        </table>

        
        <!-- ANTECEDENTES RECIENTES -->
        <h5 style="text-align: center; background-color: brown; color: #FFFFFF"><b>DATOS CLÍNICOS</b></h5>
        <table  class="table table-condensed table-hover" style="border:1px solid #FFFFFF; width:100%">
            <tr style="text-align: left" valign="middle">
                <th colspan="7">¿Sufrió algun accidente, herida, o intervención quirúrjica ? </th>
                <td  colspan="1" style="text-align: left">
                    
                </td>

            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="1">¿Cuáles? </th>
                <td style="text-align: left" colspan="7"></td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="7">¿Le ha quedado alguna secuela? </th>
                <td  colspan="1" style="text-align: left">
                    
                </td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="1">¿Cuáles? </th>
                <td style="text-align: left" colspan="7"></td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="7">¿Realiza alguna rehabilitación? </th>
                <td  colspan="1" style="text-align: left">
                    
                </td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="1">¿Cuáles? </th>
                <td style="text-align: left" colspan="7"></td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="7">¿Realiza otra actividad física?</th>
                <td  colspan="1" style="text-align: left">
                    
                </td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="1">¿Cuál? (Aeróbica/Anaeróbica) </th>
                <td style="text-align: left" colspan="7"></td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="2" >Peso (Kgrs.): </th>
                <td colspan="1" style="text-align: left"></td>
                <th colspan="3">Estatura: (Mts.) </th>
                <td colspan="2" style="text-align: left"></td>
            </tr>
        </table>

        <!-- ANTECEDENTES RECIENTES -->
        <h5 style="text-align: center; background-color: brown; color: #FFFFFF"><b>¿PADECE ALGUNA DE LAS SIGUIENTES ENFERMEDADES?</b></h5>
        <table  class="table table-condensed table-hover" style="border:1px solid #FFFFFF; width:100%">
            <tr style="text-align: left" valign="middle">
                <th colspan="1">Presión (Alta/Baja): </th>
                <td  colspan="1" style="text-align: left">
                    
                </td>
                <th colspan="3">Colesterol: </th>
                <td  colspan="3" style="text-align: left">
                    
                </td>

            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="1">Asma: </th>
                <td  colspan="1" style="text-align: left">
                    
                </td>
                <th colspan="3">Alergia:</th>
                <td  colspan="3" style="text-align: left">
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="1">¿Se encuentra medicado? </th>
                <td style="text-align: left" colspan="7"></td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="1">Enfermedad Coronarea: </th>
                <td style="text-align: left" colspan="7"></td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="1">Enfermedad Cardiovasculares: </th>
                <td style="text-align: left" colspan="7"></td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="1">Neuralgía: </th>
                <td style="text-align: left" colspan="7"></td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="1">Lumbalgía: </th>
                <td style="text-align: left" colspan="7"></td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="1">Artrosis: </th>
                <td style="text-align: left" colspan="7"></td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="1">Otras: </th>
                <td style="text-align: left" colspan="7"></td>
            </tr>
            <tr style="text-align: left" valign="middle">
                <th colspan="1">OBSERVACIONES: </th>
                <td style="text-align: left" colspan="7"></td>
            </tr>
        </table>
        Lugar y Fecha: 
        <br><br><br><br><br><br>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                
                <label>Firma y Aclaración del Alumno</label>
            </div>
        
            <!--Fecha de Realización -->
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                
                <label>Firma y Aclaración del Médico</label>
            </div>
        </div>
        
    </div>




    <script src="{{asset('js/jQuery-2.1.4.min.js')}}"></script>




 <!-- Bootstrap 3.3.5 -->
 <script src="{{ asset('js/bootstrap.min.js') }}"></script>




   <!-- AdminLTE App -->
 <script src="{{asset('js/app.min.js')}}"></script>

 <script type="text/php">
    if ( isset($pdf) ) {
        $pdf->page_script('
            $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
            $pdf->text(270, 800, "Pagina $PAGE_NUM de $PAGE_COUNT", $font, 10);
        ');
    }
</script>


</body>
</html>
